package com.astrology.iscopes.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.preference.PreferenceManager;

import androidx.core.app.NotificationCompat;

import com.astrology.iscopes.R;
import com.astrology.iscopes.common.Utility;

import java.util.Calendar;

public class TimeAlarm extends BroadcastReceiver {

    private static final int NOTIFICATION_REQUEST_CODE = 1020;
    private static final String NOTIFICATION_CHANNEL_ID = "10001";

    @Override
    public void onReceive(Context context, Intent intent) {
        Calendar c = Calendar.getInstance();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        int notificationday = preferences.getInt("notificationDay", 999);
        int notificationcount = preferences.getInt("notificationCount", 999);
        Utility.debug_log("onReceive..." + notificationday + " - " + c.get(Calendar.DATE));
        if (notificationday != c.get(Calendar.DATE)) {
            notificationday = c.get(Calendar.DATE);
            notificationcount = 1;
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("notificationDay", notificationday);
            editor.putInt("notificationCount", notificationcount);
            editor.apply();
            showNotification(context);
        } else if (notificationcount == 0) {
            notificationcount = 1;
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("notificationCount", notificationcount);
            editor.apply();
            showNotification(context);
        }
    }

    public void showNotification(Context context) {
        CharSequence title = "Horoscopes";
        CharSequence message = "Good morning! Today's horoscopes are waiting for you!";
        CharSequence channelName = "Reminder channel";
        String channelDescription = "Everyday reminder";

        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;

        Intent resultIntent = new Intent(context, NotificationHandling.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, NOTIFICATION_REQUEST_CODE, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(false)
                .setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, importance);
            notificationChannel.setDescription(channelDescription);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            if (manager != null) {
                manager.createNotificationChannel(notificationChannel);
            }
        }

        if (manager != null) {
            manager.notify(NOTIFICATION_REQUEST_CODE, builder.build());
        }
    }
}