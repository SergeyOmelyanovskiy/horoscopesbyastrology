package com.astrology.iscopes.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.astrology.iscopes.R;
import com.astrology.iscopes.appsupport.TwitterClient;
import com.astrology.iscopes.appsupport.TwitterStatus;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.MgrMenu;
import com.astrology.iscopes.common.MgrShared;
import com.astrology.iscopes.common.Utility;
import com.astrology.iscopes.orm.Horoscope;
import com.astrology.iscopes.orm.Horotype;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.quantcast.measurement.service.QuantcastClient;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class HoroDetail extends BaseActivity implements OnClickListener, AnimationListener, OnGestureListener {

    public static int last = 0;
    public static int current = 1;
    public static int next = 2;

    public int selectedSegment = current;
    private GestureDetector gd;
    private Animation right_to_left;

    public static Horoscope hs;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horoscopedetailscreen);
        gd = new GestureDetector(this, this);
        right_to_left = AnimationUtils.loadAnimation(this, R.anim.left_to_right); //R.anim.right_to_left);
        //left_to_right = AnimationUtils.loadAnimation(this, R.anim.left_to_right);

        findViewById(R.id.horoscopedetailrefreshbutton).setOnClickListener(this);
        findViewById(R.id.previoushoroscopedetailbutton).setOnClickListener(this);
        findViewById(R.id.currenthoroscopedetailbutton).setOnClickListener(this);
        findViewById(R.id.nexthoroscopedetailbutton).setOnClickListener(this);
        findViewById(R.id.sharehoroscopedetailbutton).setOnClickListener(this);
        findViewById(R.id.play_video_button).setVisibility(View.INVISIBLE); //.setOnClickListener(this);

        showHoroscope();
    }

    @Override
    public void onStart() {
        super.onStart();
        QuantcastClient.activityStart(this, Consts.key_quant_cast, null, null);
        String navigationText = MgrMenu.getInstance(this).getHorotype().longName;
        if (navigationText.contains(Consts.sign_name_holder)) {
            trackScreen(Consts.track_name_about + MgrMenu.getInstance(this).selectedHoroSignName + "_screen");
            fireBaseLogEventScreen(Consts.track_name_about + MgrMenu.getInstance(this).selectedHoroSignName + "_screen");
        } else {
            trackScreen(navigationText.replace( " ", "_") + "_screen");
            fireBaseLogEventScreen(navigationText.replace( " ", "_") + "_screen");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        QuantcastClient.activityStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MgrMenu.getInstance(this).isDateChanged()) {
            if (MgrShared.getInstance(this).isNetworkAvailable()) {
                finish();
            } else {
                Toast.makeText(this, getString(R.string.toast_network_unavailable), Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showView(int view_id, boolean yes) {
        findViewById(view_id).setVisibility(yes ? View.VISIBLE : View.INVISIBLE);
    }

    public void displayHoroscopeDetails() {
        Horotype horotype = MgrMenu.getInstance(this).getHorotype();
        TextView detailText = findViewById(R.id.horoscopedetailtext);
        TextView dateText = findViewById(R.id.detaildatetextview);
        String icon = String.format("%s%s", Consts.prefix_icn_menulist, MgrMenu.getInstance(this).selectedHoroSignName).toLowerCase(Locale.US);
        findViewById(R.id.date_bar_layout).setVisibility(View.VISIBLE);
        showGoogleAd();

        ((ImageView) HoroDetail.this.findViewById(R.id.detailicon)).setImageResource(getResources().getIdentifier(icon, "drawable", getPackageName()));
        ((TextView) HoroDetail.this.findViewById(R.id.horodetailnavigationtext)).setText(horotype.shortName == null ? "" : horotype.shortName.replace(Consts.sign_name_holder, MgrMenu.getInstance(this).selectedHoroSignName));

        if (!MgrMenu.getInstance(this).isHoroDetailsAvailable()) {
            showView(R.id.play_video_button, false);
            showView(R.id.horoscopedetailscroll, true);
            showView(R.id.sharehoroscopedetailbutton, false);
            detailText.setText(getString(R.string.horoscope_unavailable));
            if (horotype.interval != null && horotype.interval == Consts.interval_daily) {
                dateText.setText(Utility.formatDateForDaily(MgrMenu.getInstance(this).getMenu().menuDate));
            }
        } else {
            List<Horoscope> horoscopes = horotype.getHoroscope();
            updateSegmentImages();

            hs = horoscopes.get(horoscopes.size() <= selectedSegment ? 0 : selectedSegment);
            showView(R.id.play_video_button, horotype.horoId == Consts.cosmoGalId);
            showView(R.id.horoscopedetailscroll, horotype.horoId != Consts.cosmoGalId);
            showView(R.id.sharehoroscopedetailbutton, true);

            if (horotype.horoId != Consts.cosmoGalId) {
                detailText.setText(hs.getHoroscopeText(getString(R.string.horoscope_unavailable)));
            }

            if (horotype.interval == Consts.interval_default && !horotype.shortName.equalsIgnoreCase(Consts.title_about)) {
                findViewById(R.id.date_bar_layout).setVisibility(View.GONE);
            } else if (horotype.shortName.equalsIgnoreCase(Consts.title_about)) {
                dateText.setText(MgrMenu.getInstance(this).selectedHoroSignName);
            } else if (horotype.interval == Consts.interval_daily) {
                dateText.setText(Utility.formatDateForDaily(hs.date));
            } else {
                dateText.setText(hs.dateInWords);
            }
        }
    }

    public void updateSegmentImages() {
        Horotype horotype = MgrMenu.getInstance(this).getHorotype();
        Button previousButton = findViewById(R.id.previoushoroscopedetailbutton);
        Button currentButton = findViewById(R.id.currenthoroscopedetailbutton);
        Button nextButton = findViewById(R.id.nexthoroscopedetailbutton);

        if (horotype.horogroupId == 0 || horotype.horogroupId == 5 || horotype.horogroupId == 6 || (horotype.getHoroscope() != null && horotype.getHoroscope().size() <= 1)) {
            previousButton.setVisibility(ImageView.INVISIBLE);
            currentButton.setVisibility(ImageView.INVISIBLE);
            nextButton.setVisibility(ImageView.INVISIBLE);
            return;
        }

        String[] button_texts = new String[3];
        previousButton.setVisibility(View.VISIBLE);
        currentButton.setVisibility(View.VISIBLE);
        nextButton.setVisibility(View.VISIBLE);

        previousButton.setBackgroundResource(last == selectedSegment ? R.drawable.previous_pressed : R.drawable.previous_unpressed);
        currentButton.setBackgroundResource(current == selectedSegment ? R.drawable.current_pressed : R.drawable.current_unpressed);
        nextButton.setBackgroundResource(next == selectedSegment ? R.drawable.btnnext_pressed : R.drawable.btnnext_unpressed);

        if (horotype.interval == Consts.interval_daily)
            button_texts = getResources().getStringArray(R.array.button_text_daily);
        else if (horotype.interval == Consts.interval_weekly)
            button_texts = getResources().getStringArray(R.array.button_text_weekly);
        else if (horotype.interval == Consts.interval_monthly)
            button_texts = getResources().getStringArray(R.array.button_text_monthly);
        else if (horotype.interval == Consts.interval_yearly) {
            button_texts = getResources().getStringArray(R.array.button_text_yearly);
            if (Calendar.getInstance().get(Calendar.MONTH) >= Calendar.NOVEMBER) {
                previousButton.setVisibility(View.INVISIBLE);
                currentButton.setBackgroundResource(current == selectedSegment ? R.drawable.previous_pressed : R.drawable.previous_unpressed);
            } else {
                currentButton.setBackgroundResource(current == selectedSegment ? R.drawable.btnnext_pressed : R.drawable.btnnext_unpressed);
                nextButton.setVisibility(View.INVISIBLE);
            }
        }

        previousButton.setText(button_texts[0]);
        currentButton.setText(button_texts[1]);
        nextButton.setText(button_texts[2]);
    }

    public void animateBackgroundImage() {
        ImageView imageView = findViewById(R.id.detailbackgroundimage);
        String icon = (Consts.prefix_img_detailbg + MgrMenu.getInstance(this).getHorotype().fallBackIconName).replaceAll("-", "_");
        imageView.setImageDrawable(null);
        imageView.setImageBitmap(null);
        imageView.setAnimation(null);

        try {
            imageView.setImageResource(getResources().getIdentifier(icon, "drawable", getPackageName()));
            Animation an = new AlphaAnimation(1.0f, 0.0f);
            an.setDuration(2000); // duration in ms
            an.setRepeatCount(0); // -1 = infinite repeated
            //an.setRepeatMode(Animation.REVERSE); // reverses each repeat
            an.setFillBefore(true);
            an.setFillEnabled(true);
            an.setFillAfter(true);
            an.setAnimationListener(this);
            imageView.setAnimation(an);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void swipeAnimation() {
        try {
            ImageView arrows = findViewById(R.id.arrowimage);
            ImageView finger = findViewById(R.id.handimage);
            if (PreferenceManager.getDefaultSharedPreferences(this).getInt("showAnimation", 999) == 0) {
                arrows.setImageResource(getResources().getIdentifier("fs_horo_arrows", "drawable", getPackageName()));
                finger.setImageResource(getResources().getIdentifier("fs_horo_handwithcircle", "drawable", getPackageName()));
                finger.setAnimation(right_to_left);
            } else {
                arrows.setImageDrawable(null);
                finger.setImageDrawable(null);
                finger.setAnimation(null);
            }
        } catch (Exception e) {
            ImageView bg = findViewById(R.id.detailbackgroundimage);
            bg.setImageDrawable(null);
            bg.setImageBitmap(null);
            bg.setAnimation(null);
        }
    }

    @Override
    public void onClick(View arg0) {
        int id = arg0.getId();
        switch (id) {
            case R.id.promo_ad_layout_top:
            case R.id.promo_ad_layout_bottom:
                //promoAdLayoutClicked();
                break;
            case R.id.uphorotypebutton:
            case R.id.downhorotypebutton:
                boolean up = id == R.id.uphorotypebutton;
                trackScreen(up ? Consts.tap_horo_detail_up : Consts.tap_horo_detail_down);
                fireBaseLogEventScreen(up ? Consts.tap_horo_detail_up : Consts.tap_horo_detail_down);
                nextHorotypeDetails(up ? -1 : 1);
                break;
            case R.id.previoushoroscopedetailbutton:
            case R.id.currenthoroscopedetailbutton:
            case R.id.nexthoroscopedetailbutton:
                trackScreen(id == R.id.previoushoroscopedetailbutton ? Consts.tap_horo_detail_previous : id == R.id.nexthoroscopedetailbutton ? Consts.tap_horo_detail_next : Consts.tap_horo_detail_current);
                fireBaseLogEventScreen(id == R.id.previoushoroscopedetailbutton ? Consts.tap_horo_detail_previous : id == R.id.nexthoroscopedetailbutton ? Consts.tap_horo_detail_next : Consts.tap_horo_detail_current);
                if (MgrMenu.getInstance(this).isDateChanged() && MgrShared.getInstance(this).isNetworkAvailable()) {
                    finish();
                } else {
                    selectedSegment = id == R.id.previoushoroscopedetailbutton ? last : id == R.id.nexthoroscopedetailbutton ? next : current;
                    displayHoroscopeDetails();
                    //displayPromo(menuManager.getSelectedHorotypeId());
                    updateSegmentImages();
                }
                break;
            case R.id.horoscopedetailrefreshbutton:
                trackScreen(Consts.tap_horo_detail_refresh);
                fireBaseLogEventScreen(Consts.tap_horo_detail_refresh);
                refreshButtonClicked();
                break;
            case R.id.sharehoroscopedetailbutton:
                trackScreen(Consts.screen_horo_detail);
                fireBaseLogEventScreen(Consts.screen_horo_detail);
                shareHoroscope();
                break;
            case R.id.play_video_button:
                trackScreen(Consts.tap_horo_detail_play_video);
                fireBaseLogEventScreen(Consts.tap_horo_detail_play_video);
                playVideoButtonClicked(true);
                break;
            default:
                break;
        }
    }

    String generateVideoURL(Boolean isHttpURL) throws Exception {

        String baseURL;
        Calendar timeLessCalendar = Calendar.getInstance();
        // timeLessCalendar.setTimeZone();
        timeLessCalendar.set(Calendar.HOUR_OF_DAY, 0);
        timeLessCalendar.set(Calendar.MINUTE, 0);
        timeLessCalendar.set(Calendar.SECOND, 0);
        timeLessCalendar.set(Calendar.MILLISECOND, 0);
        long validStartTime = timeLessCalendar.getTime().getTime() / 1000;
        if (MgrShared.getInstance(this).isNetworkAvailable()) {
            baseURL = hs.lowBandwidthUrl;
        } else if (MgrShared.getInstance(this).isTablet()) {
            baseURL = hs.highBandwidthUrl;
        } else {
            baseURL = hs.normalBandwidthUrl;
        }

        return makeHashURL(baseURL, validStartTime, isHttpURL);
    }

    public void playVideoButtonClicked(final Boolean isHttpURL) {
        boolean videoEnabled = false;
        if (videoEnabled && MgrShared.getInstance(this).isNetworkAvailable()) {
            final Intent intent = new Intent(this, HoroVideo.class);
            new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    try {
                        intent.putExtra("videourl", generateVideoURL(isHttpURL));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        return true;
                    }

                    return false;
                }

                @Override
                protected void onPostExecute(Boolean hasErrorOccurred) {
                    if (!hasErrorOccurred) {
                        startActivity(intent);
                    } else {
                        Utility.showAlert(HoroDetail.this, getString(R.string.alert_title));
                    }
                }
            }.execute();
        } else {
            Utility.showAlert(this, getString(R.string.alert_message));
        }
    }

    private String makeHashURL(String s, long expiryTime, Boolean isHttpURL) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String url = s;
        String salt = "astr01villaGe";

        // s = "/96195/MZ/astl/shortform/201203/20120320_Aries_daily_astro-VideoScope_623_4x3_30.mov?fp=IPTV&__source=ios_5_0_apple_iPhone_iScopes&__source2=iScopes_3_0_internal12";
        s = s.substring(28); // trimming the URL
        expiryTime = expiryTime + 60 * 60 * 48; // Adding 2 days window to the
        if (url == hs.lowBandwidthUrl) {
            s = String.format("%s&__source=%s_%s_%s_%s_%s&__source2=%s_%s",
                    s,
                    Consts.os_name,
                    Utility.reformat(Consts.os_version),
                    Utility.reformat(Consts.device_brand.trim()),
                    Utility.reformat(Consts.device_model).trim(),
                    getString(R.string.app_name_for_url),
                    getString(R.string.app_name_for_url),
                    Utility.reformat(Utility.getVersionNumber(getApplicationContext())));
        } else if (url == hs.highBandwidthUrl) {
            s = String.format("%s&__source=%s_%s_%s_%s_app&__source2=%s_%s",
                    s,
                    Consts.os_name,
                    Utility.reformat(Consts.os_version),
                    Utility.reformat(Consts.device_brand),
                    Utility.reformat(Consts.device_model),
                    getString(R.string.app_name_for_url),
                    Utility.reformat(Utility.getVersionNumber(getApplicationContext())));
        } else if (url == hs.normalBandwidthUrl) {
            // s = s+"&fd=http";
            // Log.e("New url for normal bandwidth is: ", s);
        }

        if (isHttpURL) {
            s = s + "&fd=http";
        }

        return generate_Auth_Token(s, salt, expiryTime, url, isHttpURL);
    }

    private String generate_Auth_Token(String path, String salt, long expiryTime, String url, Boolean isHttpURL) throws UnsupportedEncodingException, NoSuchAlgorithmException {

        String hash = null;
        String time = Long.toString(expiryTime);
        //StringBuffer sb = new StringBuffer();
        byte expiryTime1 = (byte) (expiryTime & 0xff);
        byte expiryTime2 = (byte) ((expiryTime >> 8) & 0xff);
        byte expiryTime3 = (byte) ((expiryTime >> 16) & 0xff);
        byte expiryTime4 = (byte) ((expiryTime >> 24) & 0xff);

        byte[] a = {expiryTime1, expiryTime2, expiryTime3, expiryTime4};
        String s = path + salt;
        byte[] tokenArray = Utility.md5(s, a);// md5.getBytes("UTF-8");
        byte[] array = salt.getBytes("UTF-8");
        byte[] finalByteArray = new byte[array.length + tokenArray.length];
        for (int i = 0; i < array.length; i++) {
            finalByteArray[i] = array[i];
        }

        int arrayLengthcount = 0;
        for (int i = array.length; i < finalByteArray.length; i++) {
            finalByteArray[i] = tokenArray[arrayLengthcount];
            arrayLengthcount++;
        }

        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
        md.update(finalByteArray);
        byte[] finalByteArray2 = md.digest();
        for (int i = 0; i < finalByteArray2.length; i++) {
            hash = new BigInteger(1, finalByteArray2).toString(16);
            if (hash.length() == 31) {
                hash = "0" + hash;
            }
        }
        // Log.e("New Token Length: ", ""+hash.length());
        // Log.e("Token generated is: ", sb.toString());
        // Log.e("final hash URL is: ", hashURL);
        return makeFinalURL(url, time, hash, isHttpURL);
    }

    private String makeFinalURL(String url, String time, String token, Boolean isHttpURL) {
        if (url == hs.normalBandwidthUrl) {
            url = String.format("%s?token=%s_%s", url, time, token);
        } else {
            url = String.format("%s&__source=%s_%s_%s_%s_%s&__source2=%s_%s&token=%s_%s",
                    url,
                    Consts.os_name,
                    Utility.reformat(Consts.os_version),
                    Utility.reformat(Consts.device_brand),
                    Utility.reformat(Consts.device_model),
                    getString(R.string.app_name_for_url),
                    getString(R.string.app_name_for_url),
                    Utility.reformat(Utility.getVersionNumber(getApplicationContext())),
                    time,
                    token);
            if (isHttpURL) {
                url = url + "&fd=http";
            }
        }

        return url;
    }

    public void refreshButtonClicked() {
        String navText = MgrMenu.getInstance(this).getHorotype().longName;
        if (navText.contains(Consts.sign_name_holder)) {
            trackScreen(navText.contains("New") ? Consts.new_daily_video_scope : Consts.track_name_about + MgrMenu.getInstance(this).selectedHoroSignName + "_screen");
            fireBaseLogEventScreen(navText.contains("New") ? Consts.new_daily_video_scope : Consts.track_name_about + MgrMenu.getInstance(this).selectedHoroSignName + "_screen");
        } else {
            trackScreen(navText.replace(" ", "_") + "_screen");
            fireBaseLogEventScreen(navText.replace(" ", "_") + "_screen");
        }

        if (MgrMenu.getInstance(this).isDateChanged() && MgrShared.getInstance(this).isNetworkAvailable()) {
            finish();
        } else if (!MgrMenu.getInstance(this).isDateChanged()) {
            new loadSelectedHoroscopeDetails().execute();
        } else {
            Utility.showAlert(this, getString(R.string.alert_message));
        }
    }

    public void nextHorotypeDetails(int direction) {
        if (MgrMenu.getInstance(this).isDateChanged() && MgrShared.getInstance(this).isNetworkAvailable()) {
            finish();
        } else {
            MgrMenu.getInstance(this).nextHoroscope(direction);
            // Load from cache if available else fetch from server
            if (MgrMenu.getInstance(this).isHoroDetailsAvailable() && !MgrMenu.getInstance(this).isDateChanged()) {
                showHoroscope();
            } else if (MgrShared.getInstance(this).isNetworkAvailable()) {
                new loadSelectedHoroscopeDetails().execute();
            } else {
                MgrMenu.getInstance(this).nextHoroscope(-1 * direction);
                displayHoroscopeDetails();
                Utility.showAlert(this, getString(R.string.alert_message));
            }

            String navText = MgrMenu.getInstance(this).getHorotype().longName;
            if (navText.contains(Consts.sign_name_holder)) {
                trackScreen(Consts.track_name_about + MgrMenu.getInstance(this).selectedHoroSignName + "_screen");
                fireBaseLogEventScreen(Consts.track_name_about + MgrMenu.getInstance(this).selectedHoroSignName + "_screen");
            } else {
                trackScreen(navText + "_screen");
                fireBaseLogEventScreen(navText + "_screen");
            }
        }
    }

    public class loadSelectedHoroscopeDetails extends AsyncTask<Void, Void, String> {
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(HoroDetail.this);
            // progressDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.setMessage(getString(R.string.loading_content));
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                MgrMenu.getInstance(HoroDetail.this).loadHoroscopeDetailsFromServer();
            } catch (Exception e) {
                return e.getMessage();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String errorMessage) {
            if (pDialog != null && pDialog.isShowing()) pDialog.dismiss();
            if (errorMessage.length() > 0) {
                Utility.showAlert(HoroDetail.this, getString(R.string.data_not_retrieved));
            } else {
                showHoroscope();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }
    }

    public void showHoroscope() {
        //displayPromo(menuManager.getSelectedHorotypeId());
        displayHoroscopeDetails();
        updateSegmentImages();
        animateBackgroundImage();
        swipeAnimation();
        showGoogleAd();
    }

    public void shareHoroscope() {
        if (MgrMenu.getInstance(this).isDateChanged() && MgrShared.getInstance(this).isNetworkAvailable()) {
            finish();
        } else if (!MgrShared.getInstance(this).isNetworkAvailable()) {
            Utility.showAlert(this, getString(R.string.alert_message));
        } else {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.share_horoscope))
                    .setItems(Utility.share_items(this), shareDialogListenter)
                    .show();
        }
    }

    public DialogInterface.OnClickListener shareDialogListenter = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int item) {
            switch (item) {
                case 0:
                    trackScreen(Consts.tap_share_email);
                    fireBaseLogEventScreen(Consts.tap_share_email);
                    shareHoroscopeByEmail();
                    break;
                case 1:
                    trackScreen(Consts.tap_share_twitter);
                    fireBaseLogEventScreen(Consts.tap_share_twitter);
                    shareHoroscopeByTwitter();
                    break;
                default:
                    break;
            }
        }
    };

    public void shareHoroscopeByEmail() {
        String shareText = Utility.getEmailShareText(MgrMenu.getInstance(this).selectedHoroSignName, MgrMenu.getInstance(this).getHorotype(), hs);
        String title = Utility.getHoroscopeShareName(MgrMenu.getInstance(this).getHorotype().shareName, hs.name, MgrMenu.getInstance(this).selectedHoroSignName);
        Intent it = new Intent(Intent.ACTION_SEND);
        it.putExtra(Intent.EXTRA_EMAIL, "");
        it.putExtra(Intent.EXTRA_SUBJECT, title);
        it.putExtra(Intent.EXTRA_TEXT, shareText);
        it.setType("text/plain");
        startActivity(it);
    }

    public void shareHoroscopeByTwitter() {
        // int ind = storyURL.indexOf("?");
        Intent intent = new Intent(this, TwitterClient.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.putExtra(Consts.activity_source, Consts.activity_horo_detail);
        String str = Utility.getTwitterShareText(MgrMenu.getInstance(this).selectedHoroSignName, MgrMenu.getInstance(this).getHorotype(), hs);
        if (str != null && str.length() > 0) {
            str = Utility.removeHTMLTags(str);
        }

        TwitterStatus twitterStatus = TwitterStatus.getInstance();
        twitterStatus.twitterPostHeading = (str);
        twitterStatus.twitterPostURL = (hs.shareUrl);
        startActivity(intent);
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        ImageView imageView = findViewById(R.id.detailbackgroundimage);
        imageView.setImageDrawable(null);
        imageView.setImageBitmap(null);
        imageView.setAnimation(null);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    public String getCurrentlyPalyingURL() {
        ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED || connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            return "Low";
        } else if (MgrShared.getInstance(this).isTablet()) {
            return "High";
        } else {
            return "Normal";
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        gd.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }

    @Override
    public boolean onTouchEvent(MotionEvent me) {
        return gd.onTouchEvent(me);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        float sensitivity = 100;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int showAnimation = preferences.getInt("showAnimation", 999);
        // Swipe Up Check
        if (e1.getY() - e2.getY() > sensitivity) {
            // Setting Image Resource to Up_Arrow on Swipe Up
            // Toast.makeText(getApplicationContext(), "Swipe Up", 3).show();
            return true;
        }
        // Swipe Down Check
        else if (e2.getY() - e1.getY() > sensitivity) {
            // Setting Image Resource to Down_Arrow on Swipe Down
            // Toast.makeText(getApplicationContext(), "Swipe Down", 3).show();
            return true;
        }
        // Swipe Left Check
        else if (e1.getX() - e2.getX() > sensitivity) {
            // Setting Image Resource to Left_Arrow on Swipe Left
            SwipeLeftAnimation();

            showAnimation++;
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("showAnimation", showAnimation);
            editor.apply();

            nextHorotypeDetails(1);
            return true;
        }
        // Swipe Right Check
        else if (e2.getX() - e1.getX() > sensitivity) {
            // Setting Image Resource to Right_Arrow on Swipe Right
            SwipeRightAnimation();

            showAnimation++;
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("showAnimation", showAnimation);
            editor.apply();

            nextHorotypeDetails(-1);
            return true;
        } else {
            // If some error occurrs, setting again to Default_Image (Actually
            // it will never happen in this case)
            // Toast.makeText(getApplicationContext(), "Error", 3).show();
            return true;
        }
    }

    private void SwipeLeftAnimation() {
        Animation anim = new ScaleAnimation(0f, 50f, 0f, 0f);
        anim.start();
    }

    private void SwipeRightAnimation() {
        Animation anim = new ScaleAnimation(0f, 50f, 0f, 0f);
        anim.start();
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    private void showGoogleAd() {
        AdView mAdView = new AdView(this);
        mAdView.setAdUnitId(this.getString(R.string.dfp_key));
        mAdView.setAdSize(AdSize.BANNER);

        LinearLayout promoLayoutBottomF = findViewById(R.id.promo_ad_layout_bottom);
        promoLayoutBottomF.removeAllViews();
        promoLayoutBottomF.addView(mAdView);
        AdRequest request = new AdRequest.Builder()
                //.addTestDevice("33C59FFC686A80974BD7E06CAB8D9210")
                //.addTestDevice("70F138DD54B7AEA1F1660E73B297B215")
                .build();

        mAdView.loadAd(request);
        promoLayoutBottomF.setVisibility(LinearLayout.VISIBLE);
    }
}
