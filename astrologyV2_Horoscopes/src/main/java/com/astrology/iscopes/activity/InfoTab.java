package com.astrology.iscopes.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TabHost;

import com.astrology.iscopes.R;

@SuppressWarnings("deprecation")
public class InfoTab extends TabActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infotablayout);

        final TabHost tabHost = getTabHost();
        tabHost.getTabWidget().setDividerDrawable(R.drawable.tab_divider);

        ImageView abtImage = new ImageView(this);
        abtImage.setBackgroundResource(R.drawable.selector_about);
        tabHost.addTab(tabHost
                .newTabSpec("about")
                .setIndicator(abtImage)
                .setContent(new Intent(this, Info.class)));

        ImageView settingsImage = new ImageView(this);
        settingsImage.setBackgroundResource(R.drawable.selector_settings);
        tabHost.addTab(tabHost
                .newTabSpec("settings")
                .setIndicator(settingsImage)
                .setContent(new Intent(this, Settings.class)));

        //tabHost.setCurrentTab(0);
    }
}