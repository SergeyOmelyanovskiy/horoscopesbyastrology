package com.astrology.iscopes.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.astrology.iscopes.common.MgrMenu;
import com.astrology.iscopes.common.MgrShared;
import com.astrology.iscopes.common.Utility;
import com.astrology.iscopes.database.IScopesDB;
import com.astrology.iscopes.orm.Birthdetails;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class NotificationHandling extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int sign = MgrShared.getInstance(this).selectedSign;

        Birthdetails info = IScopesDB.getInstance(this).getBirthDetails();
        if (info != null && info.birthDate != null) {
            try {
                sign = Utility.signIdForDate(new SimpleDateFormat("MM/dd/yyyy", Locale.US).parse(info.birthDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (sign >= 0 && sign < 12) {
            MgrMenu.getInstance(this).setSelectedHorosignId(sign);
        }

        MgrShared.getInstance(this).selectedSign = sign;
        MgrShared.getInstance(this).setStates();
        startActivity(new Intent(this, Splash.class));
        finish();
    }
}