package com.astrology.iscopes.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.astrology.iscopes.R;
import com.astrology.iscopes.bo.EntryItem;
import com.astrology.iscopes.bo.ListItem;
import com.astrology.iscopes.bo.SectionItem;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.ListAdapterMenu;
import com.astrology.iscopes.common.MgrMenu;
import com.astrology.iscopes.common.MgrShared;
import com.astrology.iscopes.common.Utility;
import com.astrology.iscopes.database.IScopesDB;
import com.astrology.iscopes.orm.Horogroup;
import com.astrology.iscopes.orm.Horomenu;
import com.astrology.iscopes.orm.Horotype;
import com.quantcast.measurement.service.QuantcastClient;

import java.util.ArrayList;
import java.util.Iterator;

public class HoroMenu extends BaseActivity implements OnClickListener, OnItemClickListener {

    private ListAdapterMenu adapter;
    private boolean isEditing = false;
    private ArrayList<ListItem> items = new ArrayList<>();
    private int selectedTab = -1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horoscopemenuscreen);

        findViewById(R.id.menulistrefreshbutton).setOnClickListener(this);
        findViewById(R.id.menulistreditfavoritebutton).setOnClickListener(this);
        findViewById(R.id.alltabbutton).setOnClickListener(this);
        findViewById(R.id.favoritetabbutton).setOnClickListener(this);

        try {
            selectedTab = MgrShared.getInstance(this).selectedMenu;
            ((TextView) findViewById(R.id.menulistnavigationtext)).setText(MgrMenu.getInstance(this).selectedHoroSignName);
            createMenuList(MgrMenu.getInstance(this).getMenu(selectedTab));
            updateButtonImages();
        } catch (Exception e) {
            Utility.showAlert(this, getString(R.string.data_not_retrieved));
        }

        adapter = new ListAdapterMenu(this, items);
        ListView menuList = findViewById(R.id.listview);
        menuList.setAdapter(adapter);
        menuList.setOnItemClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        QuantcastClient.activityStart(this, Consts.key_quant_cast, null, null);
    }

    @Override
    public void onStop() {
        super.onStop();
        QuantcastClient.activityStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Consts.is_editing = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        trackScreen(MgrMenu.getInstance(this).selectedHoroSignName + Consts.screen_horo_menu);
        fireBaseLogEventScreen(MgrMenu.getInstance(this).selectedHoroSignName + Consts.screen_horo_menu);
        if (!Consts.is_editing) {
            editFavoritesButtonClicked();
        }

        if (MgrMenu.getInstance(this).isDateChanged()) {
            if (MgrShared.getInstance(this).isNetworkAvailable()) {
                finish();
            } else {
                Toast.makeText(this, getString(R.string.toast_network_unavailable), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void createMenuList(Horomenu menu) {
        items.removeAll(items);
        String signName = MgrMenu.getInstance(this).selectedHoroSignName;
        Iterator<?> hgItr = menu.getHorogroup().iterator();
        while (hgItr.hasNext()) {
            Horogroup hg = (Horogroup) hgItr.next();
            if (selectedTab == Consts.menu_all || isEditing == true) {
                SectionItem item = new SectionItem(hg.name);
                if (!item.title.equalsIgnoreCase("Video Horoscopes")) items.add(item);
            }

            Iterator<?> htItr = hg.getHorotype().iterator();
            while (htItr.hasNext()) {
                Horotype ht = (Horotype) htItr.next();
                String name = Utility.getHorotypeTextWithSign(ht.mediumName, signName);
                if (name.equalsIgnoreCase(getString(R.string.daily_overview)) ||
                        name.equalsIgnoreCase(getString(R.string.weekly_business)) ||
                        name.equalsIgnoreCase(getString(R.string.monthly_romantic))) {
                    if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("favLaunch", false)) {
                        IScopesDB.getInstance(this).addFavoriteHorotype(ht.horoId);
                    }
                }

                EntryItem item = new EntryItem(ht.horoId, name, ht.fallBackIconName);
                if (!item.title.equalsIgnoreCase("New Daily Videoscope")) items.add(item);
            }

            System.out.println(" ");
        }

        if (selectedTab == Consts.menu_all) {
            items.add(new SectionItem(Consts.title_personal_reading));
            //items.add(new EntryItem(400, Constants.GET_PERSONALIZED_READING_ENTRY, ""));
            items.add(new EntryItem(401, Consts.title_view_reading, "")); // Horo Id for My Reading...
        } else if (selectedTab == Consts.menu_favorite) {
            // items.add(new EntryItem(400, Constants.GET_PERSONALIZED_READING_ENTRY, ""));
            items.add(new EntryItem(401, Consts.title_view_reading, ""));
            items.remove(new EntryItem(150, Consts.title_video_scope, ""));
        }

        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("favLaunch", false).apply();
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.menulistrefreshbutton:
                trackScreen(Consts.tap_horo_menu_refresh);
                fireBaseLogEventScreen(Consts.tap_horo_menu_refresh);
                refreshButtonClicked();
                break;
            case R.id.menulistreditfavoritebutton:
                trackScreen(Consts.tap_horo_menu_edit_favourite);
                fireBaseLogEventScreen(Consts.tap_horo_menu_edit_favourite);
                editFavoritesButtonClicked();
                break;
            case R.id.alltabbutton:
                trackScreen(Consts.tap_horo_menu_all);
                fireBaseLogEventScreen(Consts.tap_horo_menu_all);
                tabBarButtonClicked(Consts.menu_all);
                break;
            case R.id.favoritetabbutton:
                trackScreen(Consts.tap_horo_menu_favourite);
                fireBaseLogEventScreen(Consts.tap_horo_menu_favourite);
                tabBarButtonClicked(Consts.menu_favorite);
                break;
            default:
                break;
        }
    }

    public void refreshButtonClicked() {
        trackScreen(MgrMenu.getInstance(this).selectedHoroSignName + "_" + Consts.screen_horo_menu);
        fireBaseLogEventScreen(MgrMenu.getInstance(this).selectedHoroSignName + "_" + Consts.screen_horo_menu);
        if (MgrMenu.getInstance(this).isDateChanged()) {
            finish();
        } else if (MgrShared.getInstance(this).isNetworkAvailable()) {
            new performRefreshMenu().execute();
        } else {
            Utility.showAlert(this, getString(R.string.alert_message));
        }
    }

    public void editFavoritesButtonClicked() {
        // Hide/Show bottom buttons
        Consts.is_editing = isEditing;
        findViewById(R.id.bottomtablayout).setVisibility(isEditing ? View.VISIBLE : View.GONE);
        findViewById(R.id.menulistrefreshbutton).setVisibility(selectedTab == Consts.menu_all && isEditing ? View.VISIBLE : View.GONE);
        ((ImageButton) findViewById(R.id.menulistreditfavoritebutton)).setImageResource(isEditing ? R.drawable.editfavbutton : R.drawable.btn_fav_done);
        isEditing = !isEditing;
        if (Consts.is_editing) {
            ((ImageView) findViewById(R.id.alltabbutton)).setImageResource(selectedTab == Consts.menu_all ? R.drawable.btn_all_pressed : R.drawable.btn_all_unpressed);
            ((ImageView) findViewById(R.id.favoritetabbutton)).setImageResource(selectedTab == Consts.menu_all ? R.drawable.btn_fav_unpressed : R.drawable.btn_fav_pressed);
        }

        try {
            createMenuList(MgrMenu.getInstance(this).getMenu(Consts.is_editing ? selectedTab : Consts.menu_all));
            adapter.isEditing = (isEditing);
            adapter.items = (items);
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            Utility.showAlert(this, getString(R.string.data_not_retrieved));
        }
    }

    public void tabBarButtonClicked(int menuSelected) {
        if (selectedTab == menuSelected) {
            return;
        }

        MgrShared.getInstance(this).selectedMenu = menuSelected;
        MgrShared.getInstance(this).setStates();
        selectedTab = menuSelected;
        updateButtonImages();

        try {
            createMenuList(MgrMenu.getInstance(this).getMenu(menuSelected));
            adapter.isEditing = (false);
            adapter.items = (items);
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            Utility.showAlert(this, getString(R.string.data_not_retrieved));
        }
    }

    public void updateButtonImages() {
        findViewById(R.id.menulistrefreshbutton).setVisibility(selectedTab == Consts.menu_all ? View.VISIBLE : View.INVISIBLE);
        ((ImageView) findViewById(R.id.alltabbutton)).setImageResource(selectedTab == Consts.menu_all ? R.drawable.btn_all_pressed : R.drawable.btn_all_unpressed);
        ((ImageView) findViewById(R.id.favoritetabbutton)).setImageResource(selectedTab == Consts.menu_all ? R.drawable.btn_fav_unpressed : R.drawable.btn_fav_pressed);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
        if (MgrMenu.getInstance(this).isDateChanged() && MgrShared.getInstance(this).isNetworkAvailable()) {
            finish();
        }

        EntryItem item = (EntryItem) items.get(position);
        if (!item.isSection()) {
            if (isEditing) {
                if (item.horoId != 400 && item.horoId != 401) {
                    MgrMenu.getInstance(this).addRemoveFavorite(item.horoId);
                    adapter.notifyDataSetChanged();
                }
            } else {
                MgrMenu.getInstance(this).setSelectedHorotypeId(item.horoId);
                if (item.horoId == 400) {
                    //this option is removed per co. decision //startActivity(new Intent(this, ProductListActivity.class));
                    trackScreen(Consts.tap_horo_menu_get_reading);
                    fireBaseLogEventScreen(Consts.tap_horo_menu_get_reading);
                } else if (item.horoId == 401) {
                    trackScreen(Consts.tap_horo_menu_view_readings);
                    fireBaseLogEventScreen(Consts.tap_horo_menu_view_readings);
                    startActivity(new Intent(this, MyReadings.class));
                } else {
                    if (MgrMenu.getInstance(this).isHoroDetailsAvailable()) {
                        String navigationText = MgrMenu.getInstance(this).getHorotype().longName;
                        if (navigationText.contains(Consts.sign_name_holder)) {
                            trackScreen(navigationText.contains(Consts.sign_name_holder) ? Consts.horo_menu_video_scope : Consts.horo_menu_about + MgrMenu.getInstance(this).selectedHoroSignName + Consts.tap);
                            fireBaseLogEventScreen(navigationText.contains(Consts.sign_name_holder) ? Consts.horo_menu_video_scope : Consts.horo_menu_about + MgrMenu.getInstance(this).selectedHoroSignName + Consts.tap);
                        } else {
                            trackScreen(Consts.horo_menu + navigationText.replace(" ", "_") + Consts.tap);
                            fireBaseLogEventScreen(Consts.horo_menu + navigationText.replace(" ", "_") + Consts.tap);
                        }

                        startActivity(new Intent(this, HoroDetail.class));
                    } else {
                        if (MgrShared.getInstance(this).isNetworkAvailable()) {
                            new loadSelectedHoroscopeDetails().execute();
                        } else {
                            Utility.showAlert(this, getString(R.string.alert_message));
                        }
                    }
                }
            }
        }
    }

    public class loadSelectedHoroscopeDetails extends AsyncTask<Void, Void, String> {
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(HoroMenu.this);
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.setMessage(getApplicationContext().getString(R.string.loading_content));
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                MgrMenu.getInstance(HoroMenu.this).loadHoroscopeDetailsFromServer();
                String navigationText = MgrMenu.getInstance(HoroMenu.this).getHorotype().longName;
                if (navigationText.contains(Consts.sign_name_holder)) {
                    trackScreen(navigationText.contains(Consts.sign_name_holder) ? Consts.horo_menu_video_scope : Consts.horo_menu_about + MgrMenu.getInstance(HoroMenu.this).selectedHoroSignName + Consts.tap);
                    fireBaseLogEventScreen(navigationText.contains(Consts.sign_name_holder) ? Consts.horo_menu_video_scope : Consts.horo_menu_about + MgrMenu.getInstance(HoroMenu.this).selectedHoroSignName + Consts.tap);
                } else {
                    trackScreen(Consts.horo_menu + navigationText.replace(" ", "_") + Consts.tap);
                    fireBaseLogEventScreen(Consts.horo_menu + navigationText.replace(" ", "_") + Consts.tap);
                }
            } catch (Exception e) {
                return e.getMessage();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String errorMessage) {
            if (pDialog != null && pDialog.isShowing()) pDialog.dismiss();
            if (errorMessage.length() > 0) {
                Utility.showAlert(HoroMenu.this, getString(R.string.data_not_retrieved));
            } else {
                startActivity(new Intent(HoroMenu.this, HoroDetail.class));
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (pDialog != null && pDialog.isShowing()) pDialog.dismiss();
        }
    }

    private class performRefreshMenu extends AsyncTask<Void, Void, String> {
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(HoroMenu.this);
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.setMessage(getString(R.string.loading_content));
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                MgrMenu.getInstance(HoroMenu.this).loadMenuFromServer();
            } catch (Exception e) {
                return e.getMessage();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String errorMessage) {
            if (pDialog != null && pDialog.isShowing()) pDialog.dismiss();
            if (errorMessage.length() > 0) {
                Utility.showAlert(HoroMenu.this, getString(R.string.data_not_retrieved));
            } else {
                try {
                    createMenuList(MgrMenu.getInstance(HoroMenu.this).getMenu(Consts.menu_all));
                    adapter.items = (items);
                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Utility.showAlert(HoroMenu.this, getString(R.string.data_not_retrieved));
                }
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (pDialog != null) pDialog.dismiss();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.gc();
    }
}
