package com.astrology.iscopes.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ToggleButton;

import com.astrology.iscopes.R;
import com.astrology.iscopes.appsupport.TwitterClient;
import com.astrology.iscopes.appsupport.TwitterOAUTH;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.MgrShared;
import com.astrology.iscopes.common.Utility;
import com.quantcast.measurement.service.QuantcastClient;

import java.util.Calendar;

public class Settings extends BaseActivity implements OnClickListener, OnCheckedChangeListener {

    private SharedPreferences mSettings;
    private ToggleButton reminder;

    public AlarmManager am;
    public PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settingsscreen);
        mSettings = getSharedPreferences(TwitterOAUTH.PREFS, Context.MODE_PRIVATE);

        findViewById(R.id.button_twitter).setOnClickListener(this);
        findViewById(R.id.button_update_personalinfo).setOnClickListener(this);

        reminder = findViewById(R.id.button_reminder);
        reminder.setChecked(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("dailyReminder", false));
        reminder.setOnCheckedChangeListener(this);
    }

    private void stopNotification(Context ctx) {
        pendingIntent = PendingIntent.getBroadcast(ctx, 0, new Intent(ctx, TimeAlarm.class), PendingIntent.FLAG_CANCEL_CURRENT);
    }

    public void setRepeatingAlarm(Calendar targetCal, Context context) {
        stopNotification(context);
        am.setRepeating(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(), 24 * 60 * 60 * 1000 /*5*1000*/, pendingIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuantcastClient.activityStart(this, Consts.key_quant_cast, null, null);
        trackScreen(Consts.screen_app_settings);
        fireBaseLogEventScreen(Consts.screen_app_settings);
        updateTwitterButtonImages();
        updateDailyReminderImages();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        if (isChecked) {
            editor.putBoolean("dailyReminder", true);
            editor.apply();
            am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Calendar targetCal = Calendar.getInstance();
            targetCal.set(Calendar.HOUR_OF_DAY, 9);
            targetCal.set(Calendar.MINUTE, 0);
            setRepeatingAlarm(targetCal, Settings.this);
        } else {
            editor.putBoolean("dailyReminder", false);
            editor.commit();
            stopNotification(this);
            am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            am.cancel(pendingIntent);
        }

        reminder.setChecked(isChecked);
        updateDailyReminderImages();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_twitter:
                twitterLoginLogoutButtonClicked();
                break;
            case R.id.button_update_personalinfo:
                //EasyTracker.getInstance(this).send(Utility.getTrackerVar(Consts.app_info_screen, Consts.settings_updateinfo_tap));
                //Intent i = new Intent(this, BirthDetailActivity.class);
                //i.putExtra(Constants.update_personal_info, Constants.update_personal_info);
                //startActivity(i);
            default:
                break;
        }
    }

    public void twitterLoginLogoutButtonClicked() {
        if (MgrShared.getInstance(this).isNetworkAvailable()) {
            if (!TwitterClient.getUserLoginStatus(mSettings, TwitterClient.TWITTER_LOGIN)) {
                trackScreen(Consts.tap_settings_twitter_login);
                fireBaseLogEventScreen(Consts.tap_settings_twitter_login);
                Intent intent = new Intent(this, TwitterClient.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            } else {
                trackScreen(Consts.tap_settings_twitter_logout);
                fireBaseLogEventScreen(Consts.tap_settings_twitter_logout);
                TwitterClient.setUserLoginStatus(mSettings, TwitterClient.TWITTER_LOGIN, false);
                TwitterOAUTH.oauth_token = null;
                TwitterOAUTH.oauth_verify = null;
                updateTwitterButtonImages();
            }
        } else {
            Utility.showAlert(this, getString(R.string.alert_message));
        }
    }

    public void updateTwitterButtonImages() {
        ImageButton btn_twitter = findViewById(R.id.button_twitter);
        if (btn_twitter != null) {
            boolean isLogin = TwitterClient.getUserLoginStatus(mSettings, TwitterClient.TWITTER_LOGIN);
            btn_twitter.setImageResource(isLogin ? R.drawable.settingstwitterlogoutbutton : R.drawable.settingstwitterloginbutton);
        }
    }

    private void updateDailyReminderImages() {
        reminder.setBackgroundResource(reminder.isChecked() ? R.drawable.switch_on : R.drawable.switch_off);
    }

    @Override
    public void onStop() {
        super.onStop();
        QuantcastClient.activityStop();
    }
}