package com.astrology.iscopes.activity;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import com.astrology.iscopes.R;
import com.astrology.iscopes.common.Consts;
import com.quantcast.measurement.service.QuantcastClient;

import java.util.Calendar;

public class Splash extends BaseActivity {

    protected int _splashTime = 1000; // time to display the splash screen in ms

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Settings settings = new Settings();
        settings.am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Calendar targetCal = Calendar.getInstance();
        targetCal.set(Calendar.HOUR_OF_DAY, 9);
        targetCal.set(Calendar.MINUTE, 0);
        settings.pendingIntent = null;
        SharedPreferences.Editor editor = preferences.edit();

        if (!preferences.getBoolean("firstLaunch", false)) {
            //Calendar c = Calendar.getInstance();
            //c.add(Calendar.DATE, 10);
            // Utility.debug_log("Current time after change => " + c.getTime());
            // SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            // String formattedDate = df.format(c.getTime());
            editor.putInt("readingId", 0);
            editor.putInt("laterCount", 1);
            editor.putInt("showAnimation", 0);
            editor.putInt("notificationDay", 0);
            editor.putInt("notificationCount", 0);
            editor.putBoolean("firstLaunch", true);
            editor.putBoolean("favLaunch", true);
            editor.putBoolean("rateDialog", true);
            editor.putBoolean("dailyReminder", true);
            settings.setRepeatingAlarm(targetCal, Splash.this);
        } else {
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("dailyReminder", false)) {
                settings.setRepeatingAlarm(targetCal, Splash.this);
            }

            editor.putInt("laterCount", preferences.getInt("laterCount", 999) + 1);
        }

        editor.apply();
		
		/*
		PackageInfo info = null;
		try {
			info = getPackageManager().getPackageInfo("com.astrology.iscopes", PackageManager.GET_SIGNATURES);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		for (Signature signature : info.signatures) {
	        MessageDigest md = null;
			try {
				md = MessageDigest.getInstance("SHA");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
	        md.update(signature.toByteArray()); // Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
	    }*/

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), SignWheel.class));
                finish();
            }
        }, _splashTime);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public void onStart() {
        super.onStart();
        QuantcastClient.activityStart(this, Consts.key_quant_cast, null, null);
        trackScreen(getString(R.string.home_screen));
        fireBaseLogEventScreen(getString(R.string.home_screen));
    }

    @Override
    public void onStop() {
        super.onStop();
        QuantcastClient.activityStop();
    }
}
