package com.astrology.iscopes.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AlertDialog;

import com.astrology.iscopes.R;
import com.astrology.iscopes.appsupport.TwitterClient;
import com.astrology.iscopes.appsupport.TwitterStatus;
import com.astrology.iscopes.bo.Product;
import com.astrology.iscopes.bo.Purchase;
import com.astrology.iscopes.bo.Reading;
import com.astrology.iscopes.bo.Section;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.MgrReadings;
import com.astrology.iscopes.common.MgrShared;
import com.astrology.iscopes.common.Utility;
import com.astrology.iscopes.database.IScopesDB;
import com.astrology.iscopes.orm.Birthdetails;
import com.quantcast.measurement.service.QuantcastClient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

public class ReadingReport extends BaseActivity implements OnClickListener {

    private int hashTableIndex;
    private String hashTableKey;
    private Reading reading = null;
    private WebView webView;
    private boolean publish = false;
    private ArrayList<Birthdetails> birthInfoArray;
    private Hashtable<String, ArrayList<Purchase>> purchases;
    private Product p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.readingreportscreen);
        Bundle extras = getIntent().getExtras();

        String html_color_white = getString(R.string.html_color_white);
        findViewById(R.id.sharereadingreportbutton).setOnClickListener(this);
        webView = findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.clearCache(true);
        webView.clearView();
        webView.setVerticalScrollBarEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, final String url) {
                webView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                webView.setVisibility(View.GONE);
            }
        });

        String activity_name = extras.containsKey(Consts.activity_name) ? extras.getString(Consts.activity_name) : "";
        if (activity_name.equalsIgnoreCase(Consts.activity_personal_info)) {
            String productType = extras.getString("ProductType");
            try {
                purchases = MgrReadings.getSharedObject(getApplicationContext()).getPurchasedProducts();
                if (purchases.size() > 0) {
                    ArrayList<Purchase> purchaseList = purchases.get(productType);
                    birthInfoArray = purchaseList.get(purchaseList.size() - 1).birthInfoArray;
                    if (birthInfoArray.size() > 0) {
                        int readingId = purchaseList.get(purchaseList.size() - 1).readingId;
                        String imageURL = purchaseList.get(0).productImageURL;
                        reading = MgrReadings.getSharedObject(getApplicationContext()).getPurchasedReadings(readingId);
                        int productID = Integer.parseInt(extras.getString("productID"));
                        p = IScopesDB.getInstance(getApplicationContext()).getPurchasedProduct(productID);
                        populateReadingModel(imageURL, readingId, html_color_white);
                    } else endIt();
                } else endIt();
            } catch (Exception e) {
                e.printStackTrace();
                endIt();
            }
        } else if (activity_name.equalsIgnoreCase(Consts.activity_my_reading)) {
            purchases = MgrReadings.getSharedObject(getApplicationContext()).getPurchasedProducts();
            if (purchases.size() > 0) {
                hashTableKey = extras.getString(Consts.hash_table_key);
                hashTableIndex = extras.getInt(Consts.hash_table_index);
                Purchase purchase = purchases.get(hashTableKey).get(hashTableIndex);
                try {
                    birthInfoArray = purchase.birthInfoArray;
                    if (birthInfoArray.size() > 0) {
                        p = IScopesDB.getInstance(getApplicationContext()).getPurchasedProduct(purchase.productID);
                        reading = MgrReadings.getSharedObject(getApplicationContext()).getPurchasedReadings(purchase.readingId);
                        String productImageURL = purchase.productImageURL;
                        populateReadingModel(productImageURL, purchase.readingId, html_color_white);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    endIt();
                }
            } else {
                endIt();
            }
        }

    }

    public void endIt() {
        webView.setVisibility(View.GONE);
        showDialog(getString(R.string.alert_title), getString(R.string.alert_error), true);
    }

    private void populateReadingModel(String productImageURL, int readingId, String innerSectionTextColor) {
        String htmlString = "<img style=\"border:3px solid #173E8C\" src=\'" + productImageURL + "\' width=\"120\" height=\"90\"align=\"left\"/>";
        if (birthInfoArray.size() > 0) {
            Birthdetails person1Info = birthInfoArray.get(0);
            String birthState = person1Info.birthCountry.equalsIgnoreCase("US") ? person1Info.birthState + ", " : "";
            htmlString = htmlString + (birthInfoArray.size() == 1 ? "</br>" : "")
                    + "<div><p style='word-wrap:break-word;word-break:break-all;margin-left:120px;margin-top:0px;margin-bottom:0px;padding-left:12px;text-align:left;font-family:Helvetica-Bold;font-size:10.5pt;color: white'>"
                    + person1Info.birthName + "</p>"
                    + "<p style='text-align:left;margin-left:120px;margin-top:3px;margin-bottom:2px;font-family:Helvetica-Bold;padding-left:12px;font-size:9pt;color: grey'>"
                    + person1Info.birthDate + "</br>"
                    + (person1Info.birthTime.equals("00:00") ? "" : (Utility.get12HoursTime(person1Info.birthTime) + "</br>"))
                    + person1Info.birthCity + ", "
                    + birthState
                    + person1Info.birthCountry;
            if (birthInfoArray.size() > 1) {
                htmlString = htmlString + "<div style='font-family:Helvetica-Bold;font-size:2pt;'> &nbsp;</div>";
                Birthdetails person2Info = birthInfoArray.get(1);
                birthState = person2Info.birthCountry.equalsIgnoreCase("US") ? person2Info.birthState + ", " : "";
                String secondPersonLocation = person2Info.birthCity + ", " + birthState + person2Info.birthCountry;

                htmlString = htmlString
                        + "<p style='word-wrap:break-word;margin-left:120px;margin-top:0px;margin-bottom:0px;text-align:left;font-family:Helvetica-Bold;padding-left:12px;font-size:10.5pt;color: white'>"
                        + person2Info.birthName + "</p>"
                        + "<p style='margin-left: 120px;margin-top:3px;padding:0;font-family:Helvetica-Bold;padding-left:12px;font-size:9pt;color: grey'>"
                        + person2Info.birthDate
                        + (person2Info.birthTime.equals("") ? "" : (Utility.get12HoursTime(person2Info.birthTime) + "</br>"))
                        + secondPersonLocation + "</p></div>";
            }

            if (birthInfoArray.size() == 1) {
                htmlString = htmlString + "<div><br/><br/></div>";
            }

            if (reading == null) {
                htmlString = "<span style=\"text-align:justify;font-family:Helvetica-Bold;font-size:15pt;line-height: 20pt;color:"
                        + innerSectionTextColor + "\">Sorry, your reading is not available at the moment.</span>";
            } else {
                try {
                    String byLine = reading.cover.byLine;
                    String interpretation = byLine.substring(0, byLine.indexOf("text by"));
                    String interpretor = byLine.substring(interpretation.length() + "text by".length());
                    htmlString = htmlString
                            + "<span style=\"word-wrap:break-word;text-align:left;font-family:Helvetica-Bold;font-size:20pt;line-height: 20pt;color: white\"><center>"
                            + reading.cover.title + "</center></span></br>"
                            + "<span style=\"text-align:justify;font-family:Helvetica;font-size:14pt;line-height: 1pt;color:"
                            + innerSectionTextColor + "\"><center>"
                            + interpretation + " text by </center></span></br>"
                            + "<span style=\"text-align:justify;font-family:Helvetica;font-size:14pt;line-height: 1pt;color:"
                            + innerSectionTextColor + "\"><center>"
                            + interpretor + "</center></span></br>";
                    // end of try
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                ArrayList<ArrayList<Section>> outerSectionList = reading.interpretation;
                outerSectionList.trimToSize();

                try {
                    // this loop will traverse 12 outSecion lists of type
                    for (ArrayList<Section> innerSectionList : outerSectionList) {
//						if (!outerSectionList.get(i).isEmpty()) {
                        String title = innerSectionList.get(0).title;
                        htmlString = htmlString + "<style>table {color:" + innerSectionTextColor
                                + ";width:250px !important;}</style><span style=\"text-align:left;font-family:Helvetica-Bold;font-size:16pt;color:rgb(237,20,90)\"><br/><br/><b><center>"
                                + title + "</center></b></span>";

                        int sectionIndex = 0;
                        innerSectionList.trimToSize();
                        for (Section sectionObject : innerSectionList) {
                            ArrayList<String> imagePathList = sectionObject.imagePaths;
                            imagePathList.trimToSize();
                            String imagePathsHtml = "";
                            for (String imgPath : imagePathList)
                                imagePathsHtml = imagePathsHtml + "<img style=margin-right:5px; align=\"left\" src=\'" + imgPath + "\'/>";

                            if (imagePathList.size() > 0) {
                                htmlString = htmlString + (sectionIndex == 0 ? "</br>" : "</br></br>") + imagePathsHtml;
                            }

                            htmlString = htmlString
                                    + "<span style=\"width:300px;word-wrap:break-word;text-align:left;font-family:Helvetica;font-size:14pt;color:"
                                    + innerSectionTextColor + "\">"
                                    + sectionObject.description
                                    + "</span>";
                            sectionIndex++;
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } // end of else
        }

        webView.setBackgroundColor(0);
        webView.loadDataWithBaseURL(null, htmlString, "text/html", "utf-8", null);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("x-position", webView.getScrollX());
        outState.putInt("y-position", webView.getScrollY());
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.sharereadingreportbutton) {
            trackScreen(Consts.tap_reading_report_share);
            fireBaseLogEventScreen(Consts.tap_reading_report_share);
            shareReadingDetail();
        }
    }

    private void shareReadingDetail() {
        new AlertDialog.Builder(this)
                .setTitle("Share Reading")
                .setItems(Utility.share_items(this), shareReadingDialog)
                .show();
    }

    public DialogInterface.OnClickListener shareReadingDialog = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int item) {
            switch (item) {
                case 0:
                    trackScreen(Consts.tap_reading_report_share_by_email);
                    fireBaseLogEventScreen(Consts.tap_reading_report_share_by_email);
                    shareReadingDetailByEmail();
                    break;
                case 1:
                    trackScreen(Consts.tap_reading_report_share_by_twitter);
                    fireBaseLogEventScreen(Consts.tap_reading_report_share_by_twitter);
                    shareReadingDetailByTwitter();
                    break;
                default:
                    break;
            }
        }
    };

    // Sharing through email
    public void shareReadingDetailByEmail() {
        if (MgrShared.getInstance(this).isNetworkAvailable()) {
            String shareText = formatReadingForEmail();
            if (!shareText.equals(null)) {
                shareText = "<br/><p>I want to share this " + p.name + " Reading with you.<p><br/>" + shareText
                        + "<br/><p>Get it now in Horoscopes by Astrology.com for iPhone, iPad, and iPod Touch: <br/> "
                        + "<a href = http://www.astrology.com/getapp/iphone/horoscopes>http://www.astrology.com/getapp/iphone/horoscopes</a>"
                        + "<br/><p> Or on the web:<br/> <a href =" + Utility.FormatProductSharingURL(Consts.url_product_sharing, p) + ">"
                        + Utility.FormatProductSharingURL(Consts.url_product_sharing, p) + "</a></p>"
                        + "<br/> --- <p>Sent from Horoscopes by <a href = www.astrology.com> Astrology.com</a></p>";
            }
            final Intent it = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
            it.putExtra(Intent.EXTRA_SUBJECT, p.name + " " + getString(R.string.reading_from_astrology));
            it.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(shareText));
            this.startActivity(it);
        } else {
            endIt();
        }
    }

    public void shareReadingDetailByTwitter() {
        if (MgrShared.getInstance(this).isNetworkAvailable()) {
            Intent intent = new Intent(getApplicationContext(), TwitterClient.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.putExtra(Consts.activity_source, Consts.activity_my_reading);
            String str = p.description; // Share text for twitter
            if (str != null && str.length() > 0) {
                str = Utility.removeHTMLTags(str);
                if (str.length() > 100) str = str.substring(0, 100);
                str += " ... ";
            }
            TwitterStatus twitterStatus = TwitterStatus.getInstance();
            twitterStatus.twitterPostURL = (Utility.FormatProductSharingURL(Consts.url_product_sharing, p));
            twitterStatus.twitterPostHeading = (str);
            startActivity(intent);
        } else {
            endIt();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (publish) {
//			showpostNewDialog();
            publish = false;
        }
    }

    private String formatReadingForEmail() {
        String htmlForEmail = "";
        ArrayList<Birthdetails> birthInfoArray = null;
        if (getIntent().getExtras().get(Consts.activity_name).equals(Consts.activity_my_reading)) {
            birthInfoArray = purchases.get(hashTableKey).get(hashTableIndex).birthInfoArray;
        } else if (getIntent().getExtras().get(Consts.activity_name).equals(Consts.activity_personal_info)) {
            birthInfoArray = this.birthInfoArray;
        }

        if (birthInfoArray.size() > 0) {
            Birthdetails person1Info = birthInfoArray.get(0);
            if (birthInfoArray.size() == 1) htmlForEmail = htmlForEmail + "<br/>";
            htmlForEmail = htmlForEmail
                    + "<div><p style='word-wrap:break-word;word-break:break-all;margin-left:120px;margin-top:0px;margin-bottom:0px;padding-left:12px;text-align:center !important;font-family:Helvetica-Bold;font-size:10.5pt;color: black'><b>"
                    + person1Info.birthName + "</b></p>"
                    + "<br/><center><p style='text-align:center;margin-left:120px;margin-top:3px;margin-bottom:2px;font-family:Helvetica-Bold;padding-left:12px;font-size:9pt;color:black'>"
                    + person1Info.birthDate + " "
                    + (person1Info.birthTime.equals("00:00") ? "" : (Utility.get12HoursTime(person1Info.birthTime) + "<br/>"))
                    + person1Info.birthCity + ", "
                    + person1Info.birthCountry;

            if (birthInfoArray.size() > 1) {
                htmlForEmail = htmlForEmail + "<div style='font-family:Helvetica-Bold;font-size:2pt;'> &nbsp;</div>";
                Birthdetails person2Info = birthInfoArray.get(1);
                String secondPersonLocation = person2Info.birthCity + ", " + person2Info.birthCountry;
                htmlForEmail = htmlForEmail
                        + "<center><p style='word-wrap:break-word;margin-left:120px;margin-top:0px;margin-bottom:0px;text-align:center;font-family:Helvetica-Bold;padding-left:12px;font-size:10.5pt;color:black'><b>"
                        + person2Info.birthName
                        + "</b></p> </center>"
                        + "<br/><center><p style='margin-left: 120px;margin-top:3px;padding:0;font-family:Helvetica-Bold;padding-left:12px;font-size:9pt;color:black'>"
                        + person2Info.birthDate
                        + (!person2Info.birthTime.equals("") ? Utility.get12HoursTime(person2Info.birthTime) : "") + "<br/>"
                        + secondPersonLocation + "</p></center></div>";
            }

            if (birthInfoArray.size() == 1) {
                htmlForEmail = htmlForEmail + "<br/>";
            }

            if (reading == null) {
                htmlForEmail = "<span style=\"text-align:justify;font-family:Helvetica-Bold;font-size:15pt;line-height: 20pt;color:black\"> Sorry, your reading is not available at the moment.</span>";
            } else {
                try {
                    String byLine = reading.cover.byLine;
                    String interpretation = byLine.substring(0, byLine.indexOf("text by"));
                    String interpretor = byLine.substring(interpretation.length() + "text by".length());
                    htmlForEmail = htmlForEmail
                            + "<span style=\"word-wrap:break-word;text-align:center;font-family:Helvetica-Bold;font-size:20pt;line-height: 20pt;color: black\"><center><b>"
                            + reading.cover.subTitle + "</b></center></span><br/>"
                            + "<br/><span style=\"text-align:justify;font-family:Helvetica;font-size:14pt;line-height: 1pt;color:black\"><center><b>"
                            + interpretation + " text by </center></span>"
                            + "<span style=\"text-align:justify;font-family:Helvetica;font-size:14pt;line-height: 1pt;color:black\"><center>"
                            + interpretor + "</b></center></span>";
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                ArrayList<ArrayList<Section>> outerSectionList = reading.interpretation;
                outerSectionList.trimToSize();

                // exception handling if reading is being purchased but no interpretations are coming
                try {
                    // this loop will traverse 12 outSecion lists of type ArrayList<Sections>
                    for (ArrayList<Section> outerSections : outerSectionList) {
                        String title = outerSections.get(0).title;
                        htmlForEmail = htmlForEmail
                                + "<br/><br/><span style=\"font-family:Helvetica-Bold;font-size:16pt;color:#ED145A\"><b><center>"
                                + title + "</center></b></span>";

                        ArrayList<Section> innerSectionList = outerSections;
                        innerSectionList.trimToSize();
                        for (Section sectionObject : innerSectionList) {
                            htmlForEmail = htmlForEmail
                                    + "<br/><br/><span style=\"width:300px;word-wrap:break-word;text-align:left;font-family:Helvetica;font-size:14pt;color:black\">"
                                    + sectionObject.description + "</span>";
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }// end of else
        }

        return htmlForEmail;
    }

    private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
        for (String string : subset) {
            if (!superset.contains(string)) {
                return false;
            }
        }

        return true;
    }

    private void showDialog(String title, String message, boolean finish) {
        new AlertDialog.Builder(ReadingReport.this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new onClickListener(finish))
                .show();
    }

    public class onClickListener implements DialogInterface.OnClickListener {
        boolean finish;

        public onClickListener(boolean finish) {
            this.finish = finish;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (finish) finish();
            else dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Utility.FINISH_ACTIVITY);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        QuantcastClient.activityStart(this, Consts.key_quant_cast, null, null);
        trackScreen(Consts.screen_reading_report);
        fireBaseLogEventScreen(Consts.screen_reading_report);
    }

    @Override
    public void onStop() {
        super.onStop();
        QuantcastClient.activityStop();
    }
}