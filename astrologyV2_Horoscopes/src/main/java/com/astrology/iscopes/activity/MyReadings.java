package com.astrology.iscopes.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.astrology.iscopes.R;
import com.astrology.iscopes.bo.ListItem;
import com.astrology.iscopes.bo.MyReadingsEntryItem;
import com.astrology.iscopes.bo.Product;
import com.astrology.iscopes.bo.Purchase;
import com.astrology.iscopes.bo.SectionItem;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.ListAdapterReadings;
import com.astrology.iscopes.common.MgrReadings;
import com.astrology.iscopes.database.IScopesDB;
import com.astrology.iscopes.orm.Birthdetails;
import com.quantcast.measurement.service.QuantcastClient;

import java.util.ArrayList;
import java.util.Hashtable;

public class MyReadings extends BaseActivity implements OnItemClickListener, OnItemLongClickListener {

    ArrayList<ListItem> items = new ArrayList<>();
    ListAdapterReadings adapter;
    Hashtable<String, ArrayList<Purchase>> purchases;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myreadingscreen);
        /* If readings are not bought then show "Get Personalized Reading" button
         * else show the bought readings list */
        if (MgrReadings.getSharedObject(this).getPurchasedProducts().size() == 0) {
            findViewById(R.id.myreadings_empty_screen_notification).setVisibility(TextView.VISIBLE);
            ListView menuList = findViewById(R.id.myreadings_listview);
            menuList.setVisibility(ListView.GONE);
        } else {
            adapter = new ListAdapterReadings(this, items);
            ListView readingsList = findViewById(R.id.myreadings_listview);
            readingsList.setAdapter(adapter);

            readingsList.setOnItemLongClickListener(this);
            readingsList.setOnItemClickListener(this);
            createMyReadingList();
        }
    }

    private void createMyReadingList() {
        items.removeAll(items);
        purchases = MgrReadings.getSharedObject(getApplicationContext()).getPurchasedProducts();
        ArrayList<Product> productTypes = IScopesDB.getInstance(getApplicationContext()).getProductTypes();
        productTypes.trimToSize();

        for (int i = 0; i < purchases.size(); i++) {
            items.add(new SectionItem(productTypes.get(i).name));
            ArrayList<Purchase> purchasedReadings = purchases.get(productTypes.get(i).name);
            purchasedReadings.trimToSize();
            for (Purchase purchase : purchasedReadings) {
                ArrayList<Birthdetails> birthInfoArray = purchase.birthInfoArray;
                birthInfoArray.trimToSize();
                String birthInfo = "For ";

                for (Birthdetails bdetails : birthInfoArray) {
                    if (birthInfoArray.indexOf(bdetails) > 0) birthInfo = birthInfo + " and ";
                    birthInfo = birthInfo + bdetails.birthName;
                }

                items.add(new MyReadingsEntryItem(birthInfo, purchase.purchaseTime, productTypes.get(i).name, purchase.readingId, purchasedReadings.indexOf(purchase)));
            }
        }

        items.trimToSize();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View view, final int position, long rowId) {
        new AlertDialog.Builder(MyReadings.this)
                .setTitle(getString(R.string.title_delete))
                .setMessage(getString(R.string.message_delete))
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteReading(position);
                        adapter.notifyDataSetChanged();

                        // displaying "No readings yet." notification if all the products have been deleted.
                        if (items.size() == 0) {
                            purchases.clear();
                            findViewById(R.id.myreadings_empty_screen_notification).setVisibility(TextView.VISIBLE);
                            findViewById(R.id.myreadings_listview).setVisibility(ListView.GONE);
                        }
                    }
                }).show();
        return true;
    }

    private boolean deleteReading(int itemPosition) {
        String readingType = ((MyReadingsEntryItem) items.get(itemPosition)).readingType;
        int readingId = ((MyReadingsEntryItem) items.get(itemPosition)).readingId;
        for (int i = 0; i < purchases.get(readingType).size(); i++) {
            if (purchases.get(readingType).get(i).readingId == readingId) {
                purchases.get(readingType).remove(i);
                items.remove(itemPosition);
                // deleting reading from database
                // this function will also remove a product type if its all the readings have been deleted.
                MgrReadings.getSharedObject(getApplicationContext()).deletePurchasedReading(readingId, readingType);

                // deleting the product type from if there are no readings in  it.
                if (purchases.get(readingType).size() == 0) {
                    items.remove(itemPosition - 1);
                    purchases.remove(readingType);
                }

                return true;
            }
        }

        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View view, int position, long rowId) {
        Intent readingReportIntent = new Intent(this, ReadingReport.class);
        readingReportIntent.putExtra(Consts.activity_name, Consts.activity_my_reading);
        readingReportIntent.putExtra(Consts.hash_table_key, ((MyReadingsEntryItem) items.get(position)).readingType);
        readingReportIntent.putExtra(Consts.hash_table_index, ((MyReadingsEntryItem) items.get(position)).hashTableIndex);
        startActivity(readingReportIntent);
    }

    @Override
    public void onStart() {
        super.onStart();
        QuantcastClient.activityStart(this, Consts.key_quant_cast, null, null);
        trackScreen(Consts.screen_my_readings);
        fireBaseLogEventScreen(Consts.screen_my_readings);
    }

    @Override
    public void onStop() {
        super.onStop();
        QuantcastClient.activityStop();
        MyApplication.getInstance().closeDB();
    }
}
