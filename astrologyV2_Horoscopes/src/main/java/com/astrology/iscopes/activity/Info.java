package com.astrology.iscopes.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.astrology.iscopes.R;
import com.astrology.iscopes.bo.FeedbackEntryItem;
import com.astrology.iscopes.bo.ListItem;
import com.astrology.iscopes.bo.SectionItem;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.ListAdapterFeedback;
import com.astrology.iscopes.common.MgrShared;
import com.astrology.iscopes.common.Utility;
import com.quantcast.measurement.service.QuantcastClient;

import java.util.ArrayList;

public class Info extends BaseActivity implements OnItemClickListener {

    private static String[] infoTitle;
    private static String[] infoDetail;
    private static String[] infoIcon;
    private static ArrayList<ListItem> items = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infoscreen);

        infoTitle = getResources().getStringArray(R.array.infotitle);
        infoDetail = getResources().getStringArray(R.array.infodetail);
        infoIcon = getResources().getStringArray(R.array.infoicon);

        createFeedbackList();
        ListView list_view = findViewById(R.id.info_listView);
        list_view.setAdapter(new ListAdapterFeedback(this, items));
        list_view.setOnItemClickListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        QuantcastClient.activityStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuantcastClient.activityStart(this, Consts.key_quant_cast, null, null);
        trackScreen(Consts.screen_app_info);
        fireBaseLogEventScreen(Consts.screen_app_info);
    }

    public void createFeedbackList() {
        //items.removeAll(items);
        if (!items.isEmpty()) {
            return;
        }

        for (int i = 0; i < infoTitle.length; i++)
            items.add(infoTitle[i].length() == 0 ? new SectionItem("") : new FeedbackEntryItem(infoTitle[i], infoDetail[i], infoIcon[i]));
    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View v, int position, long id) {
        switch (position) {
            case 0:
                trackScreen(Consts.tap_app_info_about);
                fireBaseLogEventScreen(Consts.tap_app_info_about);
                startActivity(new Intent(this, About.class));
                break;
            case 2:
                trackScreen(Consts.tap_app_info_feedback);
                fireBaseLogEventScreen(Consts.tap_app_info_feedback);
                openFeedbackEmail();
                break;
            case 3:
                trackScreen(Consts.tap_app_info_tell_friend);
                fireBaseLogEventScreen(Consts.tap_app_info_tell_friend);
                openTellAFriendEmail();
                break;
            case 5:
                trackScreen(Consts.tap_app_info_go_to_astrology);
                fireBaseLogEventScreen(Consts.tap_app_info_go_to_astrology);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.astrology_uri))));
                break;
            default:
                break;
        }
    }

    public void openFeedbackEmail() {
        String subject = "Feedback (" + getString(R.string.app_name) + " " + Utility.getVersionNumber(getApplicationContext()) + ")";
        String msg = getString(R.string.feedback_text, Build.MODEL, android.os.Build.VERSION.RELEASE);
        sendEmail(subject, msg, new String[]{Consts.email_feedback});
    }

    public void openTellAFriendEmail() {
        sendEmail(getString(R.string.app_name), getString(R.string.tell_friend_text) + "\n", new String[]{});
    }

    public void sendEmail(String subject, String msg, String[] to) {
        if (!MgrShared.getInstance(this).isNetworkAvailable())
            Utility.showAlert(this, getString(R.string.alert_message));
        else {
            Intent it = new Intent(Intent.ACTION_SEND);
            it.putExtra(Intent.EXTRA_EMAIL, to);
            it.putExtra(Intent.EXTRA_SUBJECT, subject);
            it.putExtra(Intent.EXTRA_TEXT, msg);
            it.setType("text/plain");
            startActivity(it);
        }
    }
}