package com.astrology.iscopes.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.astrology.iscopes.R;
import com.astrology.iscopes.appsupport.TwitterOAUTH;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.Utility;

public class CustomWebView extends AppCompatActivity {

    private boolean isRedirecting = false;
    private ProgressDialog dialog;
    private WebView webView;
    private String source;

    @SuppressWarnings("deprecation")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_web_view);
        String uri = getIntent().getExtras().getString(Consts.uri);
        source = Utility.getString(getIntent().getExtras(), Consts.activity_source, "notfound");

        webView = findViewById(R.id.cus_webview);
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.alert_loading_please_wait));
        dialog.show();

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        webView.getSettings().setSupportMultipleWindows(false);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSavePassword(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(true);
        webView.getSettings().setUseWideViewPort(false);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.setWebChromeClient(new MyWebChromeClient());

        webView.clearCache(true);
        webView.clearHistory();
        webView.loadUrl(uri);
        webView.setWebViewClient(new MyWebView());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        webView.stopLoading();
    }

    private class MyWebChromeClient extends WebChromeClient {
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            result.confirm();
            return true;
        }

        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            if (newProgress >= 90 && dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            if (title.equalsIgnoreCase(getString(R.string.twitter_redirecting))) {
                isRedirecting = true;
            }
        }
    }

    private class MyWebView extends WebViewClient {
        public void onPageFinished(WebView view, String url) {
            if (!isRedirecting) {
                super.onPageFinished(view, url);
            }

            //if (url.contains(Constants.url_contains))
            //shouldOverrideUrlLoading(view, url);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (!isRedirecting) {
                if (!dialog.isShowing()) {
                    dialog = ProgressDialog.show(CustomWebView.this, "", getString(R.string.loading_please_wait), true, true);
                }

                super.onPageStarted(view, url, favicon);
            }

            if (url.contains(Consts.url_contains)) {
                shouldOverrideUrlLoading(view, url);
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            super.shouldOverrideUrlLoading(view, url);
            boolean flag = true;

            if (url.contains(Consts.url_key)) {
                webView.stopLoading();
                String oauth_token = "oauth_token="; //Constants.oauth_token;
                String oauth_verify = "oauth_verifier="; //Constants.oauth_verifier;
                String[] querystring = url.substring(url.indexOf(oauth_token)).split("&");
                try {
                    //http://ww38.iscopes.com/1?oauth_token=0nlgGgAAAAAARNOGAAABTkZO1wk&oauth_verifier=DQijmhAQcFe2nejQVXCEw7yMr679VfwC
                    TwitterOAUTH.oauth_token = querystring[0].replace(oauth_token, ""); //oauth;
                    TwitterOAUTH.oauth_verify = querystring[1].replace(oauth_verify, ""); //subsub;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(new Intent(CustomWebView.this, TwitterOAUTH.class).putExtra(Consts.activity_source, source));
                finish();

            } else if (url.startsWith(Consts.url_more)
                    || url.startsWith("https://twitter.com/signup?context=oauth&oauth_token")
                    || url.contains("https://twitter.com/signup?context=webintent")
                    || url.contains("https://api.twitter.com/account/resend_password")
                    || url.contains("https://twitter.com/account/resend_password")
                    || url.contains("https://twitter.com/settings/applications")
                    || url.contains("https://twitter.com/tos")
                    || url.contains("https://twitter.com/privacy")
                    || url.contains("https://twitter.com/signup?context=webintent")
                    || url.contains("https://api.twitter.com/intent/session?return_to=%2Foauth%2Fauthorize")
                    || url.contains("https://twitter.com/home")
                    || url.contains("http://www.astrology.com/")
                    || url.contains("http://iscopes.com/1?denied")) {
                webView.stopLoading();
                if (!url.contains("http://iscopes.com/1?denied")) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                } else {
                    finish();
                }
            } else {
                view.loadDataWithBaseURL(url, null, null, null, null);
                flag = true;
            }

            return flag;
        }
    }
}
