package com.astrology.iscopes.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;

import com.astrology.iscopes.R;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.MgrMenu;
import com.astrology.iscopes.common.MgrReadings;
import com.astrology.iscopes.common.MgrShared;
import com.astrology.iscopes.common.Utility;
import com.astrology.iscopes.database.IScopesDB;
import com.astrology.iscopes.orm.PendingPurchases;
import com.astrology.iscopes.usercontrol.SpinWheel;
import com.quantcast.measurement.service.QuantcastClient;

import java.util.ArrayList;

public class SignWheel extends BaseActivity implements OnClickListener, OnGestureListener {

    public boolean hasImageScalled = false;
    public static double savedRotatedDegrees = 0;
    public static final String PREFS = "SignWheelSharedPreferences";

    //private static final int SWIPE_SIGN_RANGE = 60;
    private static final int SWIPE_MIN_DISTANCE = 0;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 900;
    private static final double ONE_SIGN_MAX_VELOCITY = 1800;
    private static final double TWO_SIGN_MAX_VELOCITY = 2500;
    private static final double THREE_SIGN_MAX_VELOCITY = 3500;

    private int duration = 600;
    private int screenWidth;
    private int screenHeight;

    private SpinWheel spWheel;
    private GestureDetector gestureScanner;

    private MotionEvent swipeStartEvent = null;
    private Rect rectangle;

    //private Runnable loadDataThread;
    private float totalScrollAngle;
    private static SharedPreferences signWheelPreferences;

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signwheelscreen);
        gestureScanner = new GestureDetector(this);
        gestureScanner.setIsLongpressEnabled(false);

        spWheel = findViewById(R.id.wheelSpin);
        spWheel.setLongClickable(false);
        signWheelPreferences = getSharedPreferences(PREFS, Activity.MODE_PRIVATE);

        findViewById(R.id.refreshbutton).setOnClickListener(this);
        findViewById(R.id.infobutton).setOnClickListener(this);
        // new loadPendingReadings().execute();

        if (!getPrefFirstLaunchValue()) {
            SharedPreferences.Editor editor = signWheelPreferences.edit();
            editor.putBoolean(getString(R.string.first_launch), true);
            editor.apply();
        }

        if (MgrShared.getInstance(this).selectedSign != 0 && MgrShared.getInstance(this).selectedSign != spWheel.selectedSign) {
            // On first launch select sign that was selected on the last launch
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    spWheel.rotateSignWheel(-1 * MgrShared.getInstance(SignWheel.this).selectedSign, duration);
                }
            }, 500);
        }
    }

    public int getPointX(double x) {
        double newX = (x * 100) / 480;
        return (int) ((newX / 100) * screenWidth);
    }

    public int getPointY(double y) {
        double newY = (y * 100) / 800;
        return (int) ((newY / 100) * screenHeight);
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onResume() {
        super.onResume();
        Display display = getWindowManager().getDefaultDisplay();
        screenWidth = display.getWidth();
        screenHeight = display.getHeight();
        rectangle = new Rect(getPointX(100), getPointY(150), getPointX(365), getPointY(450));
        loadMenu();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.getBoolean("rateDialog", false) && preferences.getInt("laterCount", 999) % 15 == 0) {
            showAppRateDialog();
        }
    }

    private void showAppRateDialog() {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Rate Horoscopes");
//	    builder.setIcon(R.drawable.icon);
        builder.setMessage(getString(R.string.alert_ratethisapp));
        builder.setPositiveButton("Rate Horoscopes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.astrology.iscopes")));
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("rateDialog", false);
                        editor.apply();
                        dialog.cancel();
                    }
                });

        builder.setNeutralButton("Remind Me Later",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("laterCount", preferences.getInt("laterCount", 999) + 1);
                        editor.apply();
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton("No, Thanks",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("rateDialog", false);
                        editor.apply();
                        dialog.cancel();
                    }
                });
        builder.show();
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.refreshbutton:
                refreshButtonClicked();
                break;
            case R.id.infobutton:
                trackScreen(Consts.tap_horo_sign_info);
                fireBaseLogEventScreen(Consts.tap_horo_sign_info);
                startActivity(new Intent(this, InfoTab.class));
                break;
            case R.id.wheelItem_Btn:
                signWheelButtonPressed();
                break;
            default:
                break;
        }
    }

    public void refreshButtonClicked() {
        if (MgrShared.getInstance(this).isNetworkAvailable()) {
            new performClearCacheAndLoadingMenu("").execute();
        } else {
            Utility.showAlert(this, getString(R.string.alert_message));
        }

        trackScreen(Consts.tap_horo_sign_refresh);
        fireBaseLogEventScreen(Consts.tap_horo_sign_refresh);
    }

    public void signWheelButtonPressed() {
        if (!MgrMenu.getInstance(this).isMenuAvailable()) {
            if (MgrShared.getInstance(this).isNetworkAvailable()) {
                new performLoadingMenu(getString(R.string.content_not_downloaded)).execute();
            } else {
                Utility.showAlert(this, getString(R.string.alert_message));
            }
        } else if (MgrMenu.getInstance(this).isDateChanged() && MgrShared.getInstance(this).isNetworkAvailable()) {
            String pDialogMessage = getPrefFirstLaunchValue() ? "" : getString(R.string.date_has_changed);
            new performClearCacheAndLoadingMenu(pDialogMessage).execute();
        } else {
            final ProgressDialog pDialog = new ProgressDialog(SignWheel.this);
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.setMessage(getString(R.string.loading_please_wait));
            pDialog.show();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    MgrShared.getInstance(SignWheel.this).selectedSign = spWheel.selectedSign;
                    MgrShared.getInstance(SignWheel.this).setStates();
                    MgrMenu.getInstance(SignWheel.this).setSelectedHorosignId(spWheel.selectedSign);
                    String ui_value = MgrMenu.getInstance(SignWheel.this).isDateChanged() ? "" : "_" + Consts.horo_sign + Consts.signs.get(MgrShared.getInstance(SignWheel.this).selectedSign).name() + Consts.tap;
                    if (ui_value.length() > 0) {
                        trackScreen(Consts.screen_horo_sign + ui_value);
                        fireBaseLogEventScreen(Consts.screen_horo_sign + ui_value);
                    }

                    pDialog.dismiss();
                    startActivity(new Intent(getApplicationContext(), HoroMenu.class));
                }
            }).start();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent me) {
        spWheel.invalidate();
        spWheel.refreshDrawableState();
        if (me.getAction() == MotionEvent.ACTION_MOVE) {
            swipeStartEvent = me;
        }

        if (me.getAction() == MotionEvent.ACTION_UP) {
            // Log.d("___onUp", ".angle: " + totalScrollAngle);
            scaleImageDown();

            if (hasImageScalled) {
                hasImageScalled = false;
				/*if (metrics.widthPixels < metrics.heightPixels) imgButton.setImageDrawable(signWheelButton.getDrawable()); */
            }

            if (totalScrollAngle == 0 && rectangle.contains((int) me.getX(), (int) me.getY())) {
                // //Log.i("****rectangle clicked","..");
                signWheelButtonPressed();
            }

            // for left to right
            if (totalScrollAngle > 0) {
                // Log.e("total angle Scroll: ", "" + totalScrollAngle); // reverse right

                float deltaAngle = totalScrollAngle % spWheel.singleSignDegrees;
                // deltaAngle = Math.abs(deltaAngle-spWheel.getSingleSignDegrees()); // Log.e("1-delta angle", "" + deltaAngle);
                if (deltaAngle > (spWheel.singleSignDegrees / 2))
                    deltaAngle = spWheel.singleSignDegrees - deltaAngle;
                else
                    deltaAngle *= -1;

                // Log.e("2-delta angle", "" + deltaAngle);
                spWheel.rotatePartialSignWheel(deltaAngle, duration); // rotates delta angle to the right
                spWheel.computeSelectedSign(totalScrollAngle + deltaAngle);
                // spWheel.computeSelectedSign(
                // (int)(totalScrollAngle/spWheel.getSingleSignDegrees()) );
                // menuManager.setSelectedHorosignId(spWheel.getSelectedSign());
            }

            // for right to left rotation rotation
            else if (totalScrollAngle < 0) {
                // reverse right
                float deltaAngle;
                deltaAngle = Math.abs(totalScrollAngle) % spWheel.singleSignDegrees;

                // Log.e("total angle Scroll: ","."+totalScrollAngle);
                // Log.e("delta angle",""+deltaAngle);
                if (deltaAngle > (spWheel.singleSignDegrees / 2)) {
                    deltaAngle = -1 * (spWheel.singleSignDegrees - deltaAngle);
                }

                spWheel.rotatePartialSignWheel(deltaAngle, duration); // rotates delta angle to the right
                spWheel.computeSelectedSign(totalScrollAngle + deltaAngle);
                // spWheel.computeSelectedSign((int)(totalScrollAngle/spWheel.getSingleSignDegrees()));
                // menuManager.setSelectedHorosignId(spWheel.getSelectedSign());
            }
        }

        return gestureScanner.onTouchEvent(me);
    }

    public boolean onDown(MotionEvent e) {
        scaleImageDown();
        totalScrollAngle = 0f;
        MotionEvent firstMotionEvent = MotionEvent.obtain(e);
        firstMotionEvent.setLocation(e.getRawX(), e.getRawY());

        if (rectangle.contains((int) firstMotionEvent.getX(), (int) firstMotionEvent.getRawY())) {
            // //Log.e("Id: ",""+spWheel.getSelectedSign());
            // //Log.e("Name",""+Utility.getSignName(spWheel.getSelectedSign()));
            // //Log.e("child count: ",""+spWheel.getChildCount());

            for (int i = 0; i < spWheel.getChildCount(); i++) {
                // identifying which view's image we have to image, by checking its sign name
                View view = (((LinearLayout) ((LinearLayout) spWheel.getChildAt(i)).getChildAt(0)).getChildAt(1));
                // //Log.e("text: "+((TextView)view).getText(),"."+Utility.getSignName(spWheel.getSelectedSign()));
                if (((TextView) view).getText().equals(Utility.getSignName(spWheel.selectedSign))) {
                    // this flag is used in ACTION_MOVE & ACTION_UP to scale down the image if it has been scaled up
                    hasImageScalled = true;

                    //getting reference of target imageButtong to change its resource
                    //View vv = view = (((LinearLayout) ((LinearLayout) ((LinearLayout) spWheel.getChildAt(i))).getChildAt(0)).getChildAt(0));
                    //scaleImageUp();
					/*
					imgButton = (ImageButton) vv.findViewById(R.id.wheelItem_Btn);
					if (metrics.widthPixels < metrics.heightPixels) 
						imgButton.setImageDrawable(signWheelButton.getDrawable());
						*/
                }
            }
        }

        return false;
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if (hasImageScalled) {
            hasImageScalled = false;
            scaleImageDown();
        }

        Rect hit = new Rect();
        FrameLayout fl = findViewById(R.id.frameLayout1);
        fl.getDrawingRect(hit);
        if (e1.getY() < 1.5 * hit.height() || e2.getY() < 1.5 * hit.height()) {
            return true;
        }

        float _spinCenterX1 = spWheel.rotPivotX;
        float _spinCenterY1 = spWheel.rotPivotY;

        float aPtX1 = swipeStartEvent.getX();
        float aPtY1 = swipeStartEvent.getY();

        aPtX1 = (float) (aPtX1 + (_spinCenterX1 - 240.0) - _spinCenterX1);
        aPtY1 = (float) (-(aPtY1 - 96.0) + _spinCenterY1);

        float _spinCenterX2 = spWheel.rotPivotX;
        float _spinCenterY2 = spWheel.rotPivotY;

        float aPtX2 = e2.getX();
        float aPtY2 = e2.getY();

        aPtX2 = (float) (aPtX2 + (_spinCenterX2 - 240.0) - _spinCenterX2);
        aPtY2 = (float) (-(aPtY2 - 96.0) + _spinCenterY2);

        MotionEvent p1 = MotionEvent.obtain(e1);

        if (distanceX < 0) {
            // rotationDirection=1;
            p1.setLocation(e2.getRawX() + distanceX, e2.getRawY() + distanceY);
            float swipeAngle = (float) Math.toDegrees(angleBetweenPoints(p1, e2));
            totalScrollAngle += swipeAngle;
            // Log.i("rotate right",".");
            // Log.i("***angle: ",""+swipeAngle);
            spWheel.rotatePartialSignWheel((swipeAngle), duration);// -2.0

        } else if (distanceX > 0) {
            p1.setLocation(e2.getRawX() - distanceX, e2.getRawY() - distanceY);
            float swipeAngle = (float) Math.toDegrees(angleBetweenPoints(p1, e2));

            totalScrollAngle -= swipeAngle;
            // Log.i("rotate left",".");
            // Log.i("***************angle: ",""+swipeAngle);
            spWheel.rotatePartialSignWheel((-1 * swipeAngle), duration);// -2.0
        }
        savedRotatedDegrees = spWheel.rotatedDegrees;
        return false;
    }

    // VELOCITY BASED ROTATION
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        // Log.d("onFling", ".");
        // spWheel.setRotatedDegrees(savedRotatedDegrees);
        if (hasImageScalled) {
            hasImageScalled = false;
            scaleImageDown();
			/*if (metrics.widthPixels < metrics.heightPixels) imgButton.setImageDrawable(signWheelButton.getDrawable()); */
        }

        try {
            // Log.d("velocity: ",""+Math.abs(velocityX));
            // Log.d("distance: ",""+Math.abs(e1.getX() - e2.getX()));
            // Return false if any of the event y coordinate in in navigation bar
            Rect hit = new Rect();
            FrameLayout fl = findViewById(R.id.frameLayout1);
            fl.getDrawingRect(hit);
            if (e1.getY() < 1.5 * hit.height() || e2.getY() < 1.5 * hit.height()) return false;
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH
                    || Math.abs(e1.getX() - e2.getX()) < SWIPE_MIN_DISTANCE
                    || Math.abs(velocityX) < SWIPE_THRESHOLD_VELOCITY) {
                return false;
            }

            double signsToRotate = Math.abs(velocityX) / Math.abs(e1.getX() - e2.getX());
            if (Math.abs(velocityX) < ONE_SIGN_MAX_VELOCITY) signsToRotate = 1;
            else if (Math.abs(velocityX) < TWO_SIGN_MAX_VELOCITY) signsToRotate = 2;
            else if (Math.abs(velocityX) < THREE_SIGN_MAX_VELOCITY) signsToRotate = 3;
            else signsToRotate = 4;

            // right to left swipe
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                // Log.d("", "Left Swipe");
                spWheel.rotateSignWheel((int) (-1 * signsToRotate), duration);

            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                // Log.d("", "Right Swipe");
                spWheel.rotateSignWheel((int) signsToRotate, duration);
            }
        } catch (Exception e) {
            // nothing
        }
        return true;
    }

    public void scaleImageUp() {
	/*
		int selectedSign = spWheel.getSelectedSign();
		if (Constants.signId_Contact == selectedSign)
			signWheelButton.setImageResource(R.drawable.zod_12_large);
		else if ((selectedSign - 0) * (11 - selectedSign) >= 0) 
			signWheelButton.setImageResource(getResources().getIdentifier(String.format("zod_%02d_large", selectedSign), "drawable", getPackageName()));	
	*/
    }

    public void scaleImageDown() {
		/*
		int selectedSign = spWheel.getSelectedSign();
		if (Constants.signId_Contact == selectedSign)
			signWheelButton.setImageResource(R.drawable.zod_12_small);
		else if ((selectedSign - 0) * (11 - selectedSign) >= 0) 
			signWheelButton.setImageResource(getResources().getIdentifier(String.format("zod_%02d_small", selectedSign), "drawable", getPackageName()));	
		*/
    }

    public float angleBetweenPoints(MotionEvent _swipeStartEvent, MotionEvent _swipeEndEvent) {
        float theResult = dotProduct(_swipeStartEvent, _swipeEndEvent);
        theResult /= Math.sqrt(dotProduct(_swipeStartEvent, _swipeStartEvent));
        theResult /= Math.sqrt(dotProduct(_swipeEndEvent, _swipeEndEvent));

        theResult = (float) Math.min(Math.max(theResult, -1.0), 1.0);
        theResult = (float) Math.acos(theResult);

        return theResult;
    }

    public float dotProduct(MotionEvent _swipeStartEvent, MotionEvent _swipeEndEvent) {
        return (_swipeStartEvent.getX() * _swipeEndEvent.getX()) + (_swipeStartEvent.getY() * _swipeEndEvent.getY());
    }

    // DISTANCE BASED ROTATION
    /*
     * public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
     * float velocityY) { //Log.d("", "onFling"); try {
     *
     * // Return false if any of the event y coordinate in in navigation bar
     * Rect hit = new Rect(); FrameLayout fl = (FrameLayout)
     * findViewById(R.id.frameLayout1); fl.getDrawingRect(hit); if (e1.getY() <
     * 1.5 * hit.height() || e2.getY() < 1.5 * hit.height()) return false;
     *
     * if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) return false;
     * // right to left swipe if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE &&
     * Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) { ////Log.d("",
     * "Left Swipe"); if (e1.getX() - e2.getX() < SWIPE_MIN_DISTANCE +
     * SWIPE_SIGN_RANGE) spWheel.rotateSignWheel(-1); else if (e1.getX() -
     * e2.getX() < SWIPE_MIN_DISTANCE + 2*SWIPE_SIGN_RANGE)
     * spWheel.rotateSignWheel(-2); else spWheel.rotateSignWheel(-3);
     *
     * } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE &&
     * Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) { ////Log.d("",
     * "Right Swipe"); if (e2.getX() - e1.getX() < SWIPE_MIN_DISTANCE +
     * SWIPE_SIGN_RANGE) spWheel.rotateSignWheel(1); else if (e2.getX() -
     * e1.getX() < SWIPE_MIN_DISTANCE + 2*SWIPE_SIGN_RANGE)
     * spWheel.rotateSignWheel(2); else spWheel.rotateSignWheel(3); } } catch
     * (Exception e) { // nothing }
     *
     * updateSignButtonImage(); return true; }
     */

    public void onLongPress(MotionEvent e) {

    }

    public void onShowPress(MotionEvent e) {

    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public boolean getPrefFirstLaunchValue() {
        return signWheelPreferences.getBoolean(getString(R.string.first_launch), false);
    }

    public void loadMenu() {
        MgrMenu mgrMenu = MgrMenu.getInstance(this);

        // Only process if either menu is not available or date has changed
        if (mgrMenu.isMenuAvailable() && !mgrMenu.isDateChanged()) {
            return;
        } else if (!MgrShared.getInstance(this).isNetworkAvailable()) {
            Utility.showAlert(SignWheel.this, getPrefFirstLaunchValue() ? getString(R.string.alert_message) : getString(R.string.date_changed_network_unavailable));
        } else if (!mgrMenu.isMenuAvailable()) {
            new performLoadingMenu("").execute();
        } else if (mgrMenu.isDateChanged()) {
            new performClearCacheAndLoadingMenu(getString(R.string.the_date_has_changed)).execute();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        System.gc();
        // signWheelPreferences= getSharedPreferences(PREFS,
        // Activity.MODE_PRIVATE);
        // SharedPreferences.Editor editor = signWheelPreferences.edit();
        // editor.putBoolean("isFirstLaunch",false);
        // editor.commit();

    }

    @SuppressWarnings("unused")
    private class loadPendingReadings extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            IScopesDB db = IScopesDB.getInstance(getApplicationContext());
            ArrayList<PendingPurchases> purchases = db.getPendingProducts();
            for (PendingPurchases object : purchases) {
                int readingID = MgrReadings.getSharedObject(getApplicationContext()).purchaseReading(object.url, object.type, object.position);
                if (readingID != -1) db.deletePendingReading(object.purchaseId);
            }

            return null;
        }
    }

    private class performLoadingMenu extends AsyncTask<Void, Void, String> {
        private String pDialogMessage;
        ProgressDialog pDialog;

        private performLoadingMenu(String Message) {
            pDialogMessage = Message;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(SignWheel.this);
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.setMessage(pDialogMessage + getString(R.string.loading_please_wait));
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                MgrMenu.getInstance(SignWheel.this).loadMenuFromServer();
                //Horomenu menu = menuManager.getMenu(Constants.Menu_All);
            } catch (Exception e) {
                e.printStackTrace();
                return getString(R.string.data_not_retrieved); //e.getMessage();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String errorMessage) {
            if (pDialog != null && pDialog.isShowing()) pDialog.dismiss();
            if (errorMessage.length() > 0) {
                Utility.showAlert(SignWheel.this, errorMessage);
            }

            /************* Loading Menu Diagnostics Caching **/
            //long timeBeforeCaching = System.currentTimeMillis() / 1000;
            MgrMenu.getInstance(SignWheel.this).saveMenu();
            //long timeAfterCaching = System.currentTimeMillis() / 1000;
            //long totalCachingTime = timeAfterCaching - timeBeforeCaching;
            // Log.e("TotalTime for Caching: ", "" + totalCachingTime+ " secs");
            /************* Loading Menu Diagnostics Caching **/
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            pDialog.dismiss();
        }
    }

    private class performClearCacheAndLoadingMenu extends AsyncTask<Void, String, String> {
        private String pDialogMessage;
        ProgressDialog pDialog;

        public performClearCacheAndLoadingMenu(String Message) {
            pDialogMessage = Message;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(SignWheel.this);
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.setTitle(pDialogMessage);
            pDialog.setMessage(getString(R.string.clear_cache_reload_content));
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                MgrMenu.getInstance(SignWheel.this).clearCahe();
                publishProgress("Loading Menu...");
                MgrMenu.getInstance(SignWheel.this).loadMenuFromServer();
                //list = ParseProductListXML.getFromWeb();
                // Horomenu menu = menuManager.getMenu(Constants.Menu_All);
            } catch (Exception e) {
                e.printStackTrace();
                return getString(R.string.data_not_retrieved);
            }

            return "";
        }

        @Override
        protected void onProgressUpdate(String... values) {
            pDialog.setMessage(values[0]);
        }

        @Override
        protected void onPostExecute(String errorMessage) {
            if (pDialog != null && pDialog.isShowing()) pDialog.dismiss();
            //sharedManager.products = list;
            if (errorMessage.length() > 0) Utility.showAlert(SignWheel.this, errorMessage);

            /************* Loading Menu Diagnostics Caching **/
            //long timeBeforeCaching = System.currentTimeMillis() / 1000;
            MgrMenu.getInstance(SignWheel.this).saveMenu();
            //long timeAfterCaching = System.currentTimeMillis() / 1000;
            //long totalCachingTime = timeAfterCaching - timeBeforeCaching;
            // Log.e("TotalTime for Caching: ", "" + totalCachingTime+ " secs");
            /************* Loading Menu Diagnostics Caching **/
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (pDialog != null && pDialog.isShowing()) pDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        MyApplication.getInstance().closeDB();
        // onUserLeaveHint();
        // this.finish();
        // android.os.Process.killProcess(android.os.Process.myPid());
        // super.onDestroy();
        // System.exit(0);
    }

    @Override
    public void onStart() {
        super.onStart();
        QuantcastClient.activityStart(this, Consts.key_quant_cast, null, null);
        trackScreen(Consts.screen_horo_sign);
        fireBaseLogEventScreen(Consts.screen_horo_sign);
    }

    @Override
    public void onStop() {
        super.onStop();
        MyApplication.getInstance().closeDB();
        QuantcastClient.activityStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.getInstance().closeDB();
    }
}
