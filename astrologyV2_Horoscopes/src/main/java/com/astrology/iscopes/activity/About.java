package com.astrology.iscopes.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.astrology.iscopes.R;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.Utility;
import com.quantcast.measurement.service.QuantcastClient;

public class About extends BaseActivity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aboutscreen);

        ((TextView) findViewById(R.id.versionnumber)).setText(Utility.getVersionNumberLong(getApplicationContext()));
        findViewById(R.id.btn_privacy).setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        QuantcastClient.activityStart(this, Consts.key_quant_cast, null, null);
        trackScreen(Consts.screen_about);
        fireBaseLogEventScreen(Consts.screen_about);
    }

    @Override
    public void onStop() {
        super.onStop();
        QuantcastClient.activityStop();
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.privacy_policy_link))));
    }
}