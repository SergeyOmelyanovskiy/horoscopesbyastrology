package com.astrology.iscopes.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.astrology.iscopes.R;
import com.astrology.iscopes.common.MgrVideo;
import com.astrology.iscopes.common.Utility;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.IOException;

public class HoroVideo extends AppCompatActivity implements OnCompletionListener, OnErrorListener, OnPreparedListener {

    private MediaController mediaController = null;
    private ProgressDialog pDialog;
    private Boolean hasMediaErrorOccurred = false;
    VideoView videoView = null;

    public static String resolveRedirect(String url) throws ClientProtocolException, IOException {
        System.setProperty("http.keepAlive", "false");
        HttpParams httpParameters = new BasicHttpParams();
        HttpClientParams.setRedirecting(httpParameters, false);

        HttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpConnectionParams.setConnectionTimeout(httpParameters, 5000); // connection timeout
        HttpConnectionParams.setSoTimeout(httpParameters, 5000); // socket timeout
        HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), false);
        HttpResponse response = httpClient.execute(new HttpGet(url));

        // If we didn't get a '302 Found' we aren't being redirected.
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_MOVED_TEMPORARILY)
            throw new IOException(response.getStatusLine().toString());

        Header loc[] = response.getHeaders("Location");
        return loc.length > 0 ? loc[0].getValue() : null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videoscreenlayout);
        pDialog = new ProgressDialog(HoroVideo.this);
        pDialog.setMessage(getString(R.string.loading_video));
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                if (pDialog != null && pDialog.isShowing()) pDialog.dismiss();
                HoroVideo.this.finish();
            }
        });

        pDialog.show();

        videoView = findViewById(R.id.videoview);
        mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setOnErrorListener(this);
        videoView.setOnCompletionListener(this);
        videoView.setOnPreparedListener(this);
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(Uri.parse(getIntent().getExtras().getString(getString(R.string.video_url))));
        videoView.requestFocus();
        videoView.start();
        HttpConnectionParams.setConnectionTimeout(new BasicHttpParams(), 5000);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.video_error));
        switch (what) {
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                sb.append(getString(R.string.video_error_progressive_playback));
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                sb.append(getString(R.string.video_error_server_died));
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                sb.append(getString(R.string.video_error_unknown));
                break;
            default:
                sb.append(getString(R.string.video_error_non_standard));
                sb.append(what);
                sb.append(")");
                break;
        }
        sb.append(" (" + what + ") ");
        sb.append(extra);

        if (hasMediaErrorOccurred.equals(true)) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }

            AlertDialog alertDialog = new AlertDialog.Builder(HoroVideo.this)
                    .setTitle(getString(R.string.alert_title))
                    .setMessage(getString(R.string.error_video))
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false).show();

            alertDialog.setOnDismissListener(new OnDismissListener() {
                public void onDismiss(DialogInterface dialog) {
                    HoroVideo.this.finish();
                }
            });
        } else {
            hasMediaErrorOccurred = true;
            try {
                String rtspUrl = resolveRedirect(MgrVideo.getInstance(getApplicationContext()).playVideoButtonClicked(false, HoroDetail.hs, getString(R.string.app_name_for_url), Utility.getVersionNumber(getApplicationContext())));
                videoView.setVideoURI(Uri.parse(rtspUrl));
                videoView.requestFocus();
                videoView.setPadding(4, 4, 4, 4);
                videoView.start();
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }

        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        HoroVideo.this.finish();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

        mediaController.show();
    }
}
