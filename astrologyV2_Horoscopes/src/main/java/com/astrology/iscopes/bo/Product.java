package com.astrology.iscopes.bo;

import android.graphics.Bitmap;

public class Product {

    public String ares_Id;
    public String name;
    public String price;
    public String type;
    public String imageURL;
    public String description;
    public String sku;

    public Bitmap image;
    public Managed managedType = Managed.MANAGED;

    public static enum Managed {MANAGED, UNMANAGED, SUBSCRIPTION}

    @Override
    public String toString() {
        return "Product [ares_Id=" + ares_Id + ", name=" + name + ", price="
                + price + ", image=" + image + ", type=" + type + ", imageURL="
                + imageURL + ", description=" + description + ", sku=" + sku
                + ", managedType=" + managedType + "]";
    }
}