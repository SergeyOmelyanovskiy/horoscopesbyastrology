package com.astrology.iscopes.bo;

public class MyReadingsEntryItem implements ListItem {

    public String birthName;
    public String purchaseTime;
    public String readingType;// luncar forcast or Friends & Lover or Natal Chart
    public int readingId;
    public int hashTableIndex;

    public MyReadingsEntryItem(String birthName, String purchaseTime, String readingType, int readingId, int hashTableIndex) {
        this.readingId = readingId;
        this.birthName = birthName;
        this.purchaseTime = purchaseTime;
        this.readingType = readingType;
        this.hashTableIndex = hashTableIndex;
    }

    @Override
    public boolean isSection() {
        return false;
    }
}