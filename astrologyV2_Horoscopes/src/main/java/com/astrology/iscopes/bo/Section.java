package com.astrology.iscopes.bo;

import java.util.ArrayList;

public class Section {

    public String title;
    public String imageURL;
    public String description;
    public String relation;
    public ArrayList<String> imagePaths = new ArrayList<String>();
}