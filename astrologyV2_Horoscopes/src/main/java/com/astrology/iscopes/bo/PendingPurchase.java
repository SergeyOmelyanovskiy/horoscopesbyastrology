package com.astrology.iscopes.bo;

public class PendingPurchase {

    public int id;
    public int position;
    public String url;
    public String type;

    public PendingPurchase(int id, int position, String url, String type) {
        this.id = id;
        this.position = position;
        this.url = url;
        this.type = type;
    }

    @Override
    public String toString() {
        return "PendingPurchase [id=" + id + ", position=" + position + ", url=" + url + ", type=" + type + "]";
    }
}