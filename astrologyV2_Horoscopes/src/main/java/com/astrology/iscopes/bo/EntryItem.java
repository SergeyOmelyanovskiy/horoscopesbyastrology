package com.astrology.iscopes.bo;

import com.astrology.iscopes.common.Consts;

import java.util.Locale;

public class EntryItem implements ListItem {

    public int horoId;
    public String title;
    private String iconName;

    public EntryItem(int horoId, String title, String iconName) {
        this.horoId = horoId;
        this.title = title;
        this.iconName = iconName;
    }

    public String getIconName() {
        String icon = iconName;
        if (title.contains("About ")) {
            icon = title.replace("About ", "");
        } else if (title.equalsIgnoreCase(Consts.title_get_reading)) {
            icon = "gpr";
        } else if (title.equalsIgnoreCase(Consts.title_view_reading)) {
            icon = "vpr";
        } else if (title.equalsIgnoreCase("New Daily VideoScope")) {
            icon = "video";
        }

        icon = Consts.prefix_icn_menulist + icon;
        icon = icon.replaceAll("-", "_").toLowerCase(Locale.US);
        return icon;
    }

    @Override
    public boolean isSection() {
        return false;
    }
}