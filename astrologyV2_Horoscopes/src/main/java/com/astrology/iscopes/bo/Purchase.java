package com.astrology.iscopes.bo;

import java.util.ArrayList;

import com.astrology.iscopes.orm.Birthdetails;

public class Purchase {

    public int readingId;
    public int productID;

    public String purchaseTime;
    public String productImageURL;

    public ArrayList<Birthdetails> birthInfoArray = new ArrayList<Birthdetails>();
}