package com.astrology.iscopes.bo;

import java.util.ArrayList;
import java.util.Date;

public class Promo {

    public String image;
    public String text;
    public String url;
    public String phoneNumber;

    public ArrayList<Integer> horoTypes;
    public ArrayList<Date> priorityOnDates;
    public ArrayList<Date> validDates;
}