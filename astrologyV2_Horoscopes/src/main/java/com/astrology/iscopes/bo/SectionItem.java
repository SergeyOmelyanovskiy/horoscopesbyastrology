package com.astrology.iscopes.bo;

public class SectionItem implements ListItem {

    public String title;

    public SectionItem(String title) {
        this.title = title;
    }

    @Override
    public boolean isSection() {
        return true;
    }
}