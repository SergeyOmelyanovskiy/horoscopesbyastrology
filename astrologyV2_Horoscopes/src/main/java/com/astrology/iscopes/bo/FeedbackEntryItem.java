package com.astrology.iscopes.bo;

public class FeedbackEntryItem implements ListItem {

    public String title;
    public String summary;
    public String iconName;

    public FeedbackEntryItem(String title, String summary, String iconName) {
        this.title = title;
        this.summary = summary;
        this.iconName = iconName;
    }

    @Override
    public boolean isSection() {
        return false;
    }
}