package com.astrology.iscopes.bo;

import java.util.ArrayList;

public class Reading {

    public Cover cover;
    public ArrayList<ArrayList<Section>> interpretation;
}