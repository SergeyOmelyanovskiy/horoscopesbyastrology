package com.astrology.iscopes.server;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class HttpClient {

    private ArrayList<NameValuePair> params;
    private ArrayList<NameValuePair> headers;
    private String url;

    public String message = "";
    public String response = "";
    public int responseCode;

    public HttpClient(String url) {
        this.url = url;
        params = new ArrayList<NameValuePair>();
        headers = new ArrayList<NameValuePair>();
    }

    public void AddParam(String name, String value) {
        params.add(new BasicNameValuePair(name, value));
    }

    public void AddHeader(String name, String value) {
        headers.add(new BasicNameValuePair(name, value));
    }

//	public void Execute(int method) throws Exception {
//		switch (method) {
//			case Consts.Request_Method_GET: {
//				String combinedParams = "";
//				if (!params.isEmpty()) {
//					combinedParams += "?";
//					for (NameValuePair p : params) {
//						String paramString = p.getName() + "=" + URLEncoder.encode(p.getValue(), "UTF-8");
//						if (combinedParams.length() > 1) combinedParams += "&" + paramString;
//						else combinedParams += paramString;
//					}
//				}
//
//				HttpGet request = new HttpGet(url + combinedParams);
//				for (NameValuePair h : headers)
//					request.addHeader(h.getName(), h.getValue());
//
//				executeRequest(request, url);
//				break;
//			}
//			case Consts.Request_Method_POST: {
//				HttpPost request = new HttpPost(url);
//				for (NameValuePair h : headers)
//					request.addHeader(h.getName(), h.getValue());
//
//				if (!params.isEmpty())
//					request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
//
//				executeRequest(request, url);
//				break;
//			}
//		}
//	}

    private void executeRequest(HttpUriRequest request, String url) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse httpResponse;

        try {
            httpResponse = client.execute(request);
            responseCode = httpResponse.getStatusLine().getStatusCode();
            message = httpResponse.getStatusLine().getReasonPhrase();

            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                response = convertStreamToString(instream);
                // Closing the input stream will trigger connection release
                instream.close();
            }
        } catch (ClientProtocolException e) {
            client.getConnectionManager().shutdown();
            e.printStackTrace();
        } catch (IOException e) {
            client.getConnectionManager().shutdown();
            e.printStackTrace();
        }
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null)
                sb.append(line + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}