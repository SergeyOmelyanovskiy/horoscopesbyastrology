package com.astrology.iscopes.appsupport;

public class TwitterStatus {

    private static final String bitlyAppKey = "R_272e82b81140ae2c2def7518f046583a";
    private static final String bitlyUserName = "centersf";

    public String twitterPostHeading = "";
    public String twitterPostURL = "";

    private static TwitterStatus anInstance;

    public static TwitterStatus getInstance() {
        if (anInstance == null) {
            anInstance = new TwitterStatus();
        }

        return anInstance;
    }

    private String getTwitterPostURL() {
        try {
            return new BitlyAndroid(bitlyUserName, bitlyAppKey).getShortUrl(twitterPostURL);
        } catch (Exception e) {
            return twitterPostURL;
        }
    }

    private String getTwitterPostHeading() {
        if (twitterPostHeading.length() > 100) {
            return twitterPostHeading.substring(0, 100) + " ...";
        }

        return twitterPostHeading;
    }

    public String get() {
        return getTwitterPostHeading() + "\n" + getTwitterPostURL();
    }
}