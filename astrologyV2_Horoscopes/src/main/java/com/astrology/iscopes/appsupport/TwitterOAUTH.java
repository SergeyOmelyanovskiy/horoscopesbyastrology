package com.astrology.iscopes.appsupport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.astrology.iscopes.activity.CustomWebView;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.Utility;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;

public class TwitterOAUTH extends AppCompatActivity {

    private OAuthConsumer mConsumer = null;
    private OAuthProvider mProvider = null;

    private SharedPreferences mSettings;

    public static String oauth_token = null;
    public static String oauth_verify = null;
    public static final String PREFS = "twitter";
    public static final String REQUEST_SECRET = "request_secret";
    public static final String REQUEST_TOKEN = "request_token";
    public static final String TWITTER_ACCESS_TOKEN_URL = "https://api.twitter.com/oauth/access_token";
    public static final String TWITTER_AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize";
    public static final String TWITTER_REQUEST_TOKEN_URL = "https://api.twitter.com/oauth/request_token";
    public static final String USER_SECRET = "user_secret";
    public static final String USER_TOKEN = "user_token";

    public String source;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        source = Utility.getString(getIntent().getExtras(), Consts.activity_source, "notfound");
        new AsynchTweet().execute();
    }

    private class AsynchTweet extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            mConsumer = new CommonsHttpOAuthConsumer(Consts.key_twitter_api, Consts.key_twitter_secret);
            mProvider = new CommonsHttpOAuthProvider(TWITTER_REQUEST_TOKEN_URL, TWITTER_ACCESS_TOKEN_URL, TWITTER_AUTHORIZE_URL);

            mProvider.setOAuth10a(true);
            mSettings = TwitterOAUTH.this.getSharedPreferences(PREFS, Context.MODE_PRIVATE);

            if (oauth_token == null && oauth_verify == null) {
                String authUrl;
                try {
                    authUrl = mProvider.retrieveRequestToken(mConsumer, "http://iscopes.com/1");
                    saveRequestInformation(mSettings, mConsumer.getToken(), mConsumer.getTokenSecret());
                    Intent intent = new Intent(TwitterOAUTH.this, CustomWebView.class);
                    intent.putExtra(Consts.uri, authUrl);
                    intent.putExtra(Consts.activity_source, source);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    TwitterOAUTH.this.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    Utility.debug_log(e.toString());
                    //Utility.showAlert(TwitterOAUTH.this, "An error occurred, please try again later.");
                }
            }

            return null;
        }
    }

    protected void onResume() {
        super.onResume();
        new AsyncTask<Void, Void, Intent>() {

            @Override
            protected Intent doInBackground(Void... params) {
                String token = "", secret = "";
                if (oauth_token != null && oauth_verify != null) {
                    if (mSettings != null) {
                        token = mSettings.getString(REQUEST_TOKEN, null);
                        secret = mSettings.getString(REQUEST_SECRET, null);
                    }

                    Intent i = new Intent(TwitterOAUTH.this, TwitterClient.class);
                    i.putExtra(Consts.activity_source, source);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    try {
                        if (mConsumer == null) return null;
                        if (token != null && secret != null)
                            mConsumer.setTokenWithSecret(token, secret);
                        if (!oauth_token.equals(mConsumer.getToken())) return null;

                        //Assert.assertEquals(oauth_token, mConsumer.getToken());
                        mProvider.retrieveAccessToken(mConsumer, oauth_verify);
                        token = mConsumer.getToken();
                        secret = mConsumer.getTokenSecret();
                        TwitterOAUTH.saveAuthInformation(mSettings, token, secret);
                        TwitterClient.setUserLoginStatus(mSettings, TwitterClient.TWITTER_LOGIN, true);
                        TwitterOAUTH.saveRequestInformation(mSettings, null, null);
                        return i;
                    } catch (Exception e) {
                        return null;
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Intent i) {
                if (null != i) {
                    startActivity(i);
                    finish();
                } else {
                    finish();
                }
            }
        }.execute();
    }

    public static void saveAuthInformation(SharedPreferences settings, String token, String secret) {
        SharedPreferences.Editor editor = settings.edit();
        if (token == null) {
            editor.remove(TwitterOAUTH.USER_TOKEN);
        } else {
            editor.putString(TwitterOAUTH.USER_TOKEN, token);
        }

        if (secret == null) {
            editor.remove(TwitterOAUTH.USER_SECRET);
        } else {
            editor.putString(TwitterOAUTH.USER_SECRET, secret);
        }

        editor.apply();
    }

    public static void saveRequestInformation(SharedPreferences settings, String token, String secret) {
        SharedPreferences.Editor editor = settings.edit();
        if (token == null) {
            editor.remove(TwitterOAUTH.REQUEST_TOKEN);
        } else {
            editor.putString(TwitterOAUTH.REQUEST_TOKEN, token);
        }

        if (secret == null) {
            editor.remove(TwitterOAUTH.REQUEST_SECRET);
        } else {
            editor.putString(TwitterOAUTH.REQUEST_SECRET, secret);
        }

        editor.apply();
    }
}