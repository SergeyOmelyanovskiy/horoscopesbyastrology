package com.astrology.iscopes.appsupport;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.astrology.iscopes.R;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.Utility;

import org.apache.http.HttpEntity;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.LinkedList;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;

public class TwitterClient extends AppCompatActivity implements OnClickListener {
    // This task is run on every onResume(), to make sure the current credentials are valid.
    // This is probably overkill for a non-educational program
    private OAuthConsumer mConsumer = null;
    private HttpClient mClient;
    private SharedPreferences mSettings;
    private Dialog dialog;

    public static boolean loginStatus = false;
    public static boolean isEnabled = false;

    public static final String TAG = "TwitterClient";
    public static final String TWITTER_LOGIN = "TwitterLogin";
    public static final String FRIENDS_TIMELINE_URL_STRING = "https://api.twitter.com/1.1/statuses/friends_timeline.json";
    public static final String HOME_TIMELINE_URL_STRING = "https://api.twitter.com/1.1/statuses/home_timeline.json";
    public static final String STATUSES_URL_STRING = "https://api.twitter.com/1.1/statuses/update.json";
    public static final String TWITTER_ACCESS_TOKEN_URL = "https://api.twitter.com/oauth/access_token";
    public static final String TWITTER_AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize";
    public static final String TWITTER_REQUEST_TOKEN_URL = "https://api.twitter.com/oauth/request_token";
    public static final String USER_TIMELINE_URL_STRING = "https://api.twitter.com/1.1/statuses/user_timeline.json";
    public static final String VERIFY_URL_STRING = "https://api.twitter.com/1.1/account/verify_credentials.json";

    private class GetCredentialsTask extends AsyncTask<Void, Void, JSONObject> {
        ProgressDialog authDialog;

        protected void onPreExecute() {
            authDialog = getProgressDialog(getText(R.string.auth_progress_title), getText(R.string.auth_progress_text));
            authDialog.show();
        }

        protected JSONObject doInBackground(Void... arg0) {
            try {
                HttpGet get = new HttpGet(VERIFY_URL_STRING);
                mConsumer.sign(get);

                org.apache.http.HttpResponse httpResponse = mClient.execute(get);
                HttpEntity entity = httpResponse.getEntity();
                return new JSONObject(EntityUtils.toString(entity));
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(JSONObject jso) {
            try {
                //Constants.AlreadyShown = true;
                authDialog.dismiss();
                loginStatus = jso != null;
                setUserLoginStatus(mSettings, TwitterClient.TWITTER_LOGIN, loginStatus);
                if (loginStatus == true) {
                    if (isEnabled) {
                        new PostTask().execute();
                    } else {
                        finish();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class PostTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog postDialog;

        protected void onPreExecute() {
            postDialog = getProgressDialog(getText(R.string.tweet_progress_title), getText(R.string.tweet_progress_text));
            postDialog.show();
        }

        protected JSONObject doInBackground(String... params) {
            try {
                HttpPost post = new HttpPost(STATUSES_URL_STRING);
                LinkedList<BasicNameValuePair> out = new LinkedList<BasicNameValuePair>();
                out.add(new BasicNameValuePair("status", TwitterStatus.getInstance().get()));
                post.setEntity(new UrlEncodedFormEntity(out, HTTP.UTF_8));
                post.setParams(getParams());
                // sign the request to authenticate
                mConsumer.sign(post);
                HttpEntity entity = mClient.execute(post).getEntity();
                return new JSONObject(EntityUtils.toString(entity));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject jso) {
            //Constants.AlreadyShown = true;
            postDialog.dismiss();
            Toast.makeText(getApplicationContext(), jso != null ? "Success: Message posted to twitter." : "Error: Please try again later.", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public ProgressDialog getProgressDialog(CharSequence title, CharSequence message) {
        ProgressDialog pd = new ProgressDialog(TwitterClient.this);
        pd.setTitle(title);
        pd.setMessage(message);
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.setIcon(R.drawable.icon_small);
        return pd;
    }

    public HttpParams getParams() {
        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setUseExpectContinue(params, false);
        return params;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invisible);

        String source = "notfound";
        source = Utility.getString(getIntent().getExtras(), Consts.activity_source, source);
        isEnabled = (Consts.activity_my_reading
                + Consts.activity_horo_detail
                + Consts.activity_product_detail).contains(source);

        HttpParams parameters = new BasicHttpParams();
        HttpProtocolParams.setVersion(parameters, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(parameters, HTTP.DEFAULT_CONTENT_CHARSET);
        HttpProtocolParams.setUseExpectContinue(parameters, false);
        HttpConnectionParams.setTcpNoDelay(parameters, true);
        HttpConnectionParams.setSocketBufferSize(parameters, 8192);

        SchemeRegistry schReg = new SchemeRegistry();
        schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        ClientConnectionManager tsccm = new ThreadSafeClientConnManager(parameters, schReg);
        mClient = new DefaultHttpClient(tsccm, parameters);
        mSettings = getSharedPreferences(TwitterOAUTH.PREFS, Context.MODE_PRIVATE);
        loginStatus = getUserLoginStatus(mSettings, TWITTER_LOGIN);
        mConsumer = new CommonsHttpOAuthConsumer(Consts.key_twitter_api, Consts.key_twitter_secret);

        if (!loginStatus) {
            Intent i = new Intent(this, TwitterOAUTH.class);
            i.putExtra(Consts.activity_source, source);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(i);
            finish();
        }
    }

    protected void onFinish() {
        mClient.getConnectionManager().shutdown();
    }

    @Override
    public void onClick(View v) {
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
        dialog.dismiss();
        if (v.getId() == R.id.Share) {
            new GetCredentialsTask().execute();
        } else {
            finish();
        }
    }

    public void showPreviewTwitter() {
        dialog = new Dialog(TwitterClient.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.twitter);
        dialog.setCanceledOnTouchOutside(false);
        EditText message = dialog.findViewById(R.id.Message);
        message.setText(TwitterStatus.getInstance().get());

        dialog.findViewById(R.id.Cancel).setOnClickListener(this);
        dialog.findViewById(R.id.Share).setOnClickListener(this);
        dialog.show();
    }

    public void onResume() {
        super.onResume();
        //if (!Constants.AlreadyShown) {
        if (mSettings.contains(TwitterOAUTH.USER_TOKEN) && mSettings.contains(TwitterOAUTH.USER_SECRET)) {
            String mToken = mSettings.getString(TwitterOAUTH.USER_TOKEN, null);
            String mSecret = mSettings.getString(TwitterOAUTH.USER_SECRET, null);
            if (mToken != null && mSecret != null) {
                mConsumer.setTokenWithSecret(mToken, mSecret);
            }
        }

        if (loginStatus && isEnabled) {
            showPreviewTwitter();
        } else {
            finish();
        }

		/*
		} else {
			if (isEnabled) 	
				Toast.makeText(getApplicationContext(), "Success!: Posted to twitter.", Toast.LENGTH_LONG).show();
			Constants.AlreadyShown = false;
			finish();
		}*/
    }

    public static boolean getUserLoginStatus(SharedPreferences settings, String key) {
        return settings.getBoolean(TWITTER_LOGIN, false);
    }

    public static void setUserLoginStatus(SharedPreferences settings, String key, boolean isActive) {
        SharedPreferences.Editor editor = settings.edit();
        if (!isActive) {
            editor.remove(key);
            TwitterOAUTH.oauth_token = null;
            TwitterOAUTH.oauth_verify = null;
        } else {
            editor.putBoolean(key, isActive);
        }

        editor.apply();
    }
}
