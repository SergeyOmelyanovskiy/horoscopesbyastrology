package com.astrology.iscopes.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.preference.PreferenceManager;

import com.astrology.iscopes.activity.MyApplication;
import com.astrology.iscopes.bo.Cover;
import com.astrology.iscopes.bo.Product;
import com.astrology.iscopes.bo.Purchase;
import com.astrology.iscopes.bo.Reading;
import com.astrology.iscopes.bo.Section;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.Utility;
import com.astrology.iscopes.orm.AppData;
import com.astrology.iscopes.orm.AppDataDao;
import com.astrology.iscopes.orm.BirthInfoForPurchasedReadings;
import com.astrology.iscopes.orm.BirthInfoForPurchasedReadingsDao;
import com.astrology.iscopes.orm.Birthdetails;
import com.astrology.iscopes.orm.BirthdetailsDao;
import com.astrology.iscopes.orm.DaoMaster;
import com.astrology.iscopes.orm.DaoSession;
import com.astrology.iscopes.orm.Favorites;
import com.astrology.iscopes.orm.FavoritesDao;
import com.astrology.iscopes.orm.Horogroup;
import com.astrology.iscopes.orm.HorogroupDao;
import com.astrology.iscopes.orm.Horomenu;
import com.astrology.iscopes.orm.HoromenuDao;
import com.astrology.iscopes.orm.Horoscope;
import com.astrology.iscopes.orm.HoroscopeDao;
import com.astrology.iscopes.orm.Horotype;
import com.astrology.iscopes.orm.HorotypeDao;
import com.astrology.iscopes.orm.InnerSections;
import com.astrology.iscopes.orm.InnerSectionsDao;
import com.astrology.iscopes.orm.LocationInfo;
import com.astrology.iscopes.orm.LocationInfoDao;
import com.astrology.iscopes.orm.OuterSections;
import com.astrology.iscopes.orm.OuterSectionsDao;
import com.astrology.iscopes.orm.PendingPurchases;
import com.astrology.iscopes.orm.PendingPurchasesDao;
import com.astrology.iscopes.orm.ProductInfo;
import com.astrology.iscopes.orm.ProductInfoDao;
import com.astrology.iscopes.orm.Readings;
import com.astrology.iscopes.orm.ReadingsDao;

import de.greenrobot.dao.query.DeleteQuery;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.WhereCondition.StringCondition;

public class IScopesDB {

    private static MyApplication myApp;
    private static IScopesDB anInstance;

    private Context context;
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private Cursor cursor;

    private AppDataDao appdataDao;
    private BirthdetailsDao birthDetailDao;
    private BirthInfoForPurchasedReadingsDao birthinfoDao;
    private FavoritesDao favoriteDao;
    private HorogroupDao horogroupDao;
    private HoromenuDao horomenuDao;
    private HoroscopeDao horoscopeDao;
    private HorotypeDao horotypeDao;
    private InnerSectionsDao innersectionDao;
    private LocationInfoDao locationdao;
    private OuterSectionsDao outersectionDao;
    private PendingPurchasesDao pendingpurchaseDao;
    private ProductInfoDao productinfoDao;
    private ReadingsDao readingsDao;

    public static IScopesDB getInstance(Context ctx) {
        if (anInstance == null) {
            anInstance = new IScopesDB(ctx.getApplicationContext());
        }

        myApp = MyApplication.getInstance();
        return anInstance;
    }

    private IScopesDB(Context context) {
        this.context = context;
    }

    public void saveMenu(Horomenu menu) {
        try {
            menu.date = (Utility.encodeText(menu.date));
            menu.menuDate = (Utility.encodeText(menu.menuDate));
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            horomenuDao = daoSession.getHoromenuDao();
            horomenuDao.deleteAll();
            horomenuDao.insert(menu);
            menu.date = (Utility.decodeText(menu.date));
            menu.menuDate = (Utility.decodeText(menu.menuDate));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveHorogroup(Horogroup horogroup) {
        try {
            horogroup.name = (Utility.encodeText(horogroup.name));
            horogroup.contentType = (Utility.encodeText(horogroup.contentType));
            horogroup.tags = (Utility.encodeText(horogroup.tags));
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            horogroupDao = daoSession.getHorogroupDao();

            DeleteQuery<Horogroup> deleteHorogroup = horogroupDao.queryBuilder().where(HorogroupDao.Properties.HoroId.eq(horogroup.horoId)).buildDelete();
            deleteHorogroup.executeDeleteWithoutDetachingEntities();
            horogroupDao.insert(horogroup);
            horogroup.name = (Utility.decodeText(horogroup.name));
            horogroup.contentType = (Utility.decodeText(horogroup.contentType));
            horogroup.tags = (Utility.decodeText(horogroup.tags));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveHorotype(Horotype horotype) {
        try {
            horotype.name = (Utility.encodeText(horotype.name));
            horotype.shortName = (Utility.encodeText(horotype.shortName));
            horotype.mediumName = (Utility.encodeText(horotype.mediumName));
            horotype.longName = (Utility.encodeText(horotype.longName));
            horotype.shareName = (Utility.encodeText(horotype.shareName));
            horotype.contentType = (Utility.encodeText(horotype.contentType));
            horotype.fallBackIconName = (Utility.encodeText(horotype.fallBackIconName));
            horotype.lastUpdated = (Utility.encodeText(horotype.lastUpdated));
            horotype.tags = (Utility.encodeText(horotype.tags));

            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            horotypeDao = daoSession.getHorotypeDao();
            DeleteQuery<Horotype> deleteHorotype = horotypeDao.queryBuilder().where(HorotypeDao.Properties.HoroId.eq(horotype.horoId), HorotypeDao.Properties.HorogroupId.eq(horotype.horogroupId)).buildDelete();
            deleteHorotype.executeDeleteWithoutDetachingEntities();
            horotypeDao.insert(horotype);
            horotype.name = (Utility.decodeText(horotype.name));
            horotype.shortName = (Utility.decodeText(horotype.shortName));
            horotype.mediumName = (Utility.decodeText(horotype.mediumName));
            horotype.longName = (Utility.decodeText(horotype.longName));
            horotype.shareName = (Utility.decodeText(horotype.shareName));
            horotype.contentType = (Utility.decodeText(horotype.contentType));
            horotype.fallBackIconName = (Utility.decodeText(horotype.fallBackIconName));
            horotype.lastUpdated = (Utility.decodeText(horotype.lastUpdated));
            horotype.tags = (Utility.decodeText(horotype.tags));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addDummyFavoriets() {
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            favoriteDao = daoSession.getFavoritesDao();

            Favorites addfavorite = new Favorites();
            addfavorite.horogroupId = (1);
            addfavorite.horotypeId = (111);
            favoriteDao.insert(addfavorite);

            addfavorite.horogroupId = (1);
            addfavorite.horotypeId = (112);
            favoriteDao.insert(addfavorite);

            addfavorite.horogroupId = (1);
            addfavorite.horotypeId = (113);
            favoriteDao.insert(addfavorite);

            addfavorite.horogroupId = (4);
            addfavorite.horotypeId = (36500);
            favoriteDao.insert(addfavorite);

            addfavorite.horogroupId = (4);
            addfavorite.horotypeId = (36502);
            favoriteDao.insert(addfavorite);

            addfavorite.horogroupId = (5);
            addfavorite.horotypeId = (55);
            favoriteDao.insert(addfavorite);

            addfavorite.horogroupId = (1);
            addfavorite.horotypeId = (56);
            favoriteDao.insert(addfavorite);

            addfavorite.horogroupId = (1);
            addfavorite.horotypeId = (57);
            favoriteDao.insert(addfavorite);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveHoroscope(Horoscope horoscope) {
        try {
            horoscope.date = (Utility.encodeText(horoscope.date));
            horoscope.dateInWords = (Utility.encodeText(horoscope.dateInWords));
            horoscope.name = (Utility.encodeText(horoscope.name));
            horoscope.horoscopeText = (Utility.encodeText(horoscope.horoscopeText));
            horoscope.shareUrl = (Utility.encodeText(horoscope.shareUrl));
            horoscope.tinyUrl = (Utility.encodeText(horoscope.tinyUrl));
            horoscope.lowBandwidthUrl = (Utility.encodeText(horoscope.lowBandwidthUrl));
            horoscope.normalBandwidthUrl = (Utility.encodeText(horoscope.normalBandwidthUrl));
            horoscope.highBandwidthUrl = (Utility.encodeText(horoscope.highBandwidthUrl));

            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            horoscopeDao = daoSession.getHoroscopeDao();

            if (myApp.getDatabase().isOpen()) {
                DeleteQuery<Horoscope> deleteHoroscope = horoscopeDao.queryBuilder().where(HoroscopeDao.Properties.HoroId.eq(horoscope.horoId), HoroscopeDao.Properties.HorosignId.eq(horoscope.horosignId), HoroscopeDao.Properties.HorotypeId.eq(horoscope.horotypeId)).buildDelete();
                deleteHoroscope.executeDeleteWithoutDetachingEntities();
                horoscopeDao.insert(horoscope);
            } else if (myApp.getDatabase().isDbLockedByCurrentThread()) {
                DeleteQuery<Horoscope> deleteHoroscope = horoscopeDao.queryBuilder().where(HoroscopeDao.Properties.HoroId.eq(horoscope.horoId), HoroscopeDao.Properties.HorosignId.eq(horoscope.horosignId), HoroscopeDao.Properties.HorotypeId.eq(horoscope.horotypeId)).buildDelete();
                deleteHoroscope.executeDeleteWithoutDetachingEntities();
                horoscopeDao.insert(horoscope);
            }

            horoscope.date = (Utility.decodeText(horoscope.date));
            horoscope.dateInWords = (Utility.decodeText(horoscope.dateInWords));
            horoscope.name = (Utility.decodeText(horoscope.name));
            horoscope.horoscopeText = (Utility.decodeText(horoscope.horoscopeText));
            horoscope.shareUrl = (Utility.decodeText(horoscope.shareUrl));
            horoscope.tinyUrl = (Utility.decodeText(horoscope.tinyUrl));
            horoscope.lowBandwidthUrl = (Utility.decodeText(horoscope.lowBandwidthUrl));
            horoscope.normalBandwidthUrl = (Utility.decodeText(horoscope.normalBandwidthUrl));
            horoscope.highBandwidthUrl = (Utility.decodeText(horoscope.highBandwidthUrl));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Horomenu getMenu() {
        Horomenu menu = new Horomenu();
        try {
            // openDB();
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            horomenuDao = daoSession.getHoromenuDao();

            cursor = myApp.getDatabase().query(horomenuDao.getTablename(), horomenuDao.getAllColumns(), null, null, null, null, null);
            cursor.moveToFirst();

            while (cursor.isAfterLast() == false) {
                menu.date = (cursor.getString(3));
                menu.menuDate = (cursor.getString(4));

                menu.date = (Utility.decodeText(menu.date));
                menu.menuDate = (Utility.decodeText(menu.menuDate));
                cursor.moveToNext();
            }

//			String query = "SELECT * FROM " + Constants.tableMenu;
//			menuCursor = db.rawQuery(query, null);
//
//			menuCursor.moveToFirst();
//			while (menuCursor.isAfterLast() == false) {
//
//				menu.Date(menuCursor.getString(0));
//				menu.MenuDate(menuCursor.getString(1));
//
//				menu.Date(Utility.decodeText(menu.getDate()));
//				menu.MenuDate(Utility.decodeText(menu.getMenuDate()));
//
//				menuCursor.moveToNext();
//			}
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            // closeDB();
        }

        return menu;
    }

    public ArrayList<Horogroup> getHorogroups(int hgId, boolean fav) {
        ArrayList<Horogroup> hgList = new ArrayList<Horogroup>();
        Cursor hgCursor;
        try {
            Query<Horogroup> query;
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            horogroupDao = daoSession.getHorogroupDao();
            favoriteDao = daoSession.getFavoritesDao();
            if (hgId != Consts.signs.none.idx) {
                if (fav) {
                    hgCursor = myApp.getDatabase().query(favoriteDao.getTablename(), favoriteDao.getAllColumns(), null, null, null, null, null);
                    hgCursor.moveToFirst();
                    query = horogroupDao.queryBuilder().where(new StringCondition("T.HORO_ID = " + hgId + "AND T.HORO_ID IN (SELECT F.HOROGROUP_ID FROM FAVORITES F) ORDER BY T.SORT_ORDER ASC")).build();
                } else {
                    query = horogroupDao.queryBuilder().where(HorogroupDao.Properties.HoroId.eq(hgId)).orderAsc(HorogroupDao.Properties.SortOrder).build();
                }
            } else {
                if (fav) {
                    hgCursor = myApp.getDatabase().query(favoriteDao.getTablename(), favoriteDao.getAllColumns(), null, null, null, null, null);
                    hgCursor.moveToFirst();
                    //List result_fav = favoriteDao.queryBuilder().build().list();
                    query = horogroupDao.queryBuilder().where(new StringCondition("T.HORO_ID IN (SELECT F.HOROGROUP_ID FROM FAVORITES F) ORDER BY T.SORT_ORDER ASC")).build();
                } else {
                    query = horogroupDao.queryBuilder().orderAsc(HorogroupDao.Properties.SortOrder).build();
                }
            }
            List<Horogroup> result = query.list();
            for (int i = 0; i < result.size(); i++) {
                Horogroup hg = result.get(i);
                hg.name = (Utility.decodeText(hg.name));
                hg.contentType = (Utility.decodeText(hg.contentType));
                hg.tags = (Utility.decodeText(hg.tags));
                hgList.add(hg);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return hgList;
    }

    public ArrayList<Horotype> getHorotypes(int hgId, int htId, boolean fav) {
        ArrayList<Horotype> htList = new ArrayList<Horotype>();
        //Cursor htCursor = null;
        try {
            Query<Horotype> query;
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            horotypeDao = daoSession.getHorotypeDao();
            favoriteDao = daoSession.getFavoritesDao();
            if (hgId != Consts.signs.none.idx && htId != Consts.signs.none.idx) {
                if (fav) {
                    //htCursor = myApp.getDatabase().query(favoriteDao.getTablename(), favoriteDao.getAllColumns(), null, null, null, null, null);
                    query = horotypeDao.queryBuilder().where(new StringCondition("T.HOROGROUP_ID = " + hgId + " AND T.HORO_ID = " + htId + " AND T.HORO_ID IN (SELECT F.HOROTYPE_ID FROM FAVORITES F) ORDER BY T.SORT_ORDER ASC")).build();
                } else {
                    query = horotypeDao.queryBuilder().where(HorotypeDao.Properties.HorogroupId.eq(hgId), HorotypeDao.Properties.HoroId.eq(htId)).orderAsc(HorotypeDao.Properties.SortOrder).build();
                }
            } else if (hgId != Consts.signs.none.idx && htId == Consts.signs.none.idx) {
                if (fav) {
                    query = horotypeDao.queryBuilder().where(new StringCondition("T.HOROGROUP_ID = " + hgId + " AND T.HORO_ID IN (SELECT F.HOROTYPE_ID FROM FAVORITES F) ORDER BY T.SORT_ORDER ASC")).build();
                } else {
                    query = horotypeDao.queryBuilder().where(HorotypeDao.Properties.HorogroupId.eq(hgId)).orderAsc(HorotypeDao.Properties.SortOrder).build();
                }
            } else if (hgId == Consts.signs.none.idx && htId != Consts.signs.none.idx) {
                if (fav) {
                    query = horotypeDao.queryBuilder().where(new StringCondition("T.HORO_ID = " + htId + " AND T.HORO_ID IN (SELECT F.HOROTYPE_ID FROM FAVORITES F) ORDER BY T.SORT_ORDER ASC")).build();
                } else {
                    query = horotypeDao.queryBuilder().where(HorotypeDao.Properties.HoroId.eq(htId)).orderAsc(HorotypeDao.Properties.SortOrder).build();
                }
            } else {
                if (fav) {
                    query = horotypeDao.queryBuilder().where(new StringCondition("T.HORO_ID IN (SELECT F.HOROTYPE_ID FROM FAVORITES F) ORDER BY T.SORT_ORDER ASC")).build();
                } else {
                    query = horotypeDao.queryBuilder().orderAsc(HorotypeDao.Properties.SortOrder).build();
                }
            }

            List<Horotype> result = query.list();
            for (int i = 0; i < result.size(); i++) {
                Horotype ht = result.get(i);
                ht.name = (Utility.decodeText(ht.name));
                ht.shortName = (Utility.decodeText(ht.shortName));
                ht.mediumName = (Utility.decodeText(ht.mediumName));
                ht.longName = (Utility.decodeText(ht.longName));
                ht.shareName = (Utility.decodeText(ht.shareName));
                ht.contentType = (Utility.decodeText(ht.contentType));
                ht.fallBackIconName = (Utility.decodeText(ht.fallBackIconName));
                ht.tags = (Utility.decodeText(ht.tags));
                htList.add(ht);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return htList;
    }

    public ArrayList<Horoscope> getHoroscopes(int signId, int htId) {
        ArrayList<Horoscope> hsList = new ArrayList<>();
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            horoscopeDao = daoSession.getHoroscopeDao();
            Query<Horoscope> query = horoscopeDao.queryBuilder().where(HoroscopeDao.Properties.HorosignId.eq(signId), HoroscopeDao.Properties.HorotypeId.eq(htId)).orderAsc(HoroscopeDao.Properties.SortOrder).build();

            List<Horoscope> getHoroscope = query.list();
            for (int i = 0; i < getHoroscope.size(); i++) {
                Horoscope hs = getHoroscope.get(i);
                hs.date = (Utility.decodeText(hs.date));
                hs.dateInWords = (Utility.decodeText(hs.dateInWords));
                hs.name = (Utility.decodeText(hs.name));
                hs.horoscopeText = (Utility.decodeText(hs.horoscopeText));
                hs.shareUrl = (Utility.decodeText(hs.shareUrl));
                hs.tinyUrl = (Utility.decodeText(hs.tinyUrl));
                hs.lowBandwidthUrl = (Utility.decodeText(hs.lowBandwidthUrl));
                hs.normalBandwidthUrl = (Utility.decodeText(hs.normalBandwidthUrl));
                hs.highBandwidthUrl = (Utility.decodeText(hs.highBandwidthUrl));
                hsList.add(hs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return hsList;
    }

    public void clearCahche() throws Exception {
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();

            horomenuDao = daoSession.getHoromenuDao();
            horomenuDao.deleteAll();

            horotypeDao = daoSession.getHorotypeDao();
            horotypeDao.deleteAll();

            horoscopeDao = daoSession.getHoroscopeDao();
            horoscopeDao.deleteAll();

            horogroupDao = daoSession.getHorogroupDao();
            horogroupDao.deleteAll();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new Exception(Consts.error_clear_cache);
        }
    }

    public boolean isFavoriteHorotype(int horotypeId) {
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            favoriteDao = daoSession.getFavoritesDao();
            return favoriteDao.queryBuilder().where(FavoritesDao.Properties.HorotypeId.eq(horotypeId)).build().list().size() > 0;
        } catch (SQLException e) {
            return false;
        }
    }

    public void addFavoriteHorotype(int horotypeId) {
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            favoriteDao = daoSession.getFavoritesDao();
            horotypeDao = daoSession.getHorotypeDao();
            List<Horotype> fav = horotypeDao.queryBuilder().where(HorotypeDao.Properties.HoroId.eq(horotypeId)).build().list();

            int hgId = 0;
            for (int i = 0; i < fav.size(); i++) {
                Horotype horo = new Horotype();
                horo = fav.get(i);
                hgId = horo.horogroupId;
            }

            favoriteDao.insert(new Favorites(null, hgId, horotypeId));
            List<Favorites> result = favoriteDao.queryBuilder().build().list();
            for (int j = 0; j < result.size(); j++) {
                result.get(j);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeFavoriteHorotype(int horotypeId) {
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            favoriteDao = daoSession.getFavoritesDao();
            favoriteDao.queryBuilder().where(FavoritesDao.Properties.HorotypeId.eq(horotypeId)).buildDelete().executeDeleteWithoutDetachingEntities();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveUserData(int lastSign, int lastTab) {
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            appdataDao = daoSession.getAppDataDao();
            appdataDao.deleteAll();
            appdataDao.insert(new AppData(null, lastSign, lastTab));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int[] getUserData() {
        Cursor dtCursor = null;
        int dtArray[] = {Consts.signs.Aries.idx, Consts.menu_all};
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            appdataDao = daoSession.getAppDataDao();
            dtCursor = myApp.getDatabase().query(appdataDao.getTablename(), appdataDao.getAllColumns(), null, null, null, null, null);
            dtCursor.moveToFirst();
            while (!dtCursor.isAfterLast()) {
                dtArray[0] = dtCursor.getInt(1);
                dtArray[1] = dtCursor.getInt(2);
                dtCursor.moveToNext();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (dtCursor != null) dtCursor.close();
        }

        return dtArray;
    }

    // **************** Storing functions for Purchased Reading **************//
    public void saveBirthDetails(Birthdetails info) {
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            birthDetailDao = daoSession.getBirthdetailsDao();
            birthDetailDao.insert(info);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Birthdetails getBirthDetails() {
        Birthdetails info = null;
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            birthDetailDao = daoSession.getBirthdetailsDao();
            List<Birthdetails> result = birthDetailDao.queryBuilder().build().list();
            for (int i = 0; i < result.size(); i++) {
                info = result.get(i);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return info;
    }

    public void saveBirthDetails(String birthName, String birthDate, String birthTime, String birthCity, String birthState, int birthStatePosition, String birthCountry, int birthCountryPosition, String gender) {
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            birthDetailDao = daoSession.getBirthdetailsDao();
            birthDetailDao.insert(new Birthdetails(null, birthName, birthDate, birthTime, birthCity.replaceAll("'", "''"), birthState, birthCountry, gender, birthStatePosition, birthCountryPosition));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveLocationDetails(LocationInfo loc) {
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            locationdao = daoSession.getLocationInfoDao();
            locationdao.deleteAll();
            locationdao.insert(loc);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public LocationInfo getLocationDetails() {
        LocationInfo loc = new LocationInfo();
        Cursor infoCursor = null;
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            locationdao = daoSession.getLocationInfoDao();
            infoCursor = myApp.getDatabase().query(locationdao.getTablename(), locationdao.getAllColumns(), null, null, null, null, null);
            infoCursor.moveToFirst();
            while (!infoCursor.isAfterLast()) {
                loc.county = (infoCursor.getString(1));
                loc.state = (infoCursor.getString(2));
                loc.city = (infoCursor.getString(3));
                loc.country = (infoCursor.getString(4));
                loc.longitude = (infoCursor.getString(5));
                loc.latitude = (infoCursor.getString(6));
                loc.dst = (infoCursor.getString(7));
                loc.tzDir = (infoCursor.getString(8));
                loc.tzHour = (infoCursor.getString(9));
                loc.tzMinute = (infoCursor.getString(10));
                infoCursor.moveToNext();
            }

            loc.longitude = (loc.longitude.replace("''", "'"));
            loc.latitude = (loc.latitude.replace("''", "'"));
            loc.city = (loc.city.replaceAll("''", "'"));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (infoCursor != null) infoCursor.close();
        }

        return loc;
    }

    public int saveProductDetails(Product purchasedProduct) {
        int productid = 0;
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            productinfoDao = daoSession.getProductInfoDao();
            List<ProductInfo> result = productinfoDao.queryBuilder().build().list();

            for (int i = 0; i < result.size(); i++) {
                ProductInfo productresult = result.get(i);
                if (Integer.parseInt(purchasedProduct.ares_Id) == productresult.id) {
                    return productresult.productId;
                }
            }
            productid = Integer.parseInt(purchasedProduct.ares_Id);
            productinfoDao.insert(new ProductInfo(null, productid, purchasedProduct.name, purchasedProduct.imageURL, purchasedProduct.description));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return productid;
    }

    // Retrieving One purchased Product
    public Product getPurchasedProduct(int productID) {
        Product product = new Product();
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            productinfoDao = daoSession.getProductInfoDao();
            List<ProductInfo> result = productinfoDao.queryBuilder().where(ProductInfoDao.Properties.ProductId.eq(productID)).build().list();
            for (int i = 0; i < result.size(); i++) {
                ProductInfo info = result.get(i);
                product.ares_Id = (info.productId.toString());
                product.name = (info.productTitle);
                product.imageURL = (info.productImageUrl);
                product.description = (info.productDescription);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return product;
    }

    // Retrieving One purchased Product
    public ArrayList<PendingPurchases> getPendingProducts() {
        ArrayList<PendingPurchases> list = new ArrayList<PendingPurchases>();
        Cursor cursor = null;
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            pendingpurchaseDao = daoSession.getPendingPurchasesDao();
            List<PendingPurchases> result = pendingpurchaseDao.queryBuilder().build().list();
            for (int i = 0; i < result.size(); i++) {
                PendingPurchases product = new PendingPurchases();
                product = result.get(i);
                list.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return list;
    }

    public void deletePendingReading(int purchaseId) {
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            pendingpurchaseDao = daoSession.getPendingPurchasesDao();
            pendingpurchaseDao.queryBuilder().where(PendingPurchasesDao.Properties.PurchaseId.eq(purchaseId)).buildDelete().executeDeleteWithoutDetachingEntities();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Save pending Reading in the Database
    public int savePendingReading(String url, String type, int position) {
        int readingid = -1;
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            pendingpurchaseDao = daoSession.getPendingPurchasesDao();
            readingid = (int) pendingpurchaseDao.insert(new PendingPurchases(null, null, position, url, type));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readingid;
    }

    // Save Purchased Reading in the Database
    public int savePurchasedReading(Reading reading, String time, int productid) {
        Cover c = reading.cover;
        int readingid = 0;
        try {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            readingsDao = daoSession.getReadingsDao();
//			Consts.readingId = preferences.getInt("readingId", 999);
            Readings readings = new Readings();
            readings.coverByLine = (c.byLine);
            readings.coverSubTitle = (c.subTitle);
            readings.coverTitle = (c.title);
            readings.productId = (productid);
            readings.time = (time);
//			readings.readingId = (Consts.readingId);
//			readingid = Consts.readingId;
//			Consts.readingId++;
//			preferences.edit().putInt("readingId", Consts.readingId).commit();
            readingsDao.insert(readings);
            List<Readings> testResult = readingsDao.queryBuilder().where(ReadingsDao.Properties.ProductId.eq(productid)).build().list();
            for (int i = 0; i < testResult.size(); i++)
                testResult.get(i);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readingid;
    }

    // Save Birth Info details of Purchased Reading
    public void saveBirthInfoForPurchasedReading(Birthdetails info, LocationInfo loc, int readingid) {
        if (loc.city.contains("'")) loc.city = (loc.city.replaceAll("'", "''"));
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            birthinfoDao = daoSession.getBirthInfoForPurchasedReadingsDao();

            BirthInfoForPurchasedReadings birthInfo = new BirthInfoForPurchasedReadings();
            birthInfo.birthName = (info.birthName);
            birthInfo.birthDate = (info.birthDate);
            birthInfo.birthTime = (info.birthTime);
            birthInfo.birthCity = (loc.city);
            birthInfo.birthState = (loc.state);
            birthInfo.birthCountry = (loc.country);
            birthInfo.county = (loc.county);
            birthInfo.longitude = (loc.longitude);
            birthInfo.latitude = (loc.latitude);
            birthInfo.dst = (loc.dst);
            birthInfo.tzdir = (loc.tzDir);
            birthInfo.tzhour = (loc.tzHour);
            birthInfo.tzminute = (loc.tzMinute);
            birthInfo.gender = (info.gender);
            birthInfo.readingId = (readingid);
            readingid = (int) birthinfoDao.insert(birthInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Save Outer Sections for the purchased reading in the myDb.getDatabase()
    public void saveReadingSections(Reading reading, int readingid) {
        ArrayList<ArrayList<Section>> interpretation = new ArrayList<>();
        interpretation = reading.interpretation;

        if (!(interpretation == null)) {
            interpretation.trimToSize();
            try {
                daoMaster = new DaoMaster(myApp.getDatabase());
                daoSession = daoMaster.newSession();
                outersectionDao = daoSession.getOuterSectionsDao();
                innersectionDao = daoSession.getInnerSectionsDao();
                ContentValues values = new ContentValues();

                for (int i = 0; i < interpretation.size(); i++) {
                    ArrayList<Section> innerSectionList = new ArrayList<>();
                    innerSectionList = interpretation.get(i);
                    OuterSections outersection = new OuterSections();
                    values.put("readingid", readingid);
                    outersection.readingId = (readingid);
                    outersection.outerSectionId = (i);
                    outersectionDao.insert(outersection);

                    for (int j = 0; j < innerSectionList.size(); j++) {
                        Section section = new Section();
                        section = innerSectionList.get(j);

                        InnerSections innersection = new InnerSections();
                        innersection.sectionTitle = (section.title);
                        innersection.sectionImageUrl = (Utility.tokenizeImageURLs(section.imagePaths));
                        innersection.sectionDescription = (section.description);
                        innersection.sectionRelation = (section.relation);
                        innersection.outerSectionId = (i);
                        innersection.innerSectionId = (readingid);
                        innersectionDao.insert(innersection);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // **************** Retrieving functions for Purchased Reading ***************//
    public Reading getPurchasedReadings(int purchasedReadingId) {
        Reading reading = new Reading();

        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            readingsDao = daoSession.getReadingsDao();
            outersectionDao = daoSession.getOuterSectionsDao();
            innersectionDao = daoSession.getInnerSectionsDao();

			/*
			Query<Readings> test = readingsDao.queryBuilder().build();
			List<Readings> testresult = test.list();
			for(int t=0 ; t<testresult.size() ; t++){
				Readings testread = (Readings)testresult.get(t);
			}*/

            List<Readings> result = readingsDao.queryBuilder().where(ReadingsDao.Properties.ReadingId.eq(purchasedReadingId)).build().list();
            for (int i = 0; i < result.size(); i++) {
                Readings readingResult = result.get(i);
                purchasedReadingId = readingResult.readingId;

                Cover cover = new Cover();
                cover.title = (readingResult.coverTitle);
                cover.subTitle = (readingResult.coverSubTitle);
                cover.byLine = (readingResult.coverByLine);

                reading.cover = (cover);
                List<OuterSections> outerResult = outersectionDao.queryBuilder().where(OuterSectionsDao.Properties.ReadingId.eq(purchasedReadingId)).build().list();

                //int count = outerResult.size();
                ArrayList<ArrayList<Section>> interpretationList = new ArrayList<ArrayList<Section>>();
                for (int j = 0; j < outerResult.size(); j++) {
                    OuterSections outer = outerResult.get(j);
                    int outerSectionId = outer.outerSectionId;
                    List<InnerSections> innerResult = innersectionDao.queryBuilder().where(InnerSectionsDao.Properties.OuterSectionId.eq(outerSectionId), InnerSectionsDao.Properties.InnerSectionId.eq(purchasedReadingId)).build().list();
                    ArrayList<Section> sectionList = new ArrayList<>();
                    for (int k = 0; k < innerResult.size(); k++) {
                        InnerSections inner = new InnerSections();
                        inner = innerResult.get(k);
                        Section section = new Section();
                        section.title = (inner.sectionTitle);
                        ArrayList<String> imageURLList = Utility.getImageURLTokens(inner.sectionImageUrl);
                        imageURLList.trimToSize();
                        section.imagePaths = (imageURLList);
                        section.description = (inner.sectionDescription);
                        section.relation = (inner.sectionRelation);
                        sectionList.add(section);
                    }

                    sectionList.trimToSize();
                    interpretationList.add(sectionList);
                }

                interpretationList.trimToSize();
                reading.interpretation = (interpretationList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return reading;
    }

    // getting purchased product types for My "Readings screen" list view
    public ArrayList<Product> getProductTypes() {
        ArrayList<Product> productTypes = new ArrayList<Product>();
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            productinfoDao = daoSession.getProductInfoDao();
            List<ProductInfo> result = productinfoDao.queryBuilder().build().list();

            for (int i = 0; i < result.size(); i++) {
                ProductInfo productinfo = new ProductInfo();
                productinfo = result.get(i);
                Product product = new Product();
                product.ares_Id = (productinfo.productId.toString());
                product.name = (productinfo.productTitle);
                product.description = (productinfo.productDescription);
                product.imageURL = (productinfo.productImageUrl);
                productTypes.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        productTypes.trimToSize();
        return productTypes;
    }

    public ArrayList<Purchase> getPurchasedReadignsForProdutType(int productId) {
        ArrayList<Purchase> purchasedReadingArray = new ArrayList<Purchase>();
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            productinfoDao = daoSession.getProductInfoDao();
            readingsDao = daoSession.getReadingsDao();

            List<Readings> resultReadings = readingsDao.queryBuilder().where(ReadingsDao.Properties.ProductId.eq(productId)).build().list();
            List<ProductInfo> result = productinfoDao.queryBuilder().where(ProductInfoDao.Properties.ProductId.eq(productId)).build().list();

            if (result.size() > 0) {
                String imageURL = result.get(0).productImageUrl;
                for (int j = 0; j < resultReadings.size(); j++) {
                    Readings reading = resultReadings.get(j);
                    Purchase purchase = new Purchase();
                    purchase.productID = (productId);
                    purchase.readingId = (reading.readingId);
                    purchase.purchaseTime = (reading.time);
                    purchase.birthInfoArray = (getBirthNamesForPurchasedReadings(purchase.readingId));
                    purchase.productImageURL = (imageURL);
                    purchasedReadingArray.add(purchase);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        purchasedReadingArray.trimToSize();
        return purchasedReadingArray;
    }

    public ArrayList<Birthdetails> getBirthNamesForPurchasedReadings(int purchasedReadingId) {
        ArrayList<Birthdetails> birthNamesList = new ArrayList<Birthdetails>();
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            birthinfoDao = daoSession.getBirthInfoForPurchasedReadingsDao();

            List<BirthInfoForPurchasedReadings> result = birthinfoDao.queryBuilder().where(BirthInfoForPurchasedReadingsDao.Properties.ReadingId.eq(purchasedReadingId)).build().list();

            for (int i = 0; i < result.size(); i++) {
                BirthInfoForPurchasedReadings purchasedReadings = result.get(i);
                purchasedReadings.birthCity = (purchasedReadings.birthCity.replaceAll("''", "'"));

                Birthdetails personalInfo = new Birthdetails();
                personalInfo.birthDate = (purchasedReadings.birthDate);
                personalInfo.birthName = (purchasedReadings.birthName);
                personalInfo.birthTime = (purchasedReadings.birthTime);
                personalInfo.birthState = (purchasedReadings.birthState);
                personalInfo.birthCountry = (purchasedReadings.birthCountry);
                personalInfo.birthCity = (purchasedReadings.birthCity);
                birthNamesList.add(personalInfo);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        birthNamesList.trimToSize();
        return birthNamesList;
    }

    public int deletePurchasedReading(int readingId, String readingType) {
        int deletedRowCount = 0;
        String productId = "";
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            readingsDao = daoSession.getReadingsDao();
            birthinfoDao = daoSession.getBirthInfoForPurchasedReadingsDao();

            List<Readings> resultPurchased =
                    readingsDao.queryBuilder().where(ReadingsDao.Properties.ReadingId.eq(readingId)).build().list();
            for (int i = 0; i < resultPurchased.size(); i++) {
                Readings reading = new Readings();
                reading = resultPurchased.get(i);
                productId = reading.productId.toString();
            }

            readingsDao.queryBuilder().where(ReadingsDao.Properties.ReadingId.eq(readingId)).buildDelete().executeDeleteWithoutDetachingEntities();
            birthinfoDao.queryBuilder().where(ReadingsDao.Properties.ReadingId.eq(readingId)).buildDelete().executeDeleteWithoutDetachingEntities();
            List<Readings> resultCount = readingsDao.queryBuilder().where(new StringCondition("T.PRODUCT_ID = " + productId + " GROUP BY T.READING_ID")).build().list();
            if (resultCount.size() < 1) {
                productinfoDao.queryBuilder().where(ProductInfoDao.Properties.ProductId.eq(productId)).buildDelete().executeDeleteWithoutDetachingEntities();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return deletedRowCount;
    }

    public String getImageURLForProduct(String producType) {
        String imageURL = null;
        try {
            daoMaster = new DaoMaster(myApp.getDatabase());
            daoSession = daoMaster.newSession();
            productinfoDao = daoSession.getProductInfoDao();
            List<ProductInfo> result = productinfoDao.queryBuilder().where(ProductInfoDao.Properties.ProductTitle.eq(producType)).build().list();
            for (int i = 0; i < result.size(); i++)
                imageURL = result.get(i).productImageUrl;
        } catch (Exception ex) {
            ex.printStackTrace();
            imageURL = null;
        }

        return imageURL;
    }
}