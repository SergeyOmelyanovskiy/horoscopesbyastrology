package com.astrology.iscopes.common;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.zip.GZIPInputStream;

import android.content.Context;

import com.astrology.iscopes.database.IScopesDB;
import com.astrology.iscopes.orm.Horogroup;
import com.astrology.iscopes.orm.Horomenu;
import com.astrology.iscopes.orm.Horoscope;
import com.astrology.iscopes.orm.Horotype;
import com.astrology.iscopes.parser.ParsePList;

public class MgrMenu {

    private static MgrMenu anInstance;
    private int selectedHoroSignId = 0;
    private int selectedHoroTypeId = 0;
    private Context context;

    private Horomenu menu;
    private Horotype selectedHoroType;

    public int menuType = -1;
    public String selectedHoroSignName = "";

    private MgrMenu(Context context) {
        this.menu = new Horomenu();
        this.selectedHoroType = new Horotype();
        this.context = context.getApplicationContext();
    }

    public Horomenu getMenu() {
        if (null == menu) this.menu = new Horomenu();
        return this.menu;
    }

    public Horotype getHorotype() {
        if (null == selectedHoroType) this.selectedHoroType = new Horotype();
        return this.selectedHoroType;
    }

    public static synchronized MgrMenu getInstance(Context context) {
        if (anInstance == null) anInstance = new MgrMenu(context);
        return anInstance;
    }

    public void loadMenuFromServer() throws Exception {
        try {
            /************* Loading Menu Diagnostics Fetching **/
            URL menuUrl = new URL(MgrShared.getInstance(context).getURL4Menu());
            HttpURLConnection connection = (HttpURLConnection) menuUrl.openConnection();
            InputStream stream = connection.getInputStream();
            if ("gzip".equals(connection.getContentEncoding())) {
                stream = new GZIPInputStream(stream);
            }
            /************* Loading Menu Diagnostics Parsing **/
            menu = ParsePList.parseMenuFromXMLString(stream);
        } catch (Exception e) {
            throw new Exception(Consts.error_parser_menu);
        }
    }

    public boolean isMenuAvailable() {
        if (!menu.menuDate.equalsIgnoreCase("")) {
            return true;
        }

        try {
            menu = getMenu(Consts.menu_all);
            return !menu.menuDate.equalsIgnoreCase("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean isHoroDetailsAvailable() {
        getHoroDetails();
        return selectedHoroType != null && selectedHoroType.getHoroscope().size() > 0;
    }

    public boolean isDateChanged() {
        //boolean isDateChanged = false;
        if (menu.menuDate.equalsIgnoreCase("")) {
            return true;
        }

        try {
            String dateStr = new SimpleDateFormat("yyyyMMdd", Locale.US).format(new Date());
            return !menu.menuDate.equalsIgnoreCase(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public void saveMenu() {
        IScopesDB isdb = IScopesDB.getInstance(context);
        if (isdb != null) {
            isdb.saveMenu(menu);
            Iterator<Horogroup> hgItr = menu.getHorogroup().iterator();

            while (hgItr.hasNext()) {
                Horogroup hg = hgItr.next();
                isdb.saveHorogroup(hg);
                Iterator<Horotype> htItr = hg.getHorotype().iterator();

                while (htItr.hasNext()) {
                    Horotype ht = htItr.next();
                    ht.horogroupId = (hg.horoId);
                    isdb.saveHorotype(ht);
                }
            }
        }
    }

    public Horomenu getMenu(int menuType) throws Exception {
        try {
            if (this.menuType == menuType) return this.menu;
            if (menuType == Consts.menu_all || menuType == Consts.menu_favorite) {
                this.menuType = menuType;
                this.menu = IScopesDB.getInstance(context).getMenu();
                this.menu.horogroup = (IScopesDB.getInstance(context).getHorogroups(Consts.signs.none.idx, menuType == Consts.menu_favorite));
                Iterator<Horogroup> hgItr = this.menu.getHorogroup().iterator();
                while (hgItr.hasNext()) {
                    Horogroup hg = hgItr.next();
                    hg.setHorotype(IScopesDB.getInstance(context).getHorotypes(hg.horoId, Consts.signs.none.idx, menuType == Consts.menu_favorite));
                }
            }

            return this.menu;
        } catch (Exception e) {
            throw new Exception(Consts.error_db);
        }
    }

    public void getHoroDetails() {
        IScopesDB isdb = IScopesDB.getInstance(context);
        ArrayList<Horotype> horoTypes = isdb.getHorotypes(Consts.signs.none.idx, this.selectedHoroTypeId, false);
        if (horoTypes.size() > 0) {
            this.selectedHoroType = horoTypes.get(0);
            this.selectedHoroType.horoscope = (isdb.getHoroscopes(this.selectedHoroSignId, this.selectedHoroType.horoId));
        }
    }

    public void loadHoroscopeDetailsFromServer() throws Exception {
        try {
            URL detailsUrl = new URL(MgrShared.getInstance(context).getURL4HoroDetails(this.selectedHoroTypeId, this.selectedHoroSignId));
            URLConnection connection = detailsUrl.openConnection();
            InputStream stream = connection.getInputStream();
            if ("gzip".equals(connection.getContentEncoding()))
                stream = new GZIPInputStream(stream);

            // InputStream is = detailsUrl.openStream();
            this.selectedHoroType = ParsePList.parseHoroscopeFromXMLString(stream);
            if (this.selectedHoroType.getHoroscope().size() > 1) {
                // Keep the middle Horotype if name is empty. Removing first element if requierd
                Horoscope tempHS = this.selectedHoroType.getHoroscope().get(0);
                if (tempHS.name.length() == 0) this.selectedHoroType.getHoroscope().remove(0);

                // Removing last horoscope if required
                int lastIndex = this.selectedHoroType.getHoroscope().size() - 1;
                tempHS = this.selectedHoroType.getHoroscope().get(lastIndex);
                if (tempHS.name.length() == 0)
                    this.selectedHoroType.getHoroscope().remove(lastIndex);
            }

            Iterator<Horoscope> hsItr = this.selectedHoroType.getHoroscope().iterator();
            while (hsItr.hasNext()) {
                Horoscope hs = hsItr.next();
                hs.horosignId = (this.selectedHoroSignId);
                hs.horotypeId = (this.selectedHoroTypeId);
                IScopesDB.getInstance(context).saveHoroscope(hs);
            }
        } catch (Exception e) {
            // Log.i("Exception Caught Fetch-Parse-Save Horoscope Details ",e.toString());
            throw new Exception(Consts.error_parser_horo);
        }
    }

    public void nextHoroscope(int direction) {
        Iterator<Horogroup> hgItr = this.menu.getHorogroup().iterator();
        ArrayList<Horotype> tempHoroTypeList = new ArrayList<>();

        int htIndex = -1;
        int counter = -1;

        while (hgItr.hasNext()) {
            Iterator<Horotype> htItr = hgItr.next().getHorotype().iterator();
            while (htItr.hasNext()) {
                counter++;
                Horotype ht = htItr.next();
                tempHoroTypeList.add(ht);
                if (ht.horoId == this.selectedHoroTypeId) htIndex = counter;
            }
        }

        htIndex = htIndex + direction; // Determining new horotype
        if (htIndex >= tempHoroTypeList.size()) { // select first
            setSelectedHorotypeId(tempHoroTypeList.get(0).horoId);
        } else if (htIndex < 0) { // select last
            setSelectedHorotypeId(tempHoroTypeList.get(tempHoroTypeList.size() - 1).horoId);
        } else { // select one at the index
            setSelectedHorotypeId(tempHoroTypeList.get(htIndex).horoId);
        }
    }

    public void clearCahe() throws Exception {
        try {
            IScopesDB.getInstance(context).clearCahche();
            this.menu.date = ("");
            this.menu.menuDate = ("");
            this.menu.horogroup = (new ArrayList<>());
            this.menuType = -1;
        } catch (Exception e) {
            throw new Exception(Consts.error_clear_cache);
        }
    }

    public boolean isFavorite(int horoTypeId) {
        return (IScopesDB.getInstance(context)).isFavoriteHorotype(horoTypeId);
    }

    public void addRemoveFavorite(int horoTypeId) {
        if (isFavorite(horoTypeId)) {
            IScopesDB.getInstance(context).removeFavoriteHorotype(horoTypeId);
        } else {
            IScopesDB.getInstance(context).addFavoriteHorotype(horoTypeId);
        }
    }

    public void setSelectedHorosignId(int selectedHoroSignId) {
        this.selectedHoroSignId = selectedHoroSignId;
        this.selectedHoroSignName = Utility.getSignName(selectedHoroSignId);
    }

    public void setSelectedHorotypeId(int selectedHorotypeId) {
        this.selectedHoroTypeId = selectedHorotypeId;
        this.selectedHoroType.horoId = (selectedHorotypeId);
    }
}