package com.astrology.iscopes.common;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;

import com.astrology.iscopes.bo.Product;
import com.astrology.iscopes.database.IScopesDB;
import com.astrology.iscopes.orm.LocationInfo;

public class MgrShared {

    //	private Session session;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.US);
    private Context context;

    public ArrayList<LocationInfo> locations;
    public ArrayList<Product> products = new ArrayList<>();

    // Variables to maintain state across different application launches
    public int selectedSign = Consts.signs.Aries.idx;
    public int selectedMenu = Consts.menu_all;

    private MgrShared(Context context) {
        this.context = context.getApplicationContext();
        getStates();
    }

    private static MgrShared anInstance;

    public static MgrShared getInstance(Context context) {
        if (anInstance == null) {
            anInstance = new MgrShared(context);
        }

        return anInstance;
    }

    public boolean isNetworkAvailable() {
        NetworkInfo[] netInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (!ni.isConnected()) {
                continue;
            }

            if (ni.getTypeName().equalsIgnoreCase("WIFI") || ni.getTypeName().equalsIgnoreCase("MOBILE")) {
                return true;
            }
        }

        return false;
    }

    public String getURL4Menu() {
        return Consts.url_menu.replaceAll("DATE", sdf.format(new Date()));
    }

    public String getURL4HoroDetails(int horoId, int signId) {
        String detailsURL = Consts.url_horo.replaceAll("DATE", sdf.format(new Date()));
        detailsURL = detailsURL.replaceAll("HOROSCOPE_ID", String.format(Locale.US, "%05d", horoId));
        detailsURL = detailsURL.replaceAll("SIGN_ID", String.format(Locale.US, "%02d", signId));
        return detailsURL;
    }

    public boolean isTablet() {
        try {
            // Compute screen size
            DisplayMetrics dm = context.getResources().getDisplayMetrics();
            float screenWidth = dm.widthPixels / dm.xdpi;
            float screenHeight = dm.heightPixels / dm.ydpi;
            // Tablet devices should have a screen size greater than 6 inches
            return Math.sqrt(Math.pow(screenWidth, 2) + Math.pow(screenHeight, 2)) >= 6;
        } catch (Throwable t) {
            // Log.e("Determine phone-tablet", "Failed to compute screen size");
            return false;
        }
    }

    // Maintaining states across different application launches
    public void setStates() {
        IScopesDB.getInstance(context).saveUserData(selectedSign, selectedMenu);
    }

    public void getStates() {
        int dtArray[] = IScopesDB.getInstance(context).getUserData();
        selectedSign = dtArray[0];
        selectedMenu = dtArray[1];
    }

    public void initProductListLocal(InputStream is) {
        products.clear();
        //products = ParseProductListXML.get(is);
    }

    public Product getProduct(int position) {
        return products.get(position);
    }

    public LocationInfo getLocation(int position) {
        return locations.get(position);
    }
}