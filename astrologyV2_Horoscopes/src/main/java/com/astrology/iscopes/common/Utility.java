package com.astrology.iscopes.common;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;

import androidx.appcompat.app.AlertDialog;

import com.astrology.iscopes.R;
import com.astrology.iscopes.bo.Product;
import com.astrology.iscopes.orm.Horoscope;
import com.astrology.iscopes.orm.Horotype;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;

public class Utility {

    public static int FINISH_ACTIVITY = 3;

    /******* new ***********/
    public static float PixelsToDp(Context c, float px) {
        return px / (c.getResources().getDisplayMetrics().densityDpi / 160f);
    }

    public static float DpToPixel(Context c, float dp) {
        return dp * (c.getResources().getDisplayMetrics().densityDpi / 160f);
    }

    public static void showAlert(Context c, String message) {
        showAlert(c, c.getString(R.string.alert_title), message);
    }

    public static void showAlert(Context c, String title, String message) {
        new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    public static CharSequence[] share_items(Context c) {
        return new CharSequence[]{
                c.getString(R.string.share_by_email),
                c.getString(R.string.share_by_twitter),
                c.getString(R.string.cancel)};
    }


    public static String getVersionNumber(Context c) {
        String version = "v2.3";
        try {
            version = c.getString(R.string.app_version_short, c.getApplicationContext().getPackageManager().getPackageInfo(c.getPackageName(), 0).versionName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version + (Consts.is_debug ? "-dev" : "");
    }

    public static String getVersionNumberLong(Context c) {
        String version = "version 2.3";
        try {
            version = c.getString(R.string.app_version_long, c.getApplicationContext().getPackageManager().getPackageInfo(c.getPackageName(), 0).versionName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version + (Consts.is_debug ? "-dev" : "");
    }

    public static String getString(Bundle bundle, String key, String defaultValue) {
        if (bundle == null) return defaultValue;
        return bundle.getString(key, defaultValue);
    }

    /****************/
    public static void debug_log(String msg) {
        if (Consts.is_debug) System.out.println(msg);
    }

    public static String encodeText(String str) {
        str = str.replaceAll("'", "''");
        str = str.replaceAll("'", "\'");
        return str;
    }

    public static String decodeText(String str) {
        str = str.replaceAll("''", "'");
        str = str.replaceAll("\'", "'");
        return str;
    }

    public static String getHorotypeTextWithSign(String horotype, String horosign) {
        String newName = horotype;
        newName = newName.replaceAll(Consts.sign_name_holder, horosign);
        return newName;
    }

    public static String getHoroscopeShareName(String horoText, String horoName, String horosign) {
        String newText = horoText;
        newText = newText.replaceAll(Consts.sign_name_holder, horosign);
        newText = newText.replaceAll("__SCOPE_NAME__", horoName);
        return newText;
    }

    public static String getSignName(int signId) {
        Consts.signs sign = Consts.signs.get(signId);
        if (sign != Consts.signs.none)
            return sign.name();
        else if (signId == Consts.signs.Contact.idx)
            return Consts.signs.Contact.name();
        else
            return "";
    }

    @SuppressWarnings("deprecation")
    public static String formatDateForPurchaseReadingURL(String dateString) {
        StringTokenizer sTokenizer = new StringTokenizer(dateString);

        Date date = new Date();
        date.setMonth(Integer.valueOf(sTokenizer.nextToken("/").trim()) - 1);
        date.setDate(Integer.valueOf(sTokenizer.nextToken("/").trim()));
        date.setYear(Integer.valueOf(sTokenizer.nextToken("/").trim()) - 1900);

        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
        return sdf.format(date);
    }

    @SuppressWarnings("deprecation")
    public static int signIdForDate(Date date) {
        int MMDD = ((date.getMonth() + 1) * 100) + date.getDate();

        if (MMDD >= 120 && MMDD <= 218) return Consts.signs.Aquarius.idx;
        else if (MMDD >= 219 && MMDD <= 320) return Consts.signs.Pisces.idx;
        else if (MMDD >= 321 && MMDD <= 419) return Consts.signs.Aries.idx;
        else if (MMDD >= 420 && MMDD <= 520) return Consts.signs.Taurus.idx;
        else if (MMDD >= 521 && MMDD <= 620) return Consts.signs.Gemini.idx;
        else if (MMDD >= 621 && MMDD <= 722) return Consts.signs.Cancer.idx;
        else if (MMDD >= 723 && MMDD <= 822) return Consts.signs.Leo.idx;
        else if (MMDD >= 823 && MMDD <= 922) return Consts.signs.Virgo.idx;
        else if (MMDD >= 923 && MMDD <= 1022) return Consts.signs.Libra.idx;
        else if (MMDD >= 1023 && MMDD <= 1121) return Consts.signs.Scorpio.idx;
        else if (MMDD >= 1122 && MMDD <= 1221) return Consts.signs.Sagittarius.idx;
        else if (MMDD >= 1222 || MMDD <= 119) return Consts.signs.Capricorn.idx;

        return Consts.signs.none.idx;
    }

    public static String dateRangeForSignId(Context c, int signId) {
        if (Consts.signs.get(signId) != Consts.signs.none) {
            return c.getResources().getStringArray(R.array.dates)[signId];
        }

        return "Unknown";
    }

    public static String monthFromDate(Date date) {
        return new SimpleDateFormat("MMMM", Locale.US).format(date);
    }

    @SuppressWarnings("deprecation")
    public static String formatDateForDaily(String dStr) {
        String dateStr = "";
        String suffix = "";
        try {
            Date date = new SimpleDateFormat("yyyyMMdd", Locale.US).parse(dStr);
            dateStr = new SimpleDateFormat("EEEE, MMMM d", Locale.US).format(date);
            switch (date.getDate() % 10) {
                case 1:
                    suffix = "st";
                    break;
                case 2:
                    suffix = "nd";
                    break;
                case 3:
                    suffix = "rd";
                    break;
                default:
                    suffix = "th";
                    break;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateStr + suffix;
    }

    public static Date dateFormString(String dStr) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(dStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getDateForSharing(Horotype ht, Horoscope hs) {
        if (ht.interval != Consts.interval_default && ht.interval != Consts.interval_yearly) {
            if (ht.interval == Consts.interval_daily) {
                return Utility.formatDateForDaily(hs.date);
            } else {
                return hs.dateInWords;
            }
        }

        return "";
    }

    public static String getFBTitle(String signname, Horotype ht, Horoscope hs) {
        String datestr = getDateForSharing(ht, hs);
        if (ht.name.indexOf("About") >= 0) {
            return "About " + signname;
        }

        return signname + " " + ht.name + (datestr.length() > 0 ? ": " + datestr : "");
    }

    public static String getShareText(String signName, Horotype ht, Horoscope hs) {
        String shareText;
        String shareVideoDefaultSuffix = "#video-horoscope";
        if (ht.horoId == Consts.cosmoGalId) {
            shareText = "Check out today's " + signName + " video horoscope: ";
            String shareUrl = hs.shareUrl.replaceAll(shareVideoDefaultSuffix, "&ice=ast%3Aiscopes-android%3Avideo%3Ashare-email" + shareVideoDefaultSuffix);
            shareText = ": \n\n" + shareText + shareUrl;
        } else {
            shareText = ": \n\n" + hs.horoscopeText;
        }

        shareText = getDateForSharing(ht, hs) + shareText;

        if (ht.name.length() > 0) {
            if (ht.interval != Consts.interval_yearly && ht.interval != Consts.interval_default) {
                shareText = ", " + shareText;
            }

            shareText = Utility.getHorotypeTextWithSign(ht.name, signName) + shareText;
        }

        if (signName.length() > 0 && ht.name.indexOf("About") == -1) {
            shareText = signName + ", " + shareText;
        }

        return shareText;
    }

    public static String getEmailShareText(String signName, Horotype ht, Horoscope hs) {
        String shareText = getShareText(signName, ht, hs);
        String postScript = "\n\n---\nSent from Horoscopes by Astrology.com.\n\nGet the free app...\n Android: "
                + Consts.url_share_android
                + "\n iPhone, iPad, iPod Touch: "
                + Consts.url_share_ios + "\n";
        shareText = shareText + postScript;

        return shareText;
    }

    public static String getTwitterShareText(String signName, Horotype ht, Horoscope hs) {
        String shareText = getShareText(signName, ht, hs).replace("\n", "");
        if (shareText.length() > 100) shareText = shareText.substring(0, 100) + " ... ";
        return shareText;
    }

    public static String removeHTMLTags(String html) {
        return Html.fromHtml(html).toString();
    }

    public static byte[] md5(String s, byte[] array) throws UnsupportedEncodingException {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.reset();
            byte[] tokenArray = s.getBytes("UTF-8");
            byte[] finalByteArray = new byte[array.length + tokenArray.length];

            for (int i = 0; i < array.length; i++)
                finalByteArray[i] = array[i];

            int arrayLengthcount = 0;
            for (int i = array.length; i < finalByteArray.length; i++) {
                finalByteArray[i] = tokenArray[arrayLengthcount];
                arrayLengthcount++;
            }

            byte messageDigest[] = digest.digest(finalByteArray);
            return messageDigest;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String reformat(String s) {
        if (s.contains(".")) s = s.replace(".", "_");
        if (s.contains("-")) s = s.replace("-", "_");
        if (s.contains(" ")) s = s.replace(" ", "_");
        return s;
    }

    // Function for fetching XML from URL
    public static String getXMLFromURL(String url) {
        return sendGetRequest(new HttpGet(url));
    }

    // Sending Request to fetch Data from Server
    private static String sendGetRequest(HttpGet request) {
        // Create a new HttpClient and Post Header
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString;

        try {
            // Execute HTTP Get Request
            response = httpclient.execute(request);

            // Read the response
            InputStream is = response.getEntity().getContent();
            BufferedInputStream bis = new BufferedInputStream(is, 2048);
            ByteArrayBuffer baf = new ByteArrayBuffer(2048);

            int current;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }

            /* Convert the Bytes read to a String. */
            responseString = new String(baf.toByteArray());
        } catch (Exception ex) {
            responseString = "";
            ex.printStackTrace();
        }

        return responseString;
    }

    // Make Atlas web service URL
    public static String makeAtlasWebServiceURL(String url, String date, String city, String country, String state, String time) {
        url = url.replace("DATE", date).trim();
        url = url.replace("CITY", city).trim().replace(" ", "%20");
        url = url.replace("COUNTRY", country).trim().replace(" ", "%20");
        if (!state.equals(null)) {
            url = url.replace("STATE", state).trim().replace(" ", "%20");
        } else {
            url = url.replace("STATE", "").trim().replace(" ", "%20");
        }

        url = url.replace("TIME_STAMP", time).trim();
        return url;
    }

    // Format date & time for purchased product
    public static String getDateTime() {
        return new SimpleDateFormat("MMM dd, yyyy, hh:mm aaa", Locale.US).format(new Date());
    }

    public static String tokenizeImageURLs(ArrayList<String> imagePaths) {
        String imageURLString;
        if (imagePaths.size() == 1) {
            imageURLString = imagePaths.get(0);
        } else {
            StringBuilder sb = new StringBuilder();
            String sep = "";
            for (String s : imagePaths) {
                sb.append(sep).append(s);
                sep = ",";
            }

            imageURLString = sb.toString();
        }

        return imageURLString;
    }

    public static ArrayList<String> getImageURLTokens(String imagesString) {
        StringTokenizer sTokenizer = new StringTokenizer(imagesString, ",");
        ArrayList<String> imageURLList = new ArrayList<String>();
        while (sTokenizer.hasMoreTokens())
            imageURLList.add(sTokenizer.nextToken());

        return imageURLList;
    }

    // Share Product Text via Email
    public static String getShareProductText(Product p) {
        String productShareEmailText = "";
        if (p != null) {
            productShareEmailText = "<p align='center' style='color:#000000; font-size:large; font-family:'Times New Roman', Times, serif'><center><b>"
                    + p.name + "</b></center></p><br/><br /><p align='left' style='color:#000000; font-size:medium; font-family:'Times New Roman', Times, serif' >"
                    + p.description + "</p>";
        }

        return productShareEmailText;
    }

    // Return Image as a base64 string
    public static String getImageUrlAsString(String url) {
        try {
            return Base64.encodeBytes(DownloadFromUrl(url, ""));
        } catch (Exception e) {
            return null;
        }
    }

    // download image from the network
    public static byte[] DownloadFromUrl(String imageURL, String fileName) {
        // this is the down loader method
        ByteArrayBuffer baf = new ByteArrayBuffer(50);
        try {
            URL url = new URL(imageURL); // you can write here any link
            /* Open a connection to that URL. */
            URLConnection ucon = url.openConnection();

            // Define InputStreams to read from the URLConnection.
            InputStream is = ucon.getInputStream();
            if (is != null) {
                BufferedInputStream bis = new BufferedInputStream(is);
                // Read bytes to the Buffer until there is nothing more to read(-1).
                int current = 0;
                while ((current = bis.read()) != -1)
                    baf.append((byte) current);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return baf.toByteArray();
    }

    // Formatting Product Sharing URL on Social Media
    public static String FormatProductSharingURL(String url, Product p) {
        if (p != null) {
            url = url.replace("PRODUCT_NAME", p.name.replaceAll(" ", "-"));
            url = url.replace("PRODUCT_ID", p.ares_Id);
        }
        return url;
    }

    public static boolean checkDate(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        if (c.before(Calendar.getInstance()))
            return true;

        return false;
    }

    public static String get12HoursTime(String mTime) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm", Locale.US);
            return new SimpleDateFormat("h:mm a", Locale.US).format(sdf.parse(mTime));
        } catch (Exception e) {
            return null;
        }
    }

    public static String get24HoursTime(String mTime) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("h:mm a", Locale.US);
            final Date dateObj = sdf.parse(mTime);
            return new SimpleDateFormat("HH:mm", Locale.US).format(dateObj);
        } catch (final Exception e) {
            return null;
        }
    }
}
