package com.astrology.iscopes.common;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.astrology.iscopes.R;
import com.astrology.iscopes.bo.FeedbackEntryItem;
import com.astrology.iscopes.bo.ListItem;

public class ListAdapterFeedback extends ArrayAdapter<ListItem> {

    private Context context;
    private ArrayList<ListItem> items;

    public ListAdapterFeedback(Context context, ArrayList<ListItem> items) {
        super(context, 0, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (items != null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ListItem i = items.get(position);
            if (i.isSection()) {
                v = vi.inflate(R.layout.menulistsectionitem, parent, false);
                LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
                params.height = (int) Utility.DpToPixel(context, 10);
                v.setLayoutParams(params);
            } else {
                v = vi.inflate(R.layout.feedbacklistentryitem, parent, false);
                TextView title = v.findViewById(R.id.feedback_list_item_title);
                TextView summary = v.findViewById(R.id.feedback_list_item_entry_summary);
                ImageView iconImage = v.findViewById(R.id.feedback_list_item_icon);
                FeedbackEntryItem ei = (FeedbackEntryItem) i;

                if (title != null) {
                    title.setText(ei.title);
                }

                if (summary != null) {
                    summary.setText(ei.summary);
                    if (ei.summary.length() == 0) {
                        summary.setVisibility(View.GONE);
                    } else {
                        title.setPadding(title.getPaddingLeft(), 0, title.getPaddingRight(), title.getPaddingBottom());
                    }
                }

                if (iconImage != null && ei.iconName != null) {
                    try {
                        int imageResource = context.getApplicationContext().getResources().getIdentifier(ei.iconName.replaceAll("-", "_"), "drawable", context.getApplicationContext().getPackageName());
                        if (imageResource != 0) {
                            iconImage.setImageResource(imageResource);
                        } else {
                            iconImage.setImageDrawable(null);
                        }
                    } catch (NotFoundException e) {
                        iconImage.setImageDrawable(null);
                    }
                }
            }
        }

        return v;
    }
}