package com.astrology.iscopes.common;

import android.os.Build;

import com.astrology.iscopes.BuildConfig;

public class Consts {

    public static boolean is_editing = true;
    static final boolean is_debug = BuildConfig.DEBUG;

    /************* google tracking vars: ***************/
    public static final String tap = "_Tap";
    public static final String tap_app_info_about = "appInfo_About_Tap";
    public static final String tap_app_info_feedback = "appInfo_Feedback_Tap";
    public static final String tap_app_info_tell_friend = "appInfo_TellFriend_Tap";
    public static final String tap_app_info_go_to_astrology = "appInfo_OpenAstrology_com_Tap";
    public static final String tap_horo_detail_up = "horoDetail_UpHoroType_Tap";
    public static final String tap_horo_detail_down = "horoDetail_DownHoroType_Tap";
    public static final String tap_horo_detail_refresh = "horoDetail_Refresh_Tap";
    public static final String tap_horo_detail_previous = "horoDetail_Previous_Tap";
    public static final String tap_horo_detail_current = "horoDetail_Current_Tap";
    public static final String tap_horo_detail_next = "horoDetail_Next_Tap";
    public static final String tap_horo_detail_play_video = "horoDetail_PlayVideo_Tap";
    public static final String tap_horo_menu_refresh = "horoMenu_Refresh_Tap";
    public static final String tap_horo_menu_edit_favourite = "horoMenu_EditFavourite_Tap";
    public static final String tap_horo_menu_all = "horoMenu_All_Tap";
    public static final String tap_horo_menu_favourite = "horoMenu_Favourite_Tap";
    public static final String tap_horo_menu_get_reading = "horoMenu_GetPersonalizedReading_Tap";
    public static final String tap_horo_menu_view_readings = "horoMenu_ViewMyReading_Tap";
    public static final String tap_horo_sign_refresh = "HoroSign_Refresh_Tap";
    public static final String tap_horo_sign_info = "HoroSign_Info_Tap";
    public static final String tap_reading_report_share = "readingReport_Share_Tap";
    public static final String tap_reading_report_share_by_email = "readingReport_Share_by_Email_Tap";
    public static final String tap_reading_report_share_by_twitter = "readingReport_Share_by_Twitter_Tap";
    public static final String tap_settings_twitter_login = "appSetting_TwitterLogin_Tap";
    public static final String tap_settings_twitter_logout = "appSetting_TwitterLogout_Tap";
    public static final String tap_share_twitter = "horoDetail_Share_by_Twitter_Tap";
    public static final String tap_share_email = "horoDetail_Share_by_Email_Tap";
    public static final String new_daily_video_scope = "New_Daily_Video_Scope";

    public static final String screen_about = "About_This_App_Screen";
    public static final String screen_app_settings = "App_Settings_Screen";
    public static final String screen_app_info = "App_Info_Screen";
    public static final String screen_horo_sign = "HoroSign_Screen";
    public static final String screen_horo_menu = "_Horo_Menu_Screen";
    public static final String screen_reading_report = "Reading_Report_Screen";
    public static final String screen_my_readings = "My_Readings_Screen";
    public static final String screen_horo_detail = "Screen_Horo_Detail";

    public static final String activity_name = "activityName";
    public static final String activity_source = "twitter_caller";
    public static final String activity_my_reading = "reading-activity";
    public static final String activity_horo_detail = "horodetail-activity";
    public static final String activity_product_detail = "productdetail-activity";
    public static final String activity_personal_info = "PersonalInfoActivity";

    public static final String horo_menu_video_scope = "menu_New_Daily_Videoscope_";
    public static final String horo_menu_about = "horoMenu_About_";
    public static final String horo_menu = "menu_";
    public static final String horo_sign = "HoroSign_";
    public static final String hash_table_key = "hashTableKey";
    public static final String hash_table_index = "hashTableIndex";

    public static final String sign_name_holder = "__SIGN_NAME__";
    public static final String email_feedback = "customer_android@astrology.com";

    public static final String uri = "uri";
    public static final String url_key = "iscopes.com/1?oauth_token";
    public static final String url_contains = "iscopes.com";
    public static final String url_more = "https://market.android.com";
    public static final String url_promo_json = "http://mobilesource.astrology.com/android/iscopes/1.0/promo_apps_news.json";
    public static final String url_share_ios = "http://www.astrology.com/getapp/iphone/horoscopes";
    public static final String url_share_android = "http://www.astrology.com/getapp/android/horoscopes";
    public static final String url_menu = "http://mobilesource.astrology.com/wdg/horoscopes/gzplist/DATE/menu.plist";
    public static final String url_horo = "http://mobilesource.astrology.com/wdg/horoscopes/plist/DATE/HOROSCOPE_ID/SIGN_ID.plist";
    public static final String url_product_sharing = "http://www.astrology.com/PRODUCT_NAME/2-p-PRODUCT_ID";

    public static final String prefix_icn_menulist = "typeimg_1_";
    public static final String prefix_img_detailbg = "typeimg_1_";

    public static final String track_name_about = "About_";

    public enum signs {
        none(-1), Aries(0), Taurus(1), Gemini(2), Cancer(3), Leo(4), Virgo(5), Libra(6), Scorpio(7), Sagittarius(8), Capricorn(9), Aquarius(10), Pisces(11), Contact(12);
        public int idx;

        signs(int idx) {
            this.idx = idx;
        }

        public static signs get(int idx) {
            for (signs sign : signs.values()) {
                if (sign.idx == idx) {
                    return sign;
                }
            }

            return none;
        }
    }

    public static final int interval_default = 0;
    public static final int interval_daily = 1;
    public static final int interval_weekly = 7;
    public static final int interval_monthly = 30;
    public static final int interval_yearly = 365;
    public static final int cosmoGalId = 150;
    public static final int menu_all = 1;
    public static final int menu_favorite = 2;

    public static final String title_about = "About";
    public static final String title_video_scope = "New Daily Videoscope";
    public static final String title_personal_reading = "Personalized Readings";
    public static final String title_get_reading = "Get Personalized Readings";
    public static final String title_view_reading = "View My Readings";

    public static final String error_db = "Error occurred while accessing local cache.";
    public static final String error_clear_cache = "Failed to Clear Cache.";
    public static final String error_parser_menu = "Failed to fetch Menu.";
    public static final String error_parser_horo = "Failed to fetch Horoscope.";

    public static final String key_quant_cast = "1bluj0e4vfqh5oza-vhgwrgu210wy1mpx";
    public static final String key_fb_api = "374902392615025";
    public static final String key_fb_secret = "d2380efb0aac85f2632aac32dd4f3047";
    public static final String key_twitter_api = "9tzHaNCuSJcLGNfxk9hQ";
    public static final String key_twitter_secret = "RrNVpU8iYBn5Prv2PJtBDvQqHZwGe0omG2BpFLCsKo";

    // public String DFPKey = "/1153751/Mobile_App_Android_ASTROLOGY_320x50_1";
    // public String DFPKey = "ca-app-pub-3940256099942544/6300978111"; //for testing
    // public static final String facebook_api_key = "211624852194338";
    // public static final String facebook_secret_key = "b3cd34eaff42125765cf36fcc5ac71b9";
    // public static final String CONSUMER_KEY = "iR4EQG4mbewS5k2ZCUrMg";
    // public static final String CONSUMER_SECRET = "kt1KdwP3dJtkpuBKnVXcN8TlMYOEECD5L6FEmYsCQ";

    public static final String os_name = "android";
    public static final String os_version = Build.VERSION.RELEASE;
    public static final String device_brand = android.os.Build.BRAND;
    public static final String device_model = android.os.Build.MODEL;
    public static final String salt = "tripthelightfantastik";
}
