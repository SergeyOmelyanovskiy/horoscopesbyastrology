package com.astrology.iscopes.common;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astrology.iscopes.R;
import com.astrology.iscopes.bo.EntryItem;
import com.astrology.iscopes.bo.ListItem;
import com.astrology.iscopes.bo.SectionItem;

public class ListAdapterMenu extends ArrayAdapter<ListItem> {

    private Context context;
    public boolean isEditing = false;
    public ArrayList<ListItem> items;

    public ListAdapterMenu(Context context, ArrayList<ListItem> items) {
        super(context, 0, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.menulistentryitem, parent, false);
        }

        View v = convertView;
        if (items != null) {
            ListItem i = items.get(position);
            if (i.isSection()) {
                SectionItem si = (SectionItem) i;

                LinearLayout entryItemLayout = convertView.findViewById(R.id.entryItemLayout);
                entryItemLayout.setVisibility(View.GONE);

                // v = vi.inflate(R.layout.menulistsectionitem, null);
                LinearLayout linear = convertView.findViewById(R.id.list_item_section_layout);
                linear.setVisibility(View.VISIBLE);

                linear.setOnClickListener(null);
                linear.setOnLongClickListener(null);
                linear.setLongClickable(false);

                final TextView sectionView = linear.findViewById(R.id.list_item_section_text);
                sectionView.setText(si.title);
                sectionView.setTextColor(Color.LTGRAY);
                // //Log.d("**", si.getTitle());
            } else {
                EntryItem ei = (EntryItem) i;
                LinearLayout linear = convertView.findViewById(R.id.list_item_section_layout);
                linear.setVisibility(View.GONE);

                LinearLayout entryItemLayout = convertView.findViewById(R.id.entryItemLayout);
                entryItemLayout.setVisibility(View.VISIBLE);

                // v = vi.inflate(R.layout.menulistentryitem, null);
                final TextView title = v.findViewById(R.id.list_item_title);
                final ImageView iconImage = v.findViewById(R.id.list_item_icon);
                final ImageView accessoryImage = v.findViewById(R.id.list_item_accessory);

                if (!isEditing) {
                    accessoryImage.setImageResource(R.drawable.arrow1);
                    accessoryImage.setAdjustViewBounds(true);
                    accessoryImage.setMaxHeight((int) Utility.DpToPixel(getContext(), 15));
                    accessoryImage.setMaxWidth((int) Utility.DpToPixel(getContext(), 25));
                } else {
                    if (MgrMenu.getInstance(context).isFavorite(ei.horoId)) {
                        accessoryImage.setImageResource(R.drawable.icon_favorite_on);
                        accessoryImage.setAdjustViewBounds(true);
                        accessoryImage.setMaxHeight((int) Utility.DpToPixel(getContext(), 25));
                        accessoryImage.setMaxWidth((int) Utility.DpToPixel(getContext(), 25));
                    } else {
                        accessoryImage.setImageDrawable(null);
                    }

                    if (ei.horoId == 400 || ei.horoId == 401) {
                        v.setBackgroundColor(context.getResources().getColor(R.color.gray));
                    }
                }

                if (title != null) {
                    title.setText(ei.title);
                }

                if (iconImage != null && ei.getIconName() != null) {
                    try {
                        int imageResource = context.getApplicationContext().getResources().getIdentifier(ei.getIconName(), "drawable", context.getApplicationContext().getPackageName());
                        if (imageResource != 0) {
                            iconImage.setImageResource(imageResource);
                        } else {
                            iconImage.setImageDrawable(null);
                        }
                    } catch (NotFoundException e) {
                        iconImage.setImageDrawable(null);
                    }
                }
            }
        }

        return v;
    }
}