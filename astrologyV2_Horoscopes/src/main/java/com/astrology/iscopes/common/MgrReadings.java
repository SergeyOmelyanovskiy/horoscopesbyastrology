package com.astrology.iscopes.common;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Hashtable;

import android.content.Context;
import android.os.AsyncTask;

import com.astrology.iscopes.bo.Product;
import com.astrology.iscopes.bo.Purchase;
import com.astrology.iscopes.bo.Reading;
import com.astrology.iscopes.database.IScopesDB;
import com.astrology.iscopes.orm.Birthdetails;
import com.astrology.iscopes.orm.LocationInfo;
import com.astrology.iscopes.parser.ParsePurchaseReading;

public class MgrReadings {

    private static MgrReadings anInstance;

    private LocationInfo loc1;
    private LocationInfo loc2;

    private Birthdetails info1;
    private Birthdetails info2;

    private ParsePurchaseReading parsePurchasedReading;
    private Reading purchasedReading;

    int productID;
    String readingType;
    private Context context;
    Hashtable<String, ArrayList<Purchase>> purchases = new Hashtable<String, ArrayList<Purchase>>();

    Purchase purchase = new Purchase();

    public MgrReadings(Context context) {
        loc1 = new LocationInfo();
        loc2 = new LocationInfo();
        info1 = new Birthdetails();
        info2 = new Birthdetails();
        parsePurchasedReading = new ParsePurchaseReading();
        this.context = context.getApplicationContext();
    }

    public static synchronized MgrReadings getSharedObject(Context context) {
        if (anInstance == null) anInstance = new MgrReadings(context);
        return anInstance;
    }

    public String makeOnePersonURL(String url, String productID, String type, int position) {

        readingType = type;
        info1 = IScopesDB.getInstance(context).getBirthDetails();
        loc1 = IScopesDB.getInstance(context).getLocationDetails();

        url = url.replace("PRODUCT_ID", productID).trim();
        url = url.replace("BIRTH_NAME_1", info1.birthName).replaceAll(" ", "%20").trim();
        url = url.replace("BIRTH_CITY_1", loc1.city).trim();
        url = url.replace("BIRTH_COUNTY_1", loc1.county).trim();
        url = url.replace("BIRTH_COUNTRY_1", info1.birthCountry).replaceAll(" ", "%20").trim();
        url = url.replace("BIRTH_DST_1", loc1.dst).trim();
        url = url.replace("BIRTH_LATITUDE_1", loc1.latitude).trim();
        url = url.replace("BIRTH_LONGITUDE_1", loc1.longitude).trim();

        String formatedDate = Utility.formatDateForPurchaseReadingURL(info1.birthDate).trim();
        url = url.replace("BIRTH_TIME_1", formatedDate);
        if (!info1.birthTime.isEmpty()) {
            String[] time = getSplittedTime(info1.birthTime);
            url = url.replace("BIRTH_STATE_1", loc1.state).replaceAll(" ", "%20").trim();
            url = url.replace("BIRTH_TZHOUR_1", time[0].trim());
            url = url.replace("BIRTH_TZMINUTE_1", time[1].trim());
        } else {
            String[] time = getSplittedTime("0:00");
            url = url.replace("BIRTH_TIME_1", formatedDate);
            url = url.replace("BIRTH_STATE_1", loc1.state).replaceAll(" ", "%20").trim();
            url = url.replace("BIRTH_TZHOUR_1", time[0].trim());
            url = url.replace("BIRTH_TZMINUTE_1", time[1].trim());
        }
        url = url.replace("BIRTH_TZDIR_1", loc1.tzDir).replace(" ", "");
        url = url.replace("GENDER_1", info1.gender).trim();

        // Log.d("One Person Reading URL without SH1 is: ", url);
        String relativePath = "/" + productID + ".xml";
        String text = url.substring(url.indexOf('?') + 1) + relativePath + Consts.salt;

        try {
            url += "&h=" + generateAccessToken(text);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return url;
    }

    private String[] getSplittedTime(String time) {
        return time.split(":");
    }

    public String makeTwoPersonURL(String url, String productID, String type,
                                   int position, int locationPosition, String birthName2,
                                   String birthDate2, String birthTime2, String birthCity2,
                                   String birthState2, String birthCountry2, String gender2) {

        readingType = type;
        info2.birthName = birthName2;
        info2.birthDate = birthDate2;
        info2.gender = gender2;
        if (birthTime2 != null) info2.birthTime = birthTime2;

        info1 = IScopesDB.getInstance(context).getBirthDetails();
        loc1 = IScopesDB.getInstance(context).getLocationDetails();

        ArrayList<LocationInfo> loc21 = MgrShared.getInstance(context).locations;
        loc2 = MgrShared.getInstance(context).getLocation(loc21.size() != 1 ? locationPosition : 0);

        url = url.replace("PRODUCT_ID", productID).trim();
        url = url.replace("BIRTH_LATITUDE_2", loc2.latitude).trim();

        String formatedDate = Utility.formatDateForPurchaseReadingURL(info1.birthDate).trim();
        if (!info1.birthTime.isEmpty()) {
            String[] time = getSplittedTime(info1.birthTime);
            url = url.replace("BIRTH_TZMINUTE_1", time[1]).trim();
            url = url.replace("BIRTH_TZHOUR_1", time[0]).trim();
        } else {
            String[] time = getSplittedTime("0:00");
            url = url.replace("BIRTH_TZMINUTE_1", time[1]).trim();
            url = url.replace("BIRTH_TZHOUR_1", time[0]).trim();
        }

        url = url.replace("BIRTH_TIME_1", formatedDate);
        url = url.replace("BIRTH_TIME_1", info1.birthDate).trim();
        url = url.replace("GENDER_2", gender2).trim();
        url = url.replace("BIRTH_COUNTRY_1", info1.birthCountry).replaceAll(" ", "%20").trim();
        url = url.replace("BIRTH_LONGITUDE_1", loc1.longitude).trim();
        url = url.replace("BIRTH_COUNTRY_2", birthCountry2).replaceAll(" ", "%20").trim();
        url = url.replace("BIRTH_DST_1", loc1.dst).trim();
        url = url.replace("BIRTH_LONGITUDE_2", loc2.longitude).trim();

        formatedDate = Utility.formatDateForPurchaseReadingURL(birthDate2).trim();
        if (!birthTime2.isEmpty()) {
            String[] time2 = getSplittedTime(birthTime2);
            url = url.replace("BIRTH_TZHOUR_2", time2[0]).trim();
            url = url.replace("BIRTH_TZMINUTE_2", time2[1]).trim();
        } else {
            url = url.replace("BIRTH_TZHOUR_2", "0").trim();
            url = url.replace("BIRTH_TZMINUTE_2", "00").trim();
        }

        url = url.replace("BIRTH_TIME_2", formatedDate).trim();
        url = url.replace("BIRTH_DST_2", loc2.dst.trim());
        url = url.replace("BIRTH_STATE_1", loc1.state).replaceAll(" ", "%20").trim();
        url = url.replace("BIRTH_TZDIR_1", loc1.tzDir).trim().replace(" ", "");
        url = url.replace("BIRTH_STATE_2", loc2.state).replaceAll(" ", "%20").trim();
        url = url.replace("BIRTH_TZDIR_2", loc2.tzDir).trim().replace(" ", "");

        url = url.replace("BIRTH_CITY_1", loc1.city).trim();
        url = url.replace("BIRTH_CITY_2", loc2.city).trim();
        url = url.replace("BIRTH_COUNTY_1", loc1.county).trim();
        url = url.replace("BIRTH_COUNTY_2", loc2.county).trim();
        url = url.replace("BIRTH_NAME_1", info1.birthName).replaceAll(" ", "%20").trim();
        url = url.replace("BIRTH_NAME_2", birthName2).replaceAll(" ", "%20").trim();
        url = url.replace("BIRTH_LATITUDE_1", loc1.latitude).trim();
        url = url.replace("GENDER_1", info1.gender);

        // Log.d("Two Person Reading URL without hexDigest1 is: ", url);
        String relativePath = "/" + productID + ".xml";
        String text = url.substring(url.indexOf('?') + 1) + relativePath + Consts.salt;

        try {
            url += "&h=" + generateAccessToken(text);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return url;
    }

    public String generateAccessToken(String stringToHash) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        stringToHash = stringToHash.replaceAll("%20", " ");
        // Log.d("String to Hash : ", "" + stringToHash);

        byte[] result = MessageDigest.getInstance("SHA-1").digest(stringToHash.getBytes("UTF-8"));
        // Another way to make HEX, my previous post was only the method like your solution
        StringBuilder sb = new StringBuilder();
        for (byte b : result)  // This is your byte[] result.
            sb.append(String.format("%02x", b));

        return sb.toString();
    }

    public int purchaseReading(String url, String type, int position) {
        purchasedReading = parsePurchasedReading.parseReadings(url, type);
        // Saving All the data related to product purchasing in database.
        if (purchasedReading != null) {
            readingType = type;
            // Save All the related data in database
            return saveDataInBD(position, purchasedReading);
        }

        return -1;
    }

    public class AsynchTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            purchasedReading = parsePurchasedReading.parseReadings(params[0], params[1]);
            return null;
        }
    }

    private int saveDataInBD(int position, Reading purchasedReading) {
        // Save Product Details in database
        int productid = (int) IScopesDB.getInstance(context).saveProductDetails(MgrShared.getInstance(context).getProduct(position));

        // Save Purchased Reading
        int readingid = (int) IScopesDB.getInstance(context).savePurchasedReading(purchasedReading, Utility.getDateTime(), productid);

        // Save BirthInfo related to the purchased Reading
        if (readingType.equals("1") || readingType.equals("2")) {
            IScopesDB.getInstance(context).saveBirthInfoForPurchasedReading(info1, loc1, readingid);
            if (readingType.equals("2")) {
                IScopesDB.getInstance(context).saveBirthInfoForPurchasedReading(info2, loc2, readingid);
            }
        }

        // Save Sections data in database
        IScopesDB.getInstance(context).saveReadingSections(purchasedReading, readingid);
        return readingid;
    }

    // This function returns the purchased readings for MyReadings screen.
    public Hashtable<String, ArrayList<Purchase>> getPurchasedProducts() {
        IScopesDB idb = IScopesDB.getInstance(context);
        ArrayList<Product> productTypes = idb.getProductTypes();
        productTypes.trimToSize();
        for (Product p : productTypes) {
            purchases.put(p.name, idb.getPurchasedReadignsForProdutType(Integer.parseInt(p.ares_Id)));
        }

        return purchases;
    }

    public void deletePurchasedReading(int readingId, String readingType) {
        IScopesDB.getInstance(context).deletePurchasedReading(readingId, readingType);
    }

    public String getImageURLForProduct(String productType) {
        return IScopesDB.getInstance(context).getImageURLForProduct(productType);
    }

    public Reading getPurchasedReadings(int readingId) {
        return IScopesDB.getInstance(context).getPurchasedReadings(readingId);
    }
}