package com.astrology.iscopes.common;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.astrology.iscopes.R;
import com.astrology.iscopes.bo.ListItem;
import com.astrology.iscopes.bo.MyReadingsEntryItem;
import com.astrology.iscopes.bo.SectionItem;

public class ListAdapterReadings extends ArrayAdapter<ListItem> {

    private Context context;
    private ArrayList<ListItem> items;
    private LayoutInflater vi;

    public ListAdapterReadings(Context context, ArrayList<ListItem> items) {
        super(context, 0, items);
        this.context = context;
        this.items = items;
        vi = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ListItem i = items.get(position);
        if (i != null) {
            if (i.isSection()) {
                SectionItem si = (SectionItem) i;
                v = vi.inflate(R.layout.myreadingslistsectionitem, parent, false);
                v.setOnClickListener(null);
                v.setOnLongClickListener(null);
                v.setLongClickable(false);
                ((TextView) v.findViewById(R.id.myreadings_list_item_section_text)).setText(si.title);
            } else {
                MyReadingsEntryItem ei = (MyReadingsEntryItem) i;
                v = vi.inflate(R.layout.myreadingslistentryitem, parent, false);
                TextView birthName = v.findViewById(R.id.myreadings_list_item_birthname);
                TextView purchaseDate = v.findViewById(R.id.myreadings_list_item_entry_date);

                if (birthName != null) {
                    birthName.setText(ei.birthName);
                }

                if (purchaseDate != null) {
                    purchaseDate.setText(ei.purchaseTime);
                }
            }
        }

        return v;
    }
}
