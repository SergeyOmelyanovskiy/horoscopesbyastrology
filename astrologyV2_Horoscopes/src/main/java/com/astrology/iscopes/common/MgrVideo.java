package com.astrology.iscopes.common;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.astrology.iscopes.orm.Horoscope;

public class MgrVideo {

    public static MgrVideo anInstance;
    private Context context;

    public static synchronized MgrVideo getInstance(Context context) {
        if (anInstance == null) anInstance = new MgrVideo(context);
        return anInstance;
    }

    // constructor
    private MgrVideo(Context ctx) {
        context = ctx;
    }

    public String playVideoButtonClicked(final Boolean isHttpURL, Horoscope hs, String app_name, String app_version) throws Exception {
        return generateVideoURL(isHttpURL, hs, app_name, app_version);
    }

    private String generateVideoURL(Boolean isHttpURL, Horoscope hs, String app_name, String app_version) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        Calendar timeLessCalendar = Calendar.getInstance();
        timeLessCalendar.set(Calendar.HOUR_OF_DAY, 0);
        timeLessCalendar.set(Calendar.MINUTE, 0);
        timeLessCalendar.set(Calendar.SECOND, 0);
        timeLessCalendar.set(Calendar.MILLISECOND, 0);
        // now grab the seconds
        long validStartTime = timeLessCalendar.getTime().getTime() / 1000;
        String baseURL;

        ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED || connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            baseURL = hs.lowBandwidthUrl;
        } else if (MgrShared.getInstance(context).isTablet()) {
            baseURL = hs.highBandwidthUrl;
        } else {
            baseURL = hs.normalBandwidthUrl;
        }

        return makeHashURL(baseURL, validStartTime, isHttpURL, hs, app_name, app_version);
    }

    private String makeHashURL(String s, long expiryTime, Boolean isHttpURL, Horoscope hs, String app_name, String app_version) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String url = s;
        String salt = "astr01villaGe";
        s = s.substring(28); // trimming the URL
        expiryTime = expiryTime + 60 * 60 * 48; // Adding 2 days window to the
        if (url == hs.lowBandwidthUrl) {
            s = s + "&__source=" + Consts.os_name + "_"
                    + Utility.reformat(Consts.os_version) + "_"
                    + Utility.reformat(Consts.device_brand.trim()) + "_"
                    + Utility.reformat(Consts.device_model).trim() + "_"
                    + app_name + "&__source2=" + app_name + "_"
                    + Utility.reformat(app_version).trim();
        } else if (url == hs.highBandwidthUrl) {
            s = s + "&__source=" + Consts.os_name + "_"
                    + Utility.reformat(Consts.os_version) + "_"
                    + Utility.reformat(Consts.device_brand) + "_"
                    + Utility.reformat(Consts.device_model) + "_app"
                    + "&__source2=" + app_name + "_"
                    + Utility.reformat(app_version);
        }

        if (isHttpURL.equals(true)) {
            s = s + "&fd=http";
        }

        s = generate_Auth_Token(s, salt, expiryTime, url, isHttpURL, hs, app_name, app_version);
        return s;
    }

    private String generate_Auth_Token(String path, String salt, long expiryTime, String url, Boolean isHttpURL, Horoscope hs, String app_name, String app_version) throws UnsupportedEncodingException, NoSuchAlgorithmException {

        String hash = null;

        String time = Long.toString(expiryTime);
        byte expiryTime1 = (byte) (expiryTime & 0xff);
        byte expiryTime2 = (byte) ((expiryTime >> 8) & 0xff);
        byte expiryTime3 = (byte) ((expiryTime >> 16) & 0xff);
        byte expiryTime4 = (byte) ((expiryTime >> 24) & 0xff);

        byte[] a = {expiryTime1, expiryTime2, expiryTime3, expiryTime4};
        String s = path + salt;
        byte[] tokenArray = Utility.md5(s, a);// md5.getBytes("UTF-8");
        byte[] array = salt.getBytes("UTF-8");
        byte[] finalByteArray = new byte[array.length + tokenArray.length];
        for (int i = 0; i < array.length; i++)
            finalByteArray[i] = array[i];

        int arrayLengthcount = 0;
        for (int i = array.length; i < finalByteArray.length; i++) {
            finalByteArray[i] = tokenArray[arrayLengthcount];
            arrayLengthcount++;
        }

        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
        md.update(finalByteArray);
        byte[] finalByteArray2 = md.digest();
        for (int i = 0; i < finalByteArray2.length; i++) {
            hash = new BigInteger(1, finalByteArray2).toString(16);
            if (hash.length() == 31) {
                hash = "0" + hash;
            }
        }
        return makeFinalURL(url, time, hash, isHttpURL, hs, app_name, app_version);
    }

    private String makeFinalURL(String url, String time, String token, Boolean isHttpURL, Horoscope hs, String app_name, String app_version) {
        if (url == hs.normalBandwidthUrl) {
            url = url + "?token=" + time + "_" + token;
        } else {
            url = url + "&__source=" + Consts.os_name + "_"
                    + Utility.reformat(Consts.os_version) + "_"
                    + Utility.reformat(Consts.device_brand) + "_"
                    + Utility.reformat(Consts.device_model) + "_" + app_name
                    + "&__source2=" + app_name + "_"
                    + Utility.reformat(app_version.trim()) + "&token=" + time
                    + "_" + token;
            if (isHttpURL.equals(true)) {
                url = url + "&fd=http";
            }
        }

        return url;
    }
}// end of class