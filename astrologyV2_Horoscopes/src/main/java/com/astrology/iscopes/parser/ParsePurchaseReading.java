package com.astrology.iscopes.parser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.astrology.iscopes.bo.Cover;
import com.astrology.iscopes.bo.Reading;
import com.astrology.iscopes.bo.Section;
import com.astrology.iscopes.common.Utility;

public class ParsePurchaseReading {

    public ParsePurchaseReading() {

    }

    public Reading parseReadings(String purchaseReadingURL, String type) {
        // Fetch XML response from URL
        String title = "";
        Reading purchasedReading = new Reading();
        String readingsXML = Utility.getXMLFromURL(purchaseReadingURL);
        ArrayList<ArrayList<Section>> interpretation = new ArrayList<ArrayList<Section>>();
        if (!readingsXML.equals(null)) {
            // Parsing Starts here
            try {
                InputStream is = new ByteArrayInputStream(readingsXML.getBytes("UTF-8"));

                // Build XML document
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(is);
                doc.normalize();
                NodeList nodeList, innerSectionList, outerSectionList;
                Node coverNode, interpretationNode, outerSectionItem, childNode;

                // Get Cover Node
                nodeList = doc.getElementsByTagName("cover");

                for (int i = 0; i < nodeList.getLength(); i++) {
                    coverNode = nodeList.item(i);
                    purchasedReading.cover = parseForOneCover((Element) coverNode);
                }

                // Parsing for Sections
                interpretationNode = doc.getElementsByTagName("interpretation").item(0);
                outerSectionList = interpretationNode.getChildNodes();
                // out section loop
                for (int i = 0; i < outerSectionList.getLength(); i++) {
                    // sectionItem is one node of interpretation array
                    outerSectionItem = outerSectionList.item(i);
                    if (!outerSectionItem.getNodeName().equalsIgnoreCase("#text")) {
                        ArrayList<Section> sectionList = new ArrayList<Section>();
                        innerSectionList = outerSectionItem.getChildNodes();

                        for (int j = 0; j < innerSectionList.getLength(); j++) {
                            childNode = innerSectionList.item(j);

                            if (!childNode.getNodeName().equalsIgnoreCase("#text")) {
                                if (childNode.getNodeName().equalsIgnoreCase("text")) {
                                    title = childNode.getFirstChild().getNodeValue();
                                } else if (childNode.getNodeName().equalsIgnoreCase("section")) {
                                    Section section = new Section();
                                    section.title = title;

                                    if (type.equals("1")) {
                                        String description = childNode.getChildNodes().item(5).getFirstChild().getNodeValue();
                                        section.description = description;

                                    } else if (type.equals("2")) {
                                        ArrayList<String> imagePaths = new ArrayList<String>();
                                        for (int k = 0; k < childNode.getChildNodes().getLength(); k++) {
                                            if (!childNode.getChildNodes().item(k).getNodeName().equalsIgnoreCase("#text")) {
                                                if (childNode.getChildNodes().item(k).getNodeName().equalsIgnoreCase("image")) {
                                                    imagePaths.add((childNode.getChildNodes().item(k).getFirstChild().getNodeValue()));
                                                }

                                                if (childNode.getChildNodes().item(k).getNodeName().equalsIgnoreCase("text")) {
                                                    Node node = childNode.getChildNodes().item(k);
                                                    if (node.hasAttributes()) {
                                                        if (node.getFirstChild() != null) {
                                                            section.relation = node.getFirstChild().getNodeValue();
                                                        }
                                                    } else {
                                                        if (node.getFirstChild() != null) {
                                                            section.description = node.getFirstChild().getNodeValue();
                                                        }
                                                    }
                                                }// end of if
                                            }// end of "#text" if
                                        }// end of for(k)
                                        imagePaths.trimToSize();
                                        section.imagePaths = imagePaths;
                                    }// end of else
                                    sectionList.add(section);
                                }// end of else after "text"
                                sectionList.trimToSize();
                            }// end of inner if
                        }// end of j for
                        sectionList.trimToSize();
                        interpretation.add(sectionList);
                    }
                }// end of outermost for
                purchasedReading.interpretation = interpretation;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return purchasedReading;
    }

    private Cover parseForOneCover(Element element) {
        Cover c = new Cover();
        Node childNode;
        String title;
        String subTitle;
        String byLine;

        if (element.getElementsByTagName("title").getLength() > 0) {
            childNode = element.getElementsByTagName("title").item(0);
            title = childNode.getFirstChild().getNodeValue();
            if (title != null) {
                c.title = title;
            }
        }

        if (element.getElementsByTagName("subtitle").getLength() > 0) {
            childNode = element.getElementsByTagName("subtitle").item(0);
            subTitle = childNode.getFirstChild().getNodeValue();
            if (subTitle != null) {
                c.subTitle = subTitle;
            }
        }

        if (element.getElementsByTagName("byline").getLength() > 0) {
            childNode = element.getElementsByTagName("byline").item(0);
            byLine = childNode.getFirstChild().getNodeValue();
            if (byLine != null) {
                c.byLine = byLine.trim();
            }
        }

        return c;
    }
}