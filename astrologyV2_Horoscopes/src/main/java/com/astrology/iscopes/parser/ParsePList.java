package com.astrology.iscopes.parser;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.astrology.iscopes.bo.Promo;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.Utility;
import com.astrology.iscopes.orm.Horogroup;
import com.astrology.iscopes.orm.Horomenu;
import com.astrology.iscopes.orm.Horoscope;
import com.astrology.iscopes.orm.Horotype;

@SuppressWarnings("unused")
public class ParsePList {

    public ArrayList<Promo> promoListCheck;

    public ParsePList(ArrayList<Promo> promo) {
        promoListCheck = promo;
    }

    // create xml document object from XML String
    private static Document convertXMLFromString(String xml) {
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource inputSource = new InputSource();
            inputSource.setCharacterStream(new StringReader(xml));
            doc = db.parse(inputSource);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return doc;
    }

    private static ArrayList<Promo> populatePromoObjectsFromJSON(JSONArray promoJSONArray) throws JSONException {
        ArrayList<Promo> promoList = new ArrayList<>();
        JSONObject jsonObject;

        for (int i = 0; i < promoJSONArray.length(); i++) {
            jsonObject = promoJSONArray.getJSONObject(i);
            Promo promoObject = new Promo();

            //if(jsonObject.has("webview")) promoObject.setWebview(jsonObject.getString("webview"));
            if (jsonObject.has("image")) promoObject.image = jsonObject.getString("image");
            if (jsonObject.has("text")) promoObject.text = jsonObject.getString("text");
            if (jsonObject.has("url")) promoObject.url = jsonObject.getString("url");
            if (jsonObject.has("phone")) {
                String valueString = jsonObject.getString("phone");
                StringTokenizer token = new StringTokenizer(valueString, "-");
                String tk = "";
                while (token.hasMoreElements())
                    tk = tk + token.nextToken().trim();
                promoObject.phoneNumber = tk;
            }

            if (jsonObject.has("horo_types")) {
                String valueString = jsonObject.getString("horo_types");
                valueString = valueString.substring(1, valueString.length() - 1);
                StringTokenizer token = new StringTokenizer(valueString, ",");
                ArrayList<Integer> horoTypes = new ArrayList<Integer>();
                while (token.hasMoreElements()) {
                    String tk = token.nextToken().trim();
                    horoTypes.add(Integer.valueOf(tk));
                }

                promoObject.horoTypes = horoTypes;
            }

            if (jsonObject.has("priority_on_dates")) {
                String valueString = jsonObject.getString("priority_on_dates");
                StringTokenizer token = new StringTokenizer(valueString, ",");
                ArrayList<Date> priorityOnDates = new ArrayList<Date>();
                while (token.hasMoreElements()) {
                    String tk = token.nextToken().trim();
                    priorityOnDates.add(Utility.dateFormString(tk));
                }

                promoObject.priorityOnDates = priorityOnDates;
            }

            if (jsonObject.has("valid_dates")) {
                String valueString = jsonObject.getString("valid_dates");
                StringTokenizer token = new StringTokenizer(valueString, ",");
                ArrayList<Date> validDates = new ArrayList<Date>();
                while (token.hasMoreElements()) {
                    String tk = token.nextToken().trim();
                    validDates.add(Utility.dateFormString(tk));
                }

                promoObject.validDates = validDates;
            }

            promoList.add(promoObject);
        }// end of for

        return promoList;
    }

    // sets the position of add display(top/bottom)
    private static String setAddsPosition(NodeList nodeList, int index) {
        String position = "";
        Boolean flag = true;
        for (int i = index; i < nodeList.getLength() && flag.equals(true); i++) {
            while (nodeList.item(i).getNodeName().equalsIgnoreCase("#text")) {
                i++;
            }

            if (nodeList.item(i).getNodeName().equalsIgnoreCase("dict")) {
                flag = false;
                NodeList dictNodeList = nodeList.item(i).getChildNodes();
                for (int j = 0; j < dictNodeList.getLength(); j++) {
                    // while(dictNodeList.item(++j).getFirstChild().getNodeName().equalsIgnoreCase("#text"))
                    // {//j++;
                    // }
                    if (dictNodeList.item(j).getNodeName().equalsIgnoreCase("key")) {
                        // Log.i("node name: "+j,""+dictNodeList.item(j).getNodeName());
                        while (dictNodeList.item(++j).getNodeName().equalsIgnoreCase("#text")) {

                        }

                        if (dictNodeList.item(j).getNodeName().equalsIgnoreCase("string")) {
                            position = dictNodeList.item(j).getFirstChild().getNodeValue();
                        }
                    }
                }
            }
        }

        return position;
    }

    private static ArrayList<Promo> populatePromoObjectsFromPList(Node parentNode) {
        ArrayList<Promo> promoList = new ArrayList<>();
        NodeList dictNodeList = parentNode.getChildNodes();
        for (int i = 0; i < dictNodeList.getLength(); i++) {
            if (dictNodeList.item(i).getNodeName().equalsIgnoreCase("dict")) {
                //Log.i("dict: "+i,""+dictNodeList.item(i).getNodeName());
                Promo promoObj = new Promo();
                NodeList childNodeList = dictNodeList.item(i).getChildNodes();
                for (int j = 0; j < childNodeList.getLength(); j++) {
                    Node keyNode = childNodeList.item(j);
                    if (keyNode.getNodeName().equalsIgnoreCase("key")) {
                        while (childNodeList.item(++j).getNodeName().equalsIgnoreCase("#text")) {

                        }

                        Node valueNode = childNodeList.item(j);
                        String keyString = keyNode.getChildNodes().item(0).getNodeValue();
                        String valueString = "";
                        if (valueNode.getChildNodes().item(0) != null) {
                            valueString = valueNode.getChildNodes().item(0).getNodeValue();
                        }

                        // Log.i("key: "+keyString," Value: "+valueString);
                        if (keyString.equalsIgnoreCase("text")) promoObj.text = valueString;
                        else if (keyString.equalsIgnoreCase("horo_types")) {
                            valueString = valueString.substring(1, valueString.length() - 1);
                            StringTokenizer token = new StringTokenizer(valueString, ",");
                            ArrayList<Integer> horoTypes = new ArrayList<Integer>();

                            while (token.hasMoreElements()) {
                                String tk = token.nextToken().trim();
                                horoTypes.add(Integer.valueOf(tk));
                                // //Log.i("token: ",tk);
                            }
                            promoObj.horoTypes = horoTypes;
                        } else if (keyString.equalsIgnoreCase("priority_on_dates")) {
                            // //Log.i("priority date",""+value);
                            StringTokenizer token = new StringTokenizer(valueString, ",");
                            ArrayList<Date> priorityOnDates = new ArrayList<Date>();
                            while (token.hasMoreElements()) {
                                String tk = token.nextToken().trim();
                                priorityOnDates.add(Utility.dateFormString(tk));
                                // Log.i("priority date: ",date.toString());
                            }
                            promoObj.priorityOnDates = priorityOnDates;
                            // promoObj.setHoroTypes(value);
                        } else if (keyString.equalsIgnoreCase("valid_dates")) {
                            // //Log.i("valid date:  ",""+value);
                            StringTokenizer token = new StringTokenizer(valueString, ",");
                            ArrayList<Date> validDates = new ArrayList<Date>();

                            while (token.hasMoreElements()) {
                                String tk = token.nextToken().trim();
                                validDates.add(Utility.dateFormString(tk));
                            }
                            promoObj.validDates = validDates;
                        } else if (keyString.equalsIgnoreCase("url")) {
                            promoObj.url = valueString;
                        } else if (keyString.equalsIgnoreCase("phone")) {
                            StringTokenizer token = new StringTokenizer(valueString, "-");
                            String tk = "";
                            while (token.hasMoreElements()) {
                                tk.concat(token.nextToken().trim());
                            }
                            promoObj.phoneNumber = tk;
                        } else if (keyString.equalsIgnoreCase("image")) {
                            promoObj.image = valueString;
                        }
                    }

                }// inner for
                promoList.add(promoObj);
            }
        }

        return promoList;
    }

    public static Horomenu parseMenuFromXMLString(InputStream xml) throws Exception {
        Horomenu menu = new Horomenu();
        try {
            // Building XML Document
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            // Log.e("XML Received : " + xml.toString().length(), "" + convertStreamToString(xml));
            Document doc = db.parse(xml);
            doc.getDocumentElement().normalize();

            // Getting Menu items
            NodeList plist = doc.getDocumentElement().getChildNodes();
            for (int i = 0; i < plist.getLength(); i++) {
                Node pArray = plist.item(i);
                NodeList objectList = pArray.getChildNodes();

                if (!pArray.getNodeName().equalsIgnoreCase("#text")) {
                    if (pArray.getNodeName().equalsIgnoreCase("dict")) {
                        for (int j = 0; j < objectList.getLength(); j++) {
                            Node subItemNode = objectList.item(j);
                            if (subItemNode.getNodeName().equalsIgnoreCase("key")) {
                                // Ignoring #text nodes
                                while (objectList.item(++j).getNodeName().equalsIgnoreCase("#text")) {

                                }
                                Node subItemNode2 = objectList.item(j);
                                String nodeKey = subItemNode.getChildNodes().item(0).getNodeValue();
                                String nodeValue = "";
                                if (subItemNode2.getChildNodes().item(0) != null) {
                                    nodeValue = subItemNode2.getChildNodes().item(0).getNodeValue();
                                }

                                if (nodeKey.equalsIgnoreCase("date")) {
                                    menu.date = (nodeValue);
                                } else if (nodeKey.equalsIgnoreCase("relevanceDate")) {
                                    menu.menuDate = (nodeValue);
                                } else if (nodeKey.equalsIgnoreCase("relevanceSign")) {
                                    menu.horosignId = (Integer.parseInt(nodeValue));
                                } else if (nodeKey.equalsIgnoreCase("relevanceType")) {
                                    menu.horotypeId = (Integer.parseInt(nodeValue));
                                } else if (nodeKey.equalsIgnoreCase("horogroups")) {
                                    menu.horogroup = (parseHorogroupsFromNode(subItemNode2));
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new Exception(Consts.error_parser_menu);
        }

        return menu;
    }

    public static ArrayList<Horogroup> parseHorogroupsFromNode(Node node) {
        ArrayList<Horogroup> horogroups = new ArrayList<>();
        NodeList objectList = node.getChildNodes();

        for (int i = 0; i < objectList.getLength(); i++) {
            Node pArray = objectList.item(i);
            NodeList hgNodeList = pArray.getChildNodes();

            if (!pArray.getNodeName().equalsIgnoreCase("#text")) {
                if (pArray.getNodeName().equalsIgnoreCase("dict")) {
                    Horogroup horogroup = new Horogroup();
                    for (int j = 0; j < hgNodeList.getLength(); j++) {
                        Node subItemNode = hgNodeList.item(j);
                        if (subItemNode.getNodeName().equalsIgnoreCase("key")) {
                            // Ignoring #text nodes
                            while (hgNodeList.item(++j).getNodeName().equalsIgnoreCase("#text")) {

                            }

                            Node subItemNode2 = hgNodeList.item(j);
                            String nodeKey = subItemNode.getChildNodes().item(0).getNodeValue();
                            String nodeValue = "";
                            if (subItemNode2.getChildNodes().item(0) != null) {
                                nodeValue = subItemNode2.getChildNodes().item(0).getNodeValue();
                            }

                            if (nodeKey.equalsIgnoreCase("contentTypes")) {
                                horogroup.contentType = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("id")) {
                                horogroup.horoId = (Integer.parseInt(nodeValue));
                            } else if (nodeKey.equalsIgnoreCase("name")) {
                                horogroup.name = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("sortOrder")) {
                                horogroup.sortOrder = (Integer.parseInt(nodeValue));
                            } else if (nodeKey.equalsIgnoreCase("tags")) {
                                horogroup.tags = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("horotypes")) {
                                horogroup.horotype = (parseHorotypesFromNode(subItemNode2));
                            }
                        }
                    }

                    horogroups.add(horogroup);
                }
            }
        }
        // //Log.d ("HOROGROUP COUNT", "" + horogroups.size());
        return horogroups;
    }

    public static ArrayList<Horotype> parseHorotypesFromNode(Node node) {
        ArrayList<Horotype> horotypes = new ArrayList<>();
        NodeList objectList = node.getChildNodes();
        for (int i = 0; i < objectList.getLength(); i++) {
            Node pArray = objectList.item(i);
            NodeList htNodeList = pArray.getChildNodes();

            if (!pArray.getNodeName().equalsIgnoreCase("#text")) {
                if (pArray.getNodeName().equalsIgnoreCase("dict")) {
                    Horotype horotype = new Horotype();
                    for (int j = 0; j < htNodeList.getLength(); j++) {
                        Node subItemNode = htNodeList.item(j);
                        if (subItemNode.getNodeName().equalsIgnoreCase("key")) {
                            // Ignoring #text nodes
                            while (htNodeList.item(++j).getNodeName().equalsIgnoreCase("#text")) {
                            }

                            Node subItemNode2 = htNodeList.item(j);
                            String nodeKey = subItemNode.getChildNodes().item(0).getNodeValue();
                            String nodeValue = "";
                            if (subItemNode2.getChildNodes().item(0) != null) {
                                nodeValue = subItemNode2.getChildNodes().item(0).getNodeValue();
                            }

                            if (nodeKey.equalsIgnoreCase("contentType")) {
                                horotype.contentType = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("fallbackIconName")) {
                                horotype.fallBackIconName = (nodeValue.indexOf("sign-") >= 0 ? ChangeIcon(nodeValue) : nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("id")) {
                                horotype.horoId = (Integer.parseInt(nodeValue));
                            } else if (nodeKey.equalsIgnoreCase("interval")) {
                                horotype.interval = (Integer.parseInt(nodeValue));
                            } else if (nodeKey.equalsIgnoreCase("lastUpdated")) {
                                horotype.lastUpdated = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("longName")) {
                                horotype.longName = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("mediumName")) {
                                horotype.mediumName = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("name")) {
                                horotype.name = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("shareName")) {
                                horotype.shareName = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("shortName")) {
                                horotype.shortName = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("sortOrder")) {
                                horotype.sortOrder = (Integer.parseInt(nodeValue));
                            } else if (nodeKey.equalsIgnoreCase("tags")) {
                                horotype.tags = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("horoscopes")) {
                                horotype.horoscope = (parseHoroscopesFromNode(subItemNode2));
                            }
                        }
                    }

                    horotypes.add(horotype);
                }
            }
        }

        return horotypes;
    }

    private static String ChangeIcon(String nodeValue) {
        return nodeValue.replace("sign-", "");
    }

    public static ArrayList<Horoscope> parseHoroscopesFromNode(Node node) {
        ArrayList<Horoscope> horoscopes = new ArrayList<>();
        NodeList objectList = node.getChildNodes();

        for (int i = 0; i < objectList.getLength(); i++) {
            Node pArray = objectList.item(i);
            NodeList htNodeList = pArray.getChildNodes();

            if (!pArray.getNodeName().equalsIgnoreCase("#text")) {
                if (pArray.getNodeName().equalsIgnoreCase("dict")) {
                    Horoscope horoscope = new Horoscope();
                    for (int j = 0; j < htNodeList.getLength(); j++) {
                        Node subItemNode = htNodeList.item(j);
                        if (subItemNode.getNodeName().equalsIgnoreCase("key")) {

                            // Ignoring #text nodes
                            while (htNodeList.item(++j).getNodeName().equalsIgnoreCase("#text")) {

                            }

                            Node subItemNode2 = htNodeList.item(j);
                            String nodeKey = subItemNode.getChildNodes().item(0).getNodeValue();
                            String nodeValue = "";
                            if (subItemNode2.getChildNodes().item(0) != null) {
                                nodeValue = subItemNode2.getChildNodes().item(0).getNodeValue();
                            }

                            if (nodeKey.equalsIgnoreCase("date")) {
                                horoscope.date = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("dateInWords")) {
                                horoscope.dateInWords = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("id")) {
                                horoscope.horoId = (Integer.parseInt(nodeValue));
                            } else if (nodeKey.equalsIgnoreCase("name")) {
                                horoscope.name = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("shareURL")) {
                                horoscope.shareUrl = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("sortOrder")) {
                                horoscope.sortOrder = (Integer.parseInt(nodeValue));
                            } else if (nodeKey.equalsIgnoreCase("text")) {
                                horoscope.horoscopeText = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("urlRaw")) {
                                horoscope.normalBandwidthUrl = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("urlHighBandwidthRaw")) {
                                horoscope.highBandwidthUrl = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("urlLowBandwidthRaw")) {
                                horoscope.lowBandwidthUrl = (nodeValue);
                            } else if (nodeKey.equalsIgnoreCase("urlTiny")) {
                                horoscope.tinyUrl = (nodeValue);
                            }
                        }
                    }

                    horoscopes.add(horoscope);
                }
            }
        }

        return horoscopes;
    }

    public static Horotype parseHoroscopeFromXMLString(InputStream xml) throws Exception {
        Horotype horotype = new Horotype();
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(xml);
            doc.getDocumentElement().normalize();

            // Getting main child nodes
            NodeList plist = doc.getDocumentElement().getChildNodes();
            for (int i = 0; i < plist.getLength(); i++) {
                Node pArray = plist.item(i);
                NodeList objectList = pArray.getChildNodes();

                if (!pArray.getNodeName().equalsIgnoreCase("#text")) {
                    // First Dictionary
                    if (pArray.getNodeName().equalsIgnoreCase("dict")) {
                        for (int j = 0; j < objectList.getLength(); j++) {
                            Node subItemNode = objectList.item(j);
                            if (subItemNode.getNodeName().equalsIgnoreCase("key")) {
                                // Ignoring #text nodes
                                while (objectList.item(++j).getNodeName().equalsIgnoreCase("#text")) {

                                }

                                Node subItemNode2 = objectList.item(j);
                                String nodeKey = subItemNode.getChildNodes().item(0).getNodeValue();

                                String nodeValue = "";
                                if (subItemNode2.getChildNodes().item(0) != null) {
                                    nodeValue = subItemNode2.getChildNodes().item(0).getNodeValue();
                                }

                                if (nodeKey.equalsIgnoreCase("horosigns")) {
                                    ArrayList<ArrayList<Horogroup>> horosigns = parseHorosignsFromNode(subItemNode2);
                                    ArrayList<Horogroup> horosign = horosigns.get(0);
                                    Horogroup hg = horosign.get(0);
                                    horotype = hg.getHorotype().get(0);
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            throw new Exception(Consts.error_parser_horo);
        }

        return horotype;
    }

    public static ArrayList<ArrayList<Horogroup>> parseHorosignsFromNode(Node node) {
        ArrayList<ArrayList<Horogroup>> horosigns = new ArrayList<ArrayList<Horogroup>>();
        NodeList objectList = node.getChildNodes();

        for (int i = 0; i < objectList.getLength(); i++) {
            Node pArray = objectList.item(i);
            NodeList hgNodeList = pArray.getChildNodes();

            if (!pArray.getNodeName().equalsIgnoreCase("#text")) {
                if (pArray.getNodeName().equalsIgnoreCase("dict")) {
                    for (int j = 0; j < hgNodeList.getLength(); j++) {
                        Node subItemNode = hgNodeList.item(j);

                        if (subItemNode.getNodeName().equalsIgnoreCase("key")) {

                            // Ignoring #text nodes
                            while (hgNodeList.item(++j).getNodeName().equalsIgnoreCase("#text")) {

                            }

                            Node subItemNode2 = hgNodeList.item(j);
                            String nodeKey = subItemNode.getChildNodes().item(0).getNodeValue();

                            String nodeValue = "";
                            if (subItemNode2.getChildNodes().item(0) != null) {
                                nodeValue = subItemNode2.getChildNodes().item(0).getNodeValue();
                            }

                            if (nodeKey.equalsIgnoreCase("horogroups")) {
                                //ArrayList<Horogroup> horogroups = parseHorogroupsFromNode(subItemNode2);
                                horosigns.add(parseHorogroupsFromNode(subItemNode2));
                            }
                        }
                    }
                }
            }
        }

        return horosigns;
    }

    public void printXMLFromString(String xml) {
        try {
            xml = xml.trim();
            InputStream is = new ByteArrayInputStream(xml.getBytes("UTF-8"));

            // Build XML document
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(is);
            doc.getDocumentElement().normalize();

            // final Document doc = XMLfromString(xml);
            final NodeList nodes_array = doc.getElementsByTagName("array");
            for (int index = 0; index < nodes_array.getLength(); index++) {
                final Node node = nodes_array.item(index);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    final Element e = (Element) nodes_array.item(index);
                    final NodeList nodeKey = e.getElementsByTagName("key");
                    final NodeList nodeValue = e.getElementsByTagName("string");
                    // DataModel model = new DataModel();
                    for (int i = 0; i < nodeValue.getLength(); i++) {
                        final Element eleKey = (Element) nodeKey.item(i);
                        final Element eleString = (Element) nodeValue.item(i);
                        if (eleString != null) {
                            String strValue1 = getValue(eleString, "key");
                            String strValue2 = getValue(eleString, "string");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Create xml document object from XML String
    public Document XMLfromString(String xml) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            return db.parse(is);
        } catch (Exception e) {
            return null;
        }
    }

    // fetch value from Text Node only
    private String getElementValue(Node elem) {
        Node kid;
        if (elem != null) {
            if (elem.hasChildNodes()) {
                for (kid = elem.getFirstChild(); kid != null; kid = kid.getNextSibling()) {
                    if (kid.getNodeType() == Node.TEXT_NODE)
                        return kid.getNodeValue();
                }
            }
        }

        return "";
    }

    // / Fetch value from XML Node
    private String getValue(Element item, String str) {
        NodeList n = item.getElementsByTagName(str);
        return getElementValue(n.item(0));
    }

    public String convertStreamToString(InputStream is) throws IOException {
        /* To convert the InputStream to String we use the Reader.read(char[]
         * buffer) method. We iterate until the Reader return -1 which means
         * there's no more data to read. We use the StringWriter class to
         * produce the string.
         */
        if (is != null) {
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                // is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }
}