package com.astrology.iscopes.usercontrol;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.astrology.iscopes.R;
import com.astrology.iscopes.activity.SignWheel;
import com.astrology.iscopes.common.Consts;
import com.astrology.iscopes.common.MgrMenu;
import com.astrology.iscopes.common.Utility;

import java.util.Locale;

public class SpinWheel extends RelativeLayout {

    private Context context;
    private double mDegree = 0;
    public double rotatedDegrees = 0;

    public int selectedSign = 0;
    public float singleSignDegrees = 360.0f / 12.0f; // 27.69f;
    // Same as in item XML
    private float itemWidth = 225.0f;
    //private float itemHeight = 335.0f;
    // Same as in parent XML
    private float componentWidth = 1200.0f;
    private float componentHeight = 1200.0f;

    // Values determined based on above values in initView
    public float rotPivotX = 0.0f;
    public float rotPivotY = 0.0f;

    Resources res = getResources();

    public SpinWheel(Context context) {
        super(context);
        this.context = context;
        initView();
        this.setAnimationCacheEnabled(false);
        this.setDrawingCacheEnabled(true);
        this.setDrawingCacheQuality(DRAWING_CACHE_QUALITY_HIGH);
    }

    public SpinWheel(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    protected void initView() {
        setFocusable(true);
        componentWidth = Utility.DpToPixel(context, res.getInteger(R.integer.wheel_width));
        componentHeight = Utility.DpToPixel(context, res.getInteger(R.integer.wheel_height));
        itemWidth = Utility.DpToPixel(context, res.getInteger(R.integer.wheel_item_width));
        //itemHeight = Utility.DpToPixel(context, res.getInteger(R.integer.wheel_item_height));

        rotPivotX = componentWidth / 2.0f;
        rotPivotY = componentHeight / 2.0f;
        addViews();
    }

    public void addViews() {
        float pivotX = itemWidth / 2.0f;
        float pivotY = componentHeight / 2.0f;
        for (int i = 11; i >= 0; i--) {
            View v = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.wheelitem, (ViewGroup) this.getParent(), false);
            v.setId(i);

            ImageButton imgButton = v.findViewById(R.id.wheelItem_Btn);
            //imgButton.setImageResource(res.getIdentifier(String.format("signwheel_btn_%d", i), "drawable", context.getPackageName()));
            imgButton.setImageResource(res.getIdentifier(String.format("typeimg_1_%s", Consts.signs.get(i).name().toLowerCase(Locale.US)), "drawable", context.getPackageName()));
            ((TextView) v.findViewById(R.id.wheelItem_Title)).setText(res.getStringArray(R.array.signs)[i]);
            ((TextView) v.findViewById(R.id.wheelItem_SubTitle)).setText(res.getStringArray(R.array.symbols)[i]);
            ((TextView) v.findViewById(R.id.wheelItem_Date)).setText(res.getStringArray(R.array.dates)[i]);
            ((ImageView) v.findViewById(R.id.wheelItem_symbol)).setImageResource(res.getIdentifier(String.format(Locale.US, "sym_%02d_dial", i), "drawable", context.getPackageName()));

            imgButton.setOnClickListener((OnClickListener) context);
            imgButton.setClickable(false);
            AnimationSet animSet = new AnimationSet(false);
            Animation rotationAnimation = new RotateAnimation(0.0f, singleSignDegrees * i, pivotX, pivotY);
            rotationAnimation.setFillAfter(true);// keep rotated after animation ends
            Animation fade_in_animation = new AlphaAnimation(0.0f, 1.0f);
            fade_in_animation.setDuration(1000);
            fade_in_animation.setFillAfter(false);
            animSet.addAnimation(rotationAnimation);
            animSet.addAnimation(fade_in_animation);
            animSet.setFillAfter(true);
            animSet.setInterpolator(new LinearInterpolator());
            v.startAnimation(animSet);
            this.refreshDrawableState();
            this.addView(v);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.rotate((float) mDegree, getWidth() / 2, getHeight() / 2);
        invalidate();
        super.draw(canvas);
    }

    public void rotate(float degree) {
        mDegree = degree;
        this.invalidate();
    }

    public void rotateSignWheel(int numberOfSign, long duration) {
        double rotDeg = singleSignDegrees * numberOfSign;

        selectedSign -= numberOfSign;
        if (selectedSign > 11) selectedSign -= 12;
        else if (selectedSign < 0) selectedSign += 12;

        Animation an = new RotateAnimation((float) SignWheel.savedRotatedDegrees, (float) (rotatedDegrees + (float) rotDeg), rotPivotX, rotPivotY);
        an.setInterpolator(new DecelerateInterpolator());
        an.setDuration(duration); // duration in ms
        an.setRepeatCount(0); // -1 = infinite repeated
        an.setFillBefore(true);
        an.setFillEnabled(true);
        an.setFillAfter(true);

        this.refreshDrawableState();
        this.startAnimation(an);
        rotatedDegrees += rotDeg;
        this.invalidate();
    }

    public void rotatePartialSignWheel(double angle, long animationDuration) {
        Animation an = new RotateAnimation((float) rotatedDegrees, (float) (rotatedDegrees + (float) angle), rotPivotX, rotPivotY);

        an.setDuration(animationDuration);// 5/10 // duration in ms
        an.setRepeatCount(0); // -1 = infinite repeated
        an.setFillBefore(true);
        an.setFillEnabled(true);
        an.setFillAfter(true);
        this.setAnimationCacheEnabled(false);
        an.setInterpolator(new LinearInterpolator());
        this.refreshDrawableState();

        this.startAnimation(an);
        rotatedDegrees = ((rotatedDegrees + angle));
        this.invalidate();
    }

    @Override
    protected void onAnimationEnd() {
        super.onAnimationEnd();
    }

    public void computeSelectedSign(float totalAngleRotated) {
        selectedSign -= (int) (totalAngleRotated / singleSignDegrees);
        if (selectedSign > 11) selectedSign -= 12;
        else if (selectedSign < 0) selectedSign += 12;
        MgrMenu.getInstance(context).setSelectedHorosignId(selectedSign);
    }
}