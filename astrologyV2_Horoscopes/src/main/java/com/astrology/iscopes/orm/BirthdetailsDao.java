package com.astrology.iscopes.orm;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.astrology.iscopes.orm.Birthdetails;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table BIRTHDETAILS.
*/
public class BirthdetailsDao extends AbstractDao<Birthdetails, Long> {

    public static final String TABLENAME = "BIRTHDETAILS";

    /**
     * Properties of entity Birthdetails.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property BirthName = new Property(1, String.class, "birthName", false, "BIRTH_NAME");
        public final static Property BirthDate = new Property(2, String.class, "birthDate", false, "BIRTH_DATE");
        public final static Property BirthTime = new Property(3, String.class, "birthTime", false, "BIRTH_TIME");
        public final static Property BirthCity = new Property(4, String.class, "birthCity", false, "BIRTH_CITY");
        public final static Property BirthState = new Property(5, String.class, "birthState", false, "BIRTH_STATE");
        public final static Property BirthCountry = new Property(6, String.class, "birthCountry", false, "BIRTH_COUNTRY");
        public final static Property Gender = new Property(7, String.class, "gender", false, "GENDER");
        public final static Property StatePosition = new Property(8, Integer.class, "statePosition", false, "STATE_POSITION");
        public final static Property CountryPosition = new Property(9, Integer.class, "countryPosition", false, "COUNTRY_POSITION");
    };


    public BirthdetailsDao(DaoConfig config) {
        super(config);
    }
    
    public BirthdetailsDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'BIRTHDETAILS' (" + //
                "'_id' INTEGER PRIMARY KEY ," + // 0: id
                "'BIRTH_NAME' TEXT," + // 1: birthName
                "'BIRTH_DATE' TEXT," + // 2: birthDate
                "'BIRTH_TIME' TEXT," + // 3: birthTime
                "'BIRTH_CITY' TEXT," + // 4: birthCity
                "'BIRTH_STATE' TEXT," + // 5: birthState
                "'BIRTH_COUNTRY' TEXT," + // 6: birthCountry
                "'GENDER' TEXT," + // 7: gender
                "'STATE_POSITION' INTEGER," + // 8: statePosition
                "'COUNTRY_POSITION' INTEGER);"); // 9: countryPosition
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'BIRTHDETAILS'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Birthdetails entity) {
        stmt.clearBindings();
        if (entity.id != null) stmt.bindLong(1, entity.id);
        if (entity.birthName != null) stmt.bindString(2, entity.birthName);
        if (entity.birthDate != null) stmt.bindString(3, entity.birthDate);
        if (entity.birthTime != null) stmt.bindString(4, entity.birthTime);
        if (entity.birthCity != null) stmt.bindString(5, entity.birthCity);
        if (entity.birthState != null) stmt.bindString(6, entity.birthState);
        if (entity.birthCountry != null) stmt.bindString(7, entity.birthCountry);
        if (entity.gender != null) stmt.bindString(8, entity.gender);
        if (entity.statePosition != null) stmt.bindLong(9, entity.statePosition);
        if (entity.countryPosition != null) stmt.bindLong(10, entity.countryPosition);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Birthdetails readEntity(Cursor cursor, int offset) {
        Birthdetails entity = new Birthdetails( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // birthName
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // birthDate
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // birthTime
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // birthCity
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // birthState
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // birthCountry
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // gender
            cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8), // statePosition
            cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9) // countryPosition
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Birthdetails entity, int offset) {
        entity.id = (cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.birthName = (cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.birthDate = (cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.birthTime = (cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.birthCity = (cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.birthState = (cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.birthCountry = (cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.gender = (cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.statePosition = (cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8));
        entity.countryPosition = (cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Birthdetails entity, long rowId) {
        entity.id = (rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Birthdetails entity) {
        if(entity != null) return entity.id;
        else return null;
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    } 
}
